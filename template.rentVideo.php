<center>
    <?php if ($insuffientTokens) { ?>
        Sorry, you don't have enough tokens to view this video. You currently have <?php echo (int)$currentUserTokens; ?> and this video requires <?php echo (int)$videoNumTokens; ?>.<br><br>
        <a class='btn btn-default btn-sm' href='<?php echo $basehttp; ?>/buy-tokens'><?php echo _t("Purchase Tokens"); ?></a><br><br>
    <?php } else { ?>
        You are about to purchase access to "<?php echo $rrow['title']; ?>" at a cost of <?php echo (int)$videoNumTokens; ?> tokens for a period of <?php echo datediff(false,time(),time()+$vodRentalLength,true); ?>.<br><br>
        <a class='btn btn-default btn-sm' href='<?php echo $basehttp; ?>/purchase/<?php echo $rrow['record_num']; ?>?confirm=true'><?php echo _t("Confirm Purchase"); ?></a><br><br>
    <?php } ?>
</center>