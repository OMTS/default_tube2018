<?php
$link = generateUrl('video', $row['title'], $row['record_num']);
$dirname = str_replace('.flv', '', $row['orig_filename']);
$subdir = $row['filename'][0] . '/' . $row['filename'][1] . '/' . $row['filename'][2] . '/' . $row['filename'][3] . '/' . $row['filename'][4] . '/';
$dirname = $subdir . $dirname;
$uniq = uniqid();
?>
<!-- item :: column :: start -->
<div class="item col">
    <div class="item__inner">

        <a href="<? echo $link; ?>" title="<? echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>" class="item__link" <? if (!$row['embed']) { ?>data-mb="shuffle"<? } ?>>
            <span class="item__hd">
                <span class="item__stats-bar">

                    <span class="item__stat -bg-t1 -rating">
                        <span class="item__stat-icon">
                            <span class="icon -thumb-up"></span>
                        </span>
                        <span class="item__stat-label"><? echo $row['rating']; ?>%</span>
                    </span>

                    <span class="item__stat -views">
                        <span class="item__stat-icon">
                            <span class="icon -eye"></span>
                        </span>
                        <span class="item__stat-label"><? echo $row['views']; ?></span>
                    </span>

                    <span class="item__stat -duration">
                        <span class="item__stat-icon">
                            <span class="icon -time"></span>
                        </span>
                        <span class="item__stat-label"><? echo sec2time($row['length']); ?></span>
                    </span>
                    <? if ($row['movie_height'] >= 720) { ?>
                    <span class="item__stat -quality -bg-t2">
                        <span class="item__stat-label">HD</span>
                    </span>
                    <? } ?>

                </span>
            </span>
            <span class="item__bd">
                <span class="item__thumb">
                    <? if ($row['embed']) { ?>
                    <img src="<? echo $thumb_url; ?>/embedded/<? echo $row['record_num']; ?>.jpg" alt="<? echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>" class="item__thumb-img">
                    <? } else { ?>
                    <img src="<? echo $thumb_url; ?>/<? echo $dirname; ?>/<? echo $row['orig_filename']; ?>-<? echo $row['main_thumb']; ?>.jpg"
                         data-mb="shuffle-target" class="item__thumb-img"
                         alt="<? echo $row['title']; ?>" />
                    <? } ?>
                </span>
                <span class="item__title">
                    <span class="item__title-label"><? echo $row['title']; ?></span>
                </span>
            </span>
        </a>

        <a data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close") ?>" href="<? if ($_GET['mode'] == 'favorites') { ?><? echo $basehttp; ?>/action.php?action=remove_favorites&id=<? echo $row['record_num']; ?><? } else { ?><? echo $basehttp; ?>/action.php?action=add_favorites&id=<? echo $row['record_num']; ?><? } ?>" class="item__link-fav">
            <span class="icon -plus"></span>
        </a>
        <? if ($_SESSION['userid'] && ($row['submitter'] == $_SESSION['userid'])) { ?><a href="<? echo $basehttp; ?>/edit-content/?id=<? echo $row['record_num']; ?>" title="<?php echo _t("Edit") ?>" class="item__link-edit"><span class="icon -edit"></span></a><? } ?>
    </div>
</div>
<!-- item :: column :: end -->