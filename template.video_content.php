<!-- main :: column :: start -->
<main class="main col">
    <div class="main__inner">
        <div class="row">

            <!-- box :: column :: start -->
            <div class="box col -large-mrb">
                <div class="box__inner">
                    <div class="box__bd">
                        <div class="box__bd-inner">

                            <?php getWidget('widget.video.player.php'); ?>
                            
                            <?php getWidget('widget.video.skip-thumbs.php'); ?>
                            
                            <div class="row -sep"></div>
                            
                            <?php getWidget('widget.media.title.php'); ?>
                            
                            <div class="row -sep"></div>
                            
                            <?php getWidget('widget.media.tabs.php'); ?>
                            
                            <div class="row -sep"></div>
                            
                            <?php getWidget('widget.media.tabs-content.php'); ?>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <?php if ($allowDisplayComments) { ?>
        <div class="row">
                <?php $contentID = $rrow['record_num']; ?>
                <?php $commentsType = 0; ?>
                <?php include('widgets/widget.comments.php'); ?>
            
        </div>
        <?php } ?>
        
    </div>
</main>

<?php getWidget('widget.ad.content-side.php'); ?>