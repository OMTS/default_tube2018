<!-- pipe :: column :: start -->
    <div class="pipe col">
        <div class="pipe__inner">

<div class="row -flex">
    
    <?php getWidget('widget.profile.php'); ?>

    <!-- pdesc :: column :: start -->
    <div class="pdesc col">
        <div class="pdesc__inner">
            <div class="pdesc__hd">
                <h2 class="pdesc__h"><?php echo _t("User Information") ?></h2>
            </div>
            <div class="pdesc__bd">
                <div class="pdesc__bd-inner">

                    <div class="pdesc__row -odd">
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Username"); ?></div>
                            <div class="pdesc__ct"><?php echo $urow['username']; ?></div>
                        </div>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Joined"); ?></div>
                            <div class="pdesc__ct"><?php echo date('F jS Y', strtotime($urow['date_joined'])); ?></div>
                        </div>
                    </div>
                    <div class="pdesc__row -even">
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Last Login"); ?></div>
                            <div class="pdesc__ct"><?php
                                if ($urow['lastlogin']) {
                                    echo date('Y-m-d \a\t H:i:s', $urow['lastlogin']);
                                } else {
                                    echo 'Never';
                                }
                                ?></div>
                        </div>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Gender"); ?></div>
                            <div class="pdesc__ct"><?php $g = $urow['gender'];
                                $g ? $g = _t($g) : $g = _t("N/A");
                                echo $g; ?></div>
                        </div>
                    </div>
                    <div class="pdesc__row -odd">
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Age"); ?></div>
                            <div class="pdesc__ct"><?php $g = $urow['age'];
                                $g ? $g = _t($g) : $g = _t("N/A");
                                echo $g; ?></div>
                        </div>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Location"); ?></div>
                            <div class="pdesc__ct"><?php $g = $urow['location'];
                                $g ? $g = _t($g) : $g = _t("N/A");
                                echo $g; ?></div>
                        </div>
                    </div>

                    <div class="pdesc__row -even">
                        <div class="pdesc__col -width-full">
                            <div class="pdesc__label"><?php echo _t("A little about me...") ?></div>
                            <div class="pdesc__ct"><?php $g = $urow['description'];
                    $g ? $g = nl2br($urow['description']) : $g = _t("N/A");
                    echo $g; ?></div>
                        </div>
                    </div>
                    <?php $counter = 0;
                    $cls = '-even';
                    foreach ($custom_user_fields as $k => $v) { ?>

                        <?php
                        $counter++;

                        $pre = '';
                        $suff = '';


                        if ($counter === 1) {
                            $cls = $cls === '-even' ? '-odd' : '-even';
                            $pre = '<div class="pdesc__row ' . $cls . ' ">';
                            $suff = '';
                        } elseif ($counter === 2) {
                            $pre = '';
                            $suff = '</div>';
                            $counter = 0;
                        }
                        ?>
    <?php echo $pre; ?>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo $k; ?></div>
                            <div class="pdesc__ct"><?php echo $custom[$k]; ?></div>
                        </div>
    <?php echo $suff; ?>
<?php } ?>

                </div>
            </div>
        </div>
    </div>
    <!-- pdesc :: column :: end -->
</div>
</div>
</div>



