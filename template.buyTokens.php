<center>
    <?php
    $userNumTokens = billingGetUserTokens($_SESSION['userid'])
    ?>
    You currently have <?php echo $userNumTokens['tokens']; ?> token(s). Please click the links below to purchase more:<br><br>
    <a class='btn btn-default btn-sm' href='#'><?php echo _t("Purchase 1 Token"); ?></a>
    <a class='btn btn-default btn-sm' href='#'><?php echo _t("Purchase 5 Tokens"); ?></a>
    <a class='btn btn-default btn-sm' href='#'><?php echo _t("Purchase 10 Tokens"); ?></a>
    <a class='btn btn-default btn-sm' href='#'><?php echo _t("Purchase 20 Tokens"); ?></a><br><br>
</center>