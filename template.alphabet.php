<?php
    $sortprefix = "";
    if($_GET['sortby']) {
        $sortprefix = $_GET['sortby']."/";
    }
    $type = 'models';
    if($_GET['controller'] === 'tags') {
        $type = 'tags';
    }
?>
<!-- alphabet :: column :: start -->
<div class="alphabet col">
    <div class="alphabet__inner">
        <a class="alphabet__link -letter -leter-<? if (!$_GET['letter']) {
    echo ' is-active';
} ?>" href="<? echo $basehttp; ?>/<? echo $type; ?>/<? echo $sortprefix; ?>" >All</a> 
        <?
        $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        foreach ($alphabet as $i) {
            ?>
            <a class="alphabet__link -letter -leter-<? if ($i == substr($_GET['letter'],0,1)) {
            echo ' is-active';
        } ?>" href="<? echo $basehttp; ?>/<? echo $type; ?>/<? echo $i; ?>/<? echo $sortprefix; ?>" ><? echo ucwords($i); ?></a> 
<? } ?>
    </div>
</div>
<!-- alphabet :: column :: end -->