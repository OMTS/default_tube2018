<!-- fr :: column :: start -->
<div class="fr col">
    <div class="fr__inner">
        <form class="fr__form -center" name="loginForm" method="post">
            <div class="fr__row">
                <div class="fr__col">
                    <input type="email" placeholder="<?php echo _t("email") ?>" value="<? echo $thisusername; ?>" name="email" class="fr__input -input-text" />
                </div>
            </div>
            <div class="fr__row">
                <div class="fr__col">
                    <div class="captcha">
                        <img src="<?php echo $basehttp; ?>/captcha.php?<?php echo time(); ?>" class="captcha captcha-img captcha__img">
                        <input class="form-control captcha-input -captcha captcha__input" id="signup_captchaaa" name="captchaaa" type="text" value="" placeholder="<?php echo _t("Human?") ?>">
                    </div>
                </div>
            </div>

            <div class="fr__row">
                <div class="fr__col -center">
                    <button class="btn -primary fr__btn -login" type="submit"><?php echo _t("Go!") ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- fr :: column :: end -->

