<!-- top-bar -->
<section class="top-bar">
    <div class="wrapper">
        <div class="row">
            <?php getWidget('widget.logo.php'); ?>

            <?php getWidget('widget.ucp.php'); ?>
        </div>
    </div>
</section>
<!-- top-bar - END -->
<!-- nav -->
<section class="nav-sec">
    <div class="wrapper">
        <div class="row g--main-nav-row">

            <!-- navbar-trigger :: column :: start -->
            <div class="navbar-trigger col">
                <div class="navbar-trigger__inner">
                    <button class="btn -navbar-trigger" data-mb-trigger="nav">
                        <span class="btn__icon">
                            <span class="icon -navmenu"></span>
                        </span>
                    </button>
                </div>
            </div>
            <!-- navbar-trigger :: column :: end -->

            <!-- main-nav :: column :: start -->
            <nav class="main-nav col">
                <div class="main-nav__inner">
                    <ul class="main-nav-list">
                        <li class="main-nav-list__li has-list" data-mb="quick-hover">
                            
                            <a href="<?php echo $basehttp; ?>/videos/" class="main-nav-list__link <?php if (checkNav('videos')) echo 'is-active'; ?>" data-mb="mobile-nav" title="<?php echo _t("Movies") ?>">
                                <span class="main-nav-list__icon">
                                    <span class="icon -video"></span>
                                </span>
                                <span class="main-nav-list__label"><?php echo _t("Movies") ?></span>
                                <span class="main-nav-list__icon -expand">
                                    <span class="icon -caret-down"></span>
                                </span>
                            </a>

                            <ul class="dropdown-list g--dropdown">
                                <li class="dropdown-list__li">
                                    <a class="dropdown-list__link" href="<?php echo $basehttp; ?>/videos/" title="<?php echo _t("Most Recent") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("Most Recent") ?></span>
                                    </a>
                                </li>
                                <li class="dropdown-list__li">
                                    <a class="dropdown-list__link" href="<?php echo $basehttp; ?>/most-viewed/" title="<?php echo _t("Most Viewed") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("Most Viewed") ?></span>
                                    </a>
                                </li>
                                <li class="dropdown-list__li">
                                    <a class="dropdown-list__link" href="<?php echo $basehttp; ?>/top-rated/" title="<?php echo _t("Top Rated") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("Top Rated") ?></span>
                                    </a>
                                </li>
                                <li class="dropdown-list__li"><a class="dropdown-list__link" href="<?php echo $basehttp; ?>/most-discussed/" title="<?php echo _t("Most Discussed") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("Most Discussed") ?></span>
                                    </a>
                                </li>
                                <li class="dropdown-list__li"><a class="dropdown-list__link" href="<?php echo $basehttp; ?>/longest/" title="<?php echo _t("Longest") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("Longest") ?></span>
                                    </a>
                                </li>
								<li class="dropdown-list__li"><a class="dropdown-list__link" href="<?php echo $basehttp; ?>/vr/" title="<?php echo _t("VR") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("VR") ?></span>
                                    </a>
                                </li>
                            </ul>

                        </li>
                        
                        <li class="main-nav-list__li has-list" data-mb="quick-hover">
                            <a href="<?php echo $basehttp; ?>/photos/" title="<?php echo _t("Albums") ?>" class="main-nav-list__link <?php if (checkNav('photos')) echo 'is-active'; ?>" data-mb="mobile-nav" title="<?php echo _t("Albums") ?>">
                                <span class="main-nav-list__icon">
                                    <span class="icon -camera"></span>
                                </span>
                                <span class="main-nav-list__label"><?php echo _t("Albums") ?></span>
                                <span class="main-nav-list__icon -expand">
                                    <span class="icon -caret-down"></span>
                                </span>
                            </a>

                            <ul class="dropdown-list g--dropdown">
                                <li class="dropdown-list__li">
                                    <a class="dropdown-list__link" href="<?php echo $basehttp; ?>/photos/" title="<?php echo _t("Most Recent") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("Most Recent") ?></span>
                                    </a>
                                </li>
                                <li class="dropdown-list__li">
                                    <a class="dropdown-list__link" href="<?php echo $basehttp; ?>/photos/most-viewed/" title="<?php echo _t("Most Viewed") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("Most Viewed") ?></span>
                                    </a>
                                </li>
                                <li class="dropdown-list__li">
                                    <a class="dropdown-list__link" href="<?php echo $basehttp; ?>/photos/top-rated/" title="<?php echo _t("Top Rated") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("Top Rated") ?></span>
                                    </a>
                                </li>
                                <li class="dropdown-list__li">
                                    <a class="dropdown-list__link" href="<?php echo $basehttp; ?>/photos/most-discussed/" title="<?php echo _t("Most Discussed") ?>">
                                        <span class="dropdown-list__label"><?php echo _t("Most Discussed") ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                 

                        <li class="main-nav-list__li has-list main-nav-list__li--channels" data-mb="quick-hover">
                            <a href="<?php echo $basehttp; ?>/channels/" href="#hrefText" class="main-nav-list__link <?php if (checkNav('channels') || $_GET['mode'] == 'channel') echo 'is-active'; ?>" data-mb="mobile-nav" data-mb-quick-hover="exclude-exit"  title="<?php echo _t("Categories") ?>">
                                <span class="main-nav-list__icon">
                                    <span class="icon -list"></span>
                                </span>
                                <span class="main-nav-list__label"><?php echo _t("Categories") ?></span>
                                <span class="main-nav-list__icon -expand">
                                    <span class="icon -caret-down"></span>
                                </span>
                            </a>

                            <div class="overlay-drop g--dropdown">
                                <div class="overlay-drop__inner" data-mb-quick-hover="inner-exit">
                                    <div class="row">
                                        
                                        <?php showChannelsBlocks(array('mods' => '-top-bar', 'type' => 'navigation', 'limit' => 6)); ?>
                                        <!-- citem :: column :: start -->
                                        <div class="citem col -top-bar -top-bar-more">
                                            <div class="citem__inner">
                                                <a href="<?php echo $basehttp . "/channels/"; ?>" class="citem__link" title="<?php echo _t("See All") ?>">
                                                    <span class="citem__bd">
                                                        <span class="citem__title"><?php echo _t("See All") ?></span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- citem :: column :: end -->

                                    </div>
                                </div>
                            </div>
                        </li>
                        
                        
                        <li class="main-nav-list__li">
                            <a href="<?php echo $basehttp; ?>/models/" class="main-nav-list__link <?php if (checkNav('pornstars')) echo 'is-active'; ?>" title="<?php echo _t("Models") ?>">
                                <span class="main-nav-list__icon">
                                    <span class="icon -star"></span>
                                </span>
                                <span class="main-nav-list__label"><?php echo _t("Models") ?></span>
                            </a>
                        </li>
                        
                        <li class="main-nav-list__li">
                            <a href="<?php echo $basehttp; ?>/members/" class="main-nav-list__link <?php if (checkNav('members')) echo 'is-active'; ?>" title="<?php echo _t("Community") ?>">
                                <span class="main-nav-list__icon">
                                    <span class="icon -user"></span>
                                </span>
                                <span class="main-nav-list__label"><?php echo _t("Community") ?></span>
                            </a>
                        </li>

                        <li class="main-nav-list__li">
                            <a href="<?php echo $basehttp; ?>/paysites/" class="main-nav-list__link <?php if (checkNav('paysites')) echo 'is-active'; ?>" title="<?php echo _t("Paysites") ?>">
                                <span class="main-nav-list__icon">
                                    <span class="icon -money"></span>
                                </span>
                                <span class="main-nav-list__label"><?php echo _t("Paysites") ?></span>
                            </a>
                        </li>
                        
                         <li class="main-nav-list__li">
                            <a href="<?php echo $basehttp; ?>/tags/" class="main-nav-list__link <?php if (checkNav('tags')) echo 'is-active'; ?>" title="<?php echo _t("Tags") ?>">
                                <span class="main-nav-list__icon">
                                    <span class="icon -tag"></span>
                                </span>
                                <span class="main-nav-list__label"><?php echo _t("Tags") ?></span>
                            </a>
                        </li>


                    </ul>
                </div>
            </nav>
            <!-- main-nav :: column :: end -->

            <?php getWidget('widget.searchbox.php'); ?>

         

        </div>
    </div>
</section>
<!-- nav - END -->

<section class="utility-sec">
    <div class="wrapper">
        <div class="row">

            <?php getWidget('widget.network.php'); ?>

            <?php getWidget('widget.filters.php'); ?>

        </div>
    </div>
</section>