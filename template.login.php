<!-- fr :: column :: start -->
<div class="fr col">
    <div class="fr__inner">
        <form class="fr__form -center" name="loginForm" method="post">
            <div class="fr__row">
                <div class="fr__col">
                    <input type="text" placeholder="<?php echo _t("login") ?>" value="" name="ahd_username" class="fr__input -input-text" />
                </div>
            </div>
            <div class="fr__row">
                <div class="fr__col">
                    <input type="password" placeholder="<?php echo _t("password") ?>" value="" name="ahd_password" class="fr__input -input-text" />
                </div>
            </div>
            <div class="fr__row">
                <div class="fr__col">
                    <a href="<?php echo $basehttp; ?>/forgot-pass" class="fr__link"><?php echo _t("Forgot password") ?>.</a>
                </div>
            </div>

            <div class="fr__row">
                <div class="fr__col -center">
                    <button class="btn -primary fr__btn -login" type="submit"><?php echo _t("Login") ?></button> <a class="btn -secondary fr__btn -register" href="<? echo $basehttp; ?>/signup"><?php echo _t("Register free account") ?></a>
                </div>
            </div>
            <? if (!$_SESSION['userid'] && ($enable_facebook_login || $enable_twitter_login)) { ?>
            <div class="fr__row">
                <div class="fr__col">
                    <? if (!$_SESSION['userid'] && $enable_facebook_login) { ?>
                    <?php include($basepath . '/facebook_login.php'); ?>					
                    <a class="fr__link" href="<? echo $basehttp; ?>/includes/facebook/facebook.php"><img src="<?php echo $basehttp; ?>/core/images/facebook-login-button.png" alt="<?php echo _t("Login by FaceBook") ?>" border="0" /></a>
                    <? } ?>
                    <? if (!$_SESSION['userid'] && $enable_twitter_login) { ?>
                    <a class="fr__link" href="<?php echo $twitter->getAuthenticateUrl(); ?>&force_login=true"><img src="<?php echo $basehttp; ?>/core/images/twitter-login-button.png" alt="<?php echo _t("Login by Twitter") ?>" /></a>
                    <? } ?>
                </div>
            </div>
            <? } ?>
        </form>
    </div>
</div>
<!-- fr :: column :: end -->
