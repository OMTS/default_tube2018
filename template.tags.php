<?
if (!is_array($result)) { 
    ?>
    <div class="notification alert"><?php echo _t("Sorry, no tags found!"); ?></div>
<?php
} else {
    $result = partition($result, 4);
    foreach ($result as $column) {
        ?>
        <!-- fourc :: column :: start -->
        <div class="fourc col">
            <div class="fourc__inner">
                <? foreach ($column as $row) { ?>
                    <a href="<? echo $basehttp; ?>/search/<? echo str_replace(" ","-",$row['word']); ?>/" class="regular__link -block">
                        <span class="regular__link-label"><? echo $row['word']; ?></span>
                        <span class="regular__link-counter">(<? echo $row['amount']; ?>)</span>
                    </a>
                <? } ?>
            </div>
        </div>
        <!-- fourc :: column :: end -->
        <?
    }
}
?>