<?php 
    include_once("../../mb.php");
    if(is_numeric($_GET['id'])) {
        $id = $_GET['id']; 
    }
    
    $type = (int)$_GET['type'];	
    $loaded = "";
    if($_GET['loaded']) {
        $loaded = " LIMIT 0, 1";
    }
	
    $cresult = dbQuery("SELECT *, (SELECT username FROM users WHERE users.record_num = comments.userid) AS username, (SELECT avatar FROM users WHERE users.record_num = comments.userid) AS avatar, (SELECT gender FROM users WHERE users.record_num = comments.userid) AS gender FROM comments WHERE content = '$id' AND type = '".$type."' ORDER BY timestamp DESC".$loaded,false);
    if(count($cresult) > 0) {
?>
            <?php 
                foreach($cresult as $crow) { 			
                    if(isset($crow['username']) && $crow['username'] != ""){		
                        $username = $crow['username'];	
                        $link = generateUrl('user',$crow['username'],$crow['userid']);
                    } else {
                        $username = _t("Anonymous");
                        $link = '';
                    }	
            ?>
        
             <div class="row">
                        <!-- comment :: column :: start -->
                        <div class="comment col">
                            <div class="comment__inner">

                                <div class="comment__thumb">
                                    <!-- avatar :: module :: start -->
                                    <div class="avatar mod">
                                        <div class="avatar__inner">
                                            <span class="avata__thumb">
                                                        <?php if ($link) echo '<a href="' . $link . '">'; ?>                                
                                                        <?php if ($crow['avatar'] != '' && file_exists("$basepath/media/misc/$crow[avatar]")) { ?>
                                                            <img class="avatar__img" src='<?php echo $basehttp; ?>/media/misc/<?php echo $crow[avatar]; ?>' alt= '<?php echo ucwords($crow['username']); ?>'>
                                                        <?php } else { ?>
                                                            <?php if (strtolower($crow['gender']) == 'male') { ?>
                                                                <img class="avatar__img" src='<?php echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<?php echo ucwords($crow['username']); ?>'>
                                                            <?php } elseif (strtolower($crow['gender']) == 'female') { ?>
                                                                <img class="avatar__img" src='<?php echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<?php echo ucwords($crow['username']); ?>'>
                                                            <?php } else { ?>
                                                                <img class="avatar__img" src='<?php echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<?php echo ucwords($crow['username']); ?>'>
                                                            <?php } ?>
                                                        <?php } ?>                                                        
                                                        <?php if ($link) echo '</a>'; ?>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- avatar :: module :: end -->
                                </div>

                                <div class="comment__ct">
                                    <div class="comment__hd">
                                        <div class="comment__author">
                                            <<?php if($link) { ?>a href="<?php echo $link; ?>"<?php } else { ?>span<?php } ?> class="comment__author-link"><?php echo $username; ?></<?php if($link) { ?>a<?php } else { ?>span<?php } ?>>
                                        </div>
                                        <div class="comment__stat">
                                            <span class="comment__stat-icon">
                                                <span class="icon -time"></span>
                                            </span>
                                            <span class="comment__stat-label"><?php echo datediff('n',$crow[timestamp],time(),true); ?> <?php echo _t("ago") ?></span>
                                        </div>
                                    </div>

                                    <div class="comment__bd">
                                        <?php echo $crow[comment]; ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- comment :: column :: end -->
                    </div>
        
            <?php } ?>
<?php } else { ?>
    <?php if($type == 0){ ?>
<p><?php echo _t("There are no comments for this video. Please leave your feedback and be the first!") ?></p>
    <?php } elseif($type == 1){ ?>
    <p><?php echo _t("There are no comments for this model. Please leave your feedback and be the first!") ?></p>
    <?php } else { ?>
    <p><?php echo _t("There are no comments for this profile. Please leave your feedback and be the first!") ?></p>
    <?php } ?>
<?php } ?>