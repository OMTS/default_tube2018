<aside class="aside col">
    <div class="aside__inner">
        <div class="row">

            <!-- box :: column :: start -->
            <div class="box col -mrb">
                <div class="box__inner">
                    <div class="box__hd">
                        <div class="box__hd-inner">

                            <span class="box__hd-icon -sm">
                                <span class="icon -filter"></span>
                            </span>

                            <h2 class="box__h"><?php echo _t("Search Filter"); ?></h2>

                            <div class="box__hd-utils">
                                <button class="btn -outlined g--hidden-sm-up" data-mb-expand="box-member" data-mb-options="{'activeToMaxScreenWidth': 768}">
                                    <span class="btn__icon">
                                        <span class="icon -expand"></span>
                                    </span>
                                    <span class="btn__label -when-inactive"><?php echo _t("More") ?></span>
                                    <span class="btn__label -when-active"><?php echo _t("Hide") ?></span>
                                </button>
                            </div>

                        </div>
                    </div>
                    <div class="box__bd" data-mb-expand-target="box-member">
                        <div class="box__bd-inner">
                            <form action="<? echo $basehttp ?>/members/" method="post" name="formContact" id="member-filter-form" class="form form-block">
                                <div class="row">
                                    <!-- form-item -->
                                    <div class="form-item-col col-full col">
                                        <div class="form-item-inner-col inner-col">
                                            <label><?php echo _t("Username") ?></label>
                                            <?php $ms_name = ($_GET[q] ? $_GET[q] : $_SESSION['ms']['name']); ?>
                                            <input type="text" placeholder="<? echo _t("Username"); ?>" value="<? echo $ms_name; ?>" id="name" name="name" class="form-control">
                                        </div>
                                    </div>
                                    <!-- form-item END -->
                                </div>
                                <div class="row">
                                    <!-- filter-content -->
                                    <div class="form-item-col col-full mrb2 col">
                                        <div class="filter-content-inner-col inner-col">
                                            <label><?php echo _t("Age") ?></label>
                                            <div class="irs-range-slider">
                                                <div class="irs-range-slider__inner">
                                                    <?php
                                                    $age_from = (!empty($_SESSION['ms']['ageFrom'])) ? (int) $_SESSION['ms']['ageFrom'] : 18;
                                                    $age_to = (!empty($_SESSION['ms']['ageTo'])) ? (int) $_SESSION['ms']['ageTo'] : 100;
                                                    ?>
                                                    <input type="text" data-method="onpost" data-from="<? echo $age_from; ?>" data-to="<? echo $age_to; ?>" data-max="98" data-min="18" data-attr-from="ageFrom" data-attr-to="ageTo" data-step="4" id="range_length_filter" name="filter_age" value="" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- filter-content END -->
                                </div>   
                                <div class="row">
                                    <!-- filter-content -->
                                    <div class="form-item-col col-full col">
                                        <div class="form-item-inner-col inner-col">
                                            <label><?php echo _t("Location") ?></label>
                                            <input type="text" placeholder="<?php echo _t("Location"); ?>" value="<? echo $_SESSION['ms']['location']; ?>" id="location" name="location" class="form-control">
                                        </div>
                                    </div>
                                    <!-- filter-content END -->
                                </div>
                                <div class="row">
                                    <div class="form-item-col col-full mrb2 col">
                                        <div class="form-item-inner-col inner-col">
                                            <label><?php echo _t("Gender") ?></label>
                                            <select name="gender" id="select2" class="select-short" data-style="btn-selectpicker">
                                                <option value="all"><?php echo _t("All") ?></option>
                                                <option <?php
                                                if ($_SESSION['ms']['gender'] == 'Male') {
                                                    echo 'selected';
                                                }
                                                ?> value='Male'><?php echo _t("Male") ?></option>
                                                <option <?php
                                                if ($_SESSION['ms']['gender'] == 'Female') {
                                                    echo 'selected';
                                                }
                                                ?> value='Female'><?php echo _t("Female") ?></option>
                                            </select>
                                        </div>
                                    </div>                        
                                </div>
                                <div class="row">
                                    <div class="form-item-col col-full col">
                                        <!-- filter-content -->                        
                                        <div class="cr-holder checkbox">
                                            <label>
                                                <input class="form-control" type="checkbox" name="hasAvatar" value='1' id="checkbox"  <?php
                                                if ($_SESSION[ms][avatar] == 1) {
                                                    echo "checked='checked'";
                                                }
                                                ?>>
                                                <span class="sub-label"><?php echo _t("With avatars") ?></span>
                                            </label>
                                        </div>
                                        <!-- filter-content END -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-item-col col-full col">
                                        <!-- filter-content -->                        
                                        <div class="cr-holder checkbox">
                                            <label>
                                                <input class="form-control" type="checkbox" name="hasVideos" value='1' id="checkbox"  <?php
                                                if ($_SESSION[ms][video] == 1) {
                                                    echo "checked='checked'";
                                                }
                                                ?>>
                                                <span class="sub-label"><?php echo _t("Has uploaded videos"); ?></span>
                                            </label>
                                        </div>
                                        <!-- filter-content END -->
                                    </div>
                                </div>                        
                                <div class="row">
                                    <div class="form-item-col col-full col">
                                        <!-- filter-content -->                        
                                        <div class="cr-holder checkbox">
                                            <label>
                                                <input class="form-control" type="checkbox" name="hasPhotos" value='1' id="checkbox"  <?php
                                                if ($_SESSION[ms][photo] == 1) {
                                                    echo "checked='checked'";
                                                }
                                                ?>>
                                                <span class="sub-label"><?php echo _t("Has uploaded photos"); ?></span>
                                            </label>
                                        </div>
                                        <!-- filter-content END -->
                                    </div>
                                </div>                                                
                                <div class="row">
                                    <div class="filter-content-col col">
                                        <button name="membersFilter" type="submit" class="btn btn-default"><span class="btn-label"><?php echo _t("Search"); ?></span></button>
                                        <?php if ($_SESSION['ms']) { ?><a href="<? echo $basehttp; ?>/unsetFilters" class="btn btn-primary"><span class="btn-label"><?php echo _t("Reset filters"); ?></span></a><?php } ?>
                                    </div>
                                </div>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box :: column :: end -->

        </div>
    </div>
</aside>