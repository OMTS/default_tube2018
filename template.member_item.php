<?php
$link = generateUrl('user', $row['username'], $row['record_num']);
?>
<!-- citem :: column :: start -->
<div class="citem col -member">
    <div class="citem__inner">
        <a class="citem__link" href="<?php echo $link; ?>" title="<?php echo ucwords($row['username']); ?>">
            <span class="citem__thumb">
                <?php if($row['avatar'] != '' && file_exists("$basepath/media/misc/$row[avatar]")){ ?>
                    <img class="citem__thumb-img" src='<? echo $basehttp; ?>/media/misc/<? echo $row[avatar]; ?>' alt= '<? echo ucwords($row['username']); ?>'>
                <?php } else { ?>
                    <?php if(strtolower($row['gender']) == 'male'){ ?>
                        <img class="citem__thumb-img" src='<? echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<? echo ucwords($row['username']); ?>'>
                    <?php } elseif(strtolower($row['gender']) == 'female'){ ?>
                        <img class="citem__thumb-img" src='<? echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<? echo ucwords($row['username']); ?>'>
                    <?php } else { ?>
                        <img class="citem__thumb-img" src='<? echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<? echo ucwords($row['username']); ?>'>
                    <?php } ?>
                <?php } ?>
            </span>
            <span class="citem__bd">
                <span class="citem__title"><?php echo ucwords($row['username']); ?></span>
            </span>
        </a>
    </div>
</div>
<!-- citem :: column :: end -->