<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>{SUBJECT} - {SITE_NAME}</title>
		<style type="text/css">
			body  { padding: 0; margin: 0; background-color: #ebedf1; }
			body, div, p, td  { color: #505050; font-family: Arial, sans-serif; font-size: 12px; line-height: 1.5em; text-align: justify; -webkit-text-size-adjust: none; }
			table td { border-collapse: collapse; }
			img  { border: 0; outline: none; text-decoration: none; }
			a  { color: #ff2736; font-weight: bold; }
		</style>
	</head>

	<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<center>
			<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" style="border-top: 3px solid #ebedf1">
                
				<tr>
					<td colspan="3">
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#2b2929">
							<tr>
								<td style="text-align: left; padding: 20px; border-bottom: 2px solid #ff2736">
									<a href="{SITE_URL}"><img src="{TEMPLATE_URL}/email/logo.png" alt="{SITE_NAME}" width="212" /></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
                
				<tr>
					<td width="20">&nbsp;</td>
					<td width="560">
						<table width="560" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff">
							<tr>
								<td width="560">
									<div style="padding: 10px 0; border-top: 1px solid #ebedf1; border-bottom: 1px solid #ebedf1; color: #ff2736; font-size: 14px; font-weight: bold; text-align: center;">
										{SUBJECT}
									</div>
								</td>
							</tr>
							<tr>
								<td width="560" style="padding: 10px 0 30px 0; word-break: break-word; ">
									<div style="padding: 10px 15px; background: #fff; border: 1px solid #ebedf1; border-radius: 5px;">
										{MESSAGE}
									</div>
								</td>
							</tr>
						</table>
					</td>
					<td width="20">&nbsp;</td>
				</tr>

				<tr>
					<td colspan="3">
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#2b2929">
							<tr>
								<td width="600" style="text-align: center; padding: 10px;">
									<a href="{SITE_URL}" title="{SITE_NAME}"><img src="{TEMPLATE_URL}/email/logo.png" alt="{SITE_NAME}" width="106" /></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

		</center>

	</body>
</html>
