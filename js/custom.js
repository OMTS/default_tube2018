(function($){
    function mobileNav() {
        if(navigator.userAgent.match(/Mobi/)) {
            $('[data-mb="mobile-nav"]').on("touchend", function(e) {
                if(!$(this).hasClass('clicked')) {
                    e.preventDefault();
                    $('.main-nav-list__li').removeClass('open');
                    $('[data-mb="mobile-nav"]').removeClass('clicked');
                    $(this).addClass('clicked');
                    $(this).parent('.main-nav-list__li').addClass('open');
                }
            });
            $('body').on("touchend", function(r) {
                if(r.target !== $('[data-mb="mobile-nav"]') || r.target.parentElement !== $('[data-mb="mobile-nav"]')) {
                    $('[data-mb="mobile-nav"]').removeClass('clicked');
                    $('.main-nav-list__li').removeClass('open');
                }
            });
        }
    }
    
    $(document).ready(function() {
        mobileNav();
    });
}(jQuery))