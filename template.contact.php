<!-- textpage -->
<div class="textpage-col textpage--contactform col">
    <div class="textpage-inner-col inner-col">
        
        <div class="row form-row">

            <!-- form -->
            <div class="form-col form--contact col">
                <div class="form-inner-col inner-col">
                    <form class="form-block" id="contact-form"  name="formContact" method="post" action="">


                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col col-half">
                                <div class="form-item-inner-col inner-col">
                                    <input class="form-control" name="name" type="text" id="name" value="<? echo $_SESSION['username']; ?>" placeholder="<?php echo _t("Username") ?>">
                                </div>
                            </div>
                            <!-- form-item END -->
                            <!-- form-item -->
                            <div class="form-item-col col col-half">
                                <div class="form-item-inner-col inner-col">
                                    <input class="form-control" name="email" type="text" id="email"  value="" placeholder="<?php echo _t("Email") ?>">
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col col-full">
                                <div class="form-item-inner-col inner-col">
                                    <textarea name="message" id="textfield3" placeholder="<?php echo _t("Message") ?>"></textarea>
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col form-item--captcha">
                                <div class="form-item-inner-col inner-col">
                                    <div class="captcha">
                                        <img src="<? echo $basehttp; ?>/captcha.php?<? echo time(); ?>" class="captcha captcha-img captcha__img">
                                        <input class="form-control captcha-input -captcha captcha__input" name="captchaaa" type="text" value="" placeholder="<?php echo _t("Human?") ?>">
                                    </div>
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col form-item--actions">
                                <div class="form-item-inner-col inner-col">
                                    <button class="btn btn-default" type="submit" name="Submit"><span class="btn-label"><?php echo _t("Send message") ?></span></button>
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                    </form>
                </div>
            </div>
            <!-- form END -->
        </div>
    </div>
</div>
<!-- textpage END -->
