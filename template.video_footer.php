
                            </div><!-- row :: column :: end -->
                            
                            <div class="row">
                                <div class="box col -large-mrb">
                                    <div class="box__inner">
                                        <div class="box__hd">
                                            <div class="box__hd-inner">
                                                <span class="box__hd-icon">
                                                    <span class="icon -video"></span>
                                                </span>
                                                <h2 class="box__h"><?php echo _t("Related Content"); ?></h2>
                                            </div>
                                        </div>
                                        
                                        <div class="box__bd">
                                            <div class="box__bd-inner">
                                                <div class="row">
                                                    <?php showRelated($rrow['related'], "template.content_item.php", $rrow['record_num'], 12); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div><!-- wrapper :: column :: end -->
                    </section><!-- regular-sec -sec-main :: column :: end -->
                    
                    <?php getWidget('widget.ad.bottom.php'); ?>

                </section><!-- page-main END -->

                <?php getWidget('widget.footer.links.php'); ?>

            </div><!-- page-wrapper__inner END -->

        </section><!-- page-wrapper END -->

    </body>
</html>