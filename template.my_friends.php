<?php
    $resultNew = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added,  users.record_num, users.username FROM friends, users 
		          WHERE friends.friend = '$_SESSION[userid]' AND users.record_num =  friends.user AND approved = 0 ORDER BY friends.date_added", false);
?>

<div class="row -flex">
    
    <?php getWidget('widget.profile.php'); ?>
     <!-- pdesc :: column :: start -->
    <div class="pdesc col">
        <div class="pdesc__inner">
            <div class="pdesc__bd">
                <div class="pdesc__bd-inner">
                    
                    
                    <!-- tab :: module :: start -->
                    <div class="tab mod" data-tab-group="profile" data-tab-id="1">
                        <h2><?php echo _t("New invitations") ?> <span class="dimmed-desc">(<?php echo count($resultNew); ?>)</span></h2>
                                    <div class="table-responsive">
                                        <form method="post" action="">
                                            <table width="100%" border="0" class="sTable">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="150"><div align="left"><?php echo _t("User") ?></div></th>
                                                        <th class="text-center"><div align="left"><?php echo _t("Message") ?></div></th>
                                                        <th class="text-center"><div class="text-center"><?php echo _t("Approve") ?></div></th>	
                                                        <th class="text-center"  width="90"><div align="left"><?php echo _t("Sent") ?></div></th>
                                                        <th class="text-center"  width="60"><div class="text-center"><?php echo _t("Delete") ?></div></th>
                                                    </tr>	
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if ($resultNew) {
                                                            foreach ($resultNew as $nrow) {
                                                                $link = generateUrl('user', $nrow[username], $nrow[record_num]);
                                                    ?>
                                                    <tr id="friend<?php echo $nrow[friend_id]; ?>">
                                                        <td><a href="<?php echo $link; ?>"><?php echo $nrow[username]; ?></a></td>
                                                        <td><?php echo $nrow[friend_request_message]; ?></td>
                                                        <td>
                                                            <div class="text-center">
                                                                <a onClick="javascript:approveFriend('<?php echo $nrow[friend_id]; ?>'); return false;" href="#" class="addIco">
                                                                    <span class="icon i-ok"></span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <td><strong><?php echo datediff('', $nrow[date_added], date('Y-m-d H:i:s'), false); ?></strong> ago</td>        
                                                        <td class="text-center">
                                                            <div class="text-center">
                                                                <a onClick="javascript:deleteFriend('<?php echo $nrow[friend_id]; ?>'); return false;" href="#" class="removeIco">
                                                                    <span class="icon i-delete"></span>
                                                                </a>
                                                            </div>
                                                        </td>		
                                                    </tr>
                                                    <?php } ?>				
                                                    <td colspan="5" class="no-border">
                                                        <a class="btn btn-default btn-sm" href="<?php echo $basehttp; ?>/my-friends?acceptAll=1"><?php echo _t("Accept ALL") ?></a>  <a href="<?php echo $basehttp; ?>/my-friends?declineAll=1" class="btn btn-default btn-sm"><?php echo _t("Decline  ALL") ?></a>
                                                    </td>	
                                                <?php } else {
                                                    ?>
                                                    <td colspan="5" class="no-border"><?php echo _t("No new invation") ?></td>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>
                    </div>
                    <!-- tab :: module :: end -->
                    
                    <!-- tab :: module :: start -->
                    <div class="tab mod" data-tab-group="profile" data-tab-id="2">
                        <h2><?php echo _t("My Friends") ?></h2>
                                    <div class="table-responsive">
                                        <form method="post" action="">
                                            <table width="100%" border="0" class="sTable">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="150"><div align="left"><a href="?sortby=name"><?php echo _t("User") ?></a></div></th>
                                                        <th class="text-center"><div align="left"><a href="?sortby=date"><?php echo _t("Date added") ?></a></div></th>			
                                                        <th class="text-center" width="60"><div class="text-center"><?php echo _t("Delete") ?></div></th>
                                                    </tr>	
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if (isset($_GET[sortby]) && $_GET[sortby] == 'name') {
                                                            $orderBy = ' users.username ASC ';
                                                        } else if (isset($_GET[sortby]) && $_GET[sortby] == 'date') {
                                                            $orderBy = ' friends.date_added DESC ';
                                                        } else {
                                                            $orderBy = 'friends.date_added DESC';
                                                        }
                                                        $resultFriends = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added,  users.record_num, users.username FROM friends, users WHERE  friends.friend = '$_SESSION[userid]' AND users.record_num =  friends.user AND approved = 1 ORDER BY $orderBy", false);

                                                        if ($resultFriends) {
                                                            foreach ($resultFriends as $frow) {
                                                                $link = generateUrl('user', $frow[username], $frow[record_num]);
                                                    ?>
                                                    <tr id="friend<?php echo $frow[friend_id]; ?>">
                                                        <td><a href="<?php echo $link; ?>"><?php echo $frow[username]; ?></a></td>			
                                                        <td><strong><?php echo datediff('', $frow[date_added], date('Y-m-d H:i:s'), false); ?></strong> <?php echo _t("ago") ?></td>
                                                        <td class="text-center"><div class="text-center"><a onClick="javascript:deleteFriendApproved('<?php echo $frow[friend_id]; ?>');
                                                                        return false;" class="removeIco" href="#"><span class="icon i-delete"></span></a></div></td>		
                                                    </tr>
                                                    <?php
                                                            }
                                                        } else {
                                                    ?>
                                                    <td colspan="3"><?php echo _t("You have no friends") ?>.</td>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>
                    </div>
                    <!-- tab :: module :: end -->
                    
                    <!-- tab :: module :: start -->
                    <div class="tab mod" data-tab-group="profile" data-tab-id="3">
                        <h2><?php echo _t("Sent Invitations") ?></h2>
                                    <div class="table-responsive">
                                        <form method="post" action="">
                                            <table width="100%" border="0" class="sTable">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="150"><div align="left"><?php echo _t("User") ?></div></th>	
                                                        <th class="text-center"><div align="left"><?php echo _t("Message") ?></div></th>		
                                                        <th class="text-center" width="90"><div align="left"><?php echo _t("Sent") ?></div></th>			
                                                        <th class="text-center" width="60"><div class="text-center"><?php echo _t("Delete") ?></div></th>
                                                    </tr>	
                                                </thead>
                                                <tbody>
                                                <?php
                                                    $resultSent = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added, users.record_num, users.username FROM friends, users WHERE  friends.user = '$_SESSION[userid]' AND users.record_num =  friends.friend AND  friends.approved = 0  ORDER BY friends.date_added DESC", false);
                                                    if ($resultSent) {
                                                        foreach ($resultSent as $srow) {
                                                            $link = generateUrl('user', $srow[username], $srow[record_num]);
                                                ?>
                                                    <tr id="friend<?php echo $srow[friend_id]; ?>">
                                                        <td><a href="<?php echo $link; ?>"><?php echo $srow[username]; ?></a></td>
                                                        <td><?php echo $srow[friend_request_message]; ?></td>
                                                        <td class="text-center"><div align="left"><strong><?php echo datediff('', $srow[date_added], date('Y-m-d H:i:s'), false); ?></strong> ago</div></td>	
                                                        <td class="text-center"><div class="text-center">
                                                                <a onclick="javascript:deleteFriend('<?php echo $srow[friend_id]; ?>');
                                                                                return false;" href="#" class="removeIco"><span class="icon i-delete"></span></a></div></td>		
                                                    </tr>
                                                <?php
                                                        }
                                                    } else {
                                                ?>
                                                    <tr><td colspan="4"><?php echo _t("No result") ?></td></tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>
                    </div>
                    <!-- tab :: module :: end -->
                    
                    
                </div>
            </div>
                    
        </div>
    </div>
    
</div>
<script>
    function deleteFriend(id) {
        $("#friend" + id).fadeOut("slow");
        $.get('<?php echo $basehttp; ?>/includes/ajax.delete_friend.php?id=' + id);
    }
    function deleteFriendApproved(id) {
        $("#friend" + id).fadeOut("slow");
        $.get('<?php echo $basehttp; ?>/includes/ajax.delete_friend.php?approved=1&id=' + id);
    }

    function approveFriend(id) {
        $("#friend" + id).fadeOut("slow");
        $.get('<?php echo $basehttp; ?>/includes/ajax.approve_friend.php?id=' + id);
    }

    $(document).ready(function () {
        $('#profile h4').click(function () {
            $(this).parent().children('.hidden').slideToggle('fast');
            if ($(this).children('.more').hasClass('off')) {
                $(this).children('.more').removeClass('off').addClass('on');
            } else {
                $(this).children('.more').removeClass('on').addClass('off');
            }
        });
    });
</script>
