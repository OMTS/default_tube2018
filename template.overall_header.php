<?php $colorClass = isset($_COOKIE["user_mb_invertedColors"]) && $_COOKIE["user_mb_invertedColors"] === true ? " js-inverted-colors" : ""; ?><!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7-only<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8-only<?php echo $colorClass ?>"><![endif]-->
<!--[if gte IE 9]><!--> <html class="no-js no-filter<?php echo $colorClass ?>"><!--<![endif]-->
    <head>
        <?php getWidget('widget.header.scripts.php'); ?>
    </head>
    <body class="page-<?php echo bodyClasses(); ?>">

        <section class="page-wrapper">
            <div class="page-wrapper__inner">
                
                <section class="header-sec">
                    <?php getTemplate('template.nav.php'); ?>
                </section>
                <!-- header-sec END -->

                <!-- page-main START -->
                <section class="page-main">

                    <?php getWidget('widget.top.php'); ?>

                    <?php getWidget('widget.index.top.php'); ?>

                    <?php getWidget('widget.index.most-viewed.php'); ?>

                    <?php getTemplate('template.paysite_header.php'); ?>


                     <?php
                        $sidebarTpl = 'template.sidebar.php';
                        $isSidebarVisible = false;
                        $isMediaPage = false;

                        if (($_GET[controller] == "index" || checkNav('paysites') || checkNav('channels')) && $_GET[mode] != "search") {
                            $isSidebarVisible = true;
                        }
                        if ($_GET[controller] == "members" || ($_GET[mode] == "search" && $_GET[type] == "members")) {
                            $sidebarTpl = 'template.sidebar_members.php';
                            $isSidebarVisible = true;
                        }
                     ?>



                    <section class="regular-sec g-sec -sec-main">
                        <div class="wrapper">
                            <div class="row<?php if($isSidebarVisible) { ?> -content-aside -aside-left<?php } ?>">

                                <?php if($isSidebarVisible) { ?>
                                    <?php getTemplate($sidebarTpl); ?>
                                    <!-- main :: column :: start -->
                                    <main class="main col">
                                        <div class="main__inner">
                                            <!-- MAIN -->
                                            <div class="row">
                                <?php } ?>

                                    <!-- box :: column :: start -->
                                    <div class="box col">
                                        <div class="box__inner">

                                            <?php getWidget('widget.title.php'); ?>

                                            <?php getWidget('widget.index.category-desc.php'); ?>


                                            <div class="box__bd">
                                                <div class="box__bd-inner">
                                                    <?php if (checkNav('pornstars') || $_GET['controller'] === 'tags') { ?>
                                                    <div class="row">
                                                        <?php include($template_path . '/template.alphabet.php'); ?>
                                                    </div>
                                                    <?php } ?>
                                                    <div class="row<?php if($isSidebarVisible) { ?> -cols-narrow<?php } ?>">

                                                        <?php getWidget('widget.ad.list.php'); ?>
