<!-- pipe :: column :: start -->
    <div class="pipe col">
        <div class="pipe__inner">

<div class="row -flex">
    
    <?php getWidget('widget.profile.php'); ?>

    <!-- pdesc :: column :: start -->
    <div class="pdesc col">
        <div class="pdesc__inner">
            <div class="pdesc__hd">
                <h2 class="pdesc__h"><?php echo _t("Profile") ?> <span class="pdesc__h-legend">(<?php echo _t("Views") ?>: <?php echo $rrow['views']; ?>)</span></h2>
            </div>
            <div class="pdesc__bd">
                <div class="pdesc__bd-inner">

                    <div class="pdesc__row -odd">
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Name"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['name'] ? $rrow['name'] : _t('N/A'); ?></div>
                        </div>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Aka"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['aka'] ? $rrow['aka'] : _t('N/A'); ?></div>
                        </div>
                    </div>
                    
                    <div class="pdesc__row -even">
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Dob"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['dob'] ? $rrow['dob'] :  _t('N/A'); ?></div>
                        </div>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Height"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['height'] ? $rrow['height'] : _t('N/A'); ?></div>
                        </div>
                    </div>
                    
                    <div class="pdesc__row -odd">
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Weight"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['weight'] ? $rrow['weight'] : _t('N/A'); ?></div>
                        </div>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Measurements"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['measurements'] ? $rrow['measurements'] : _t('N/A'); ?></div>
                        </div>
                    </div>
                    
                    <div class="pdesc__row -even">
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Hair"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['hair'] ? $rrow['hair'] : _t('N/A'); ?></div>
                        </div>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Eyes"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['eyes'] ? $rrow['eyes'] : _t('N/A'); ?></div>
                        </div>
                    </div>
                    
                    <div class="pdesc__row -odd">
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Ethnicity"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['ethnicity'] ? $rrow['ethnicity'] : _t('N/A'); ?></div>
                        </div>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Official site"); ?></div>
                            <div class="pdesc__ct"><?php if($rrow['official_site_url']) { ?><a href="<?php echo $rrow['official_site_url']; ?>" target="_blank" class="pdesc__link"> <?php echo $rrow['official_site_name']; ?></a><?php } else { echo _t('N/A'); }?></div>
                        </div>
                    </div>
                    
                    <div class="pdesc__row -even">
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Biography"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['biography'] ? $rrow['biography'] : _t('N/A'); ?></div>
                        </div>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo _t("Rank"); ?></div>
                            <div class="pdesc__ct"><?php echo $rrow['rank']; ?></div>
                        </div>
                    </div>
                    
  

             
                    <?php $counter = 0;
                    $cls = '-even';
                    $custom = unserialize($rrow['custom']);
                    foreach ($custom_pornstar_fields as $k => $v) { ?>

                        <?php
                        $counter++;

                        $pre = '';
                        $suff = '';


                        if ($counter === 1) {
                            $cls = $cls === '-even' ? '-odd' : '-even';
                            $pre = '<div class="pdesc__row ' . $cls . ' ">';
                            $suff = '';
                        } elseif ($counter === 2) {
                            $pre = '';
                            $suff = '</div>';
                            $counter = 0;
                        }
                        ?>
                        <?php echo $pre; ?>
                        <div class="pdesc__col">
                            <div class="pdesc__label"><?php echo $k; ?></div>
                            <div class="pdesc__ct"><?php echo $custom[$k] ? $custom[$k] : _t('N/A'); ?></div>
                        </div>
                        <?php echo $suff; ?>
                        <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <!-- pdesc :: column :: end -->
</div>
</div>
</div>

