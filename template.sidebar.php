<aside class="aside col">
    <div class="aside__inner">
        <?php getWidget('widget.side.filters.php'); ?>
        
        <?php getWidget('widget.side.duration.php'); ?>
        
        <?php getWidget('widget.side.categories.php'); ?>
        
        <?php getWidget('widget.side.tags.php'); ?>
        
        <?php getWidget('widget.side.paysites.php'); ?>
    </div>
</aside>