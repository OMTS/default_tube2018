<?php
if ($_GET['controller'] == 'index' && isset($_GET['mode']) && $_GET['mode'] == 'paysites' && is_numeric($_GET['paysite'])) {
    if ($currentLang) {
        $langSelect = ", paysites_languages.data";
        $langJoin = " RIGHT JOIN paysites_languages ON paysites_languages.paysite = paysites.record_num";
        $langWhere = " AND paysites_languages.language = '$currentLang'";
    }

    $getPaysite = dbQuery("SELECT * $langSelect FROM paysites $langJoin WHERE enabled = 1 AND record_num = {$_GET['paysite']} $langWhere", false);
    if (!empty($getPaysite)) {
        $paysite = $getPaysite[0];
        if ($currentLang) {
            $langData = unserialize($paysite['data']);
            $description = $langData['description'];
        } else {
            $description = $paysite['description'];
        }
        $paysiteImgUrl = $basehttp . '/core/images/catdefault.jpg';
        if (file_exists("$misc_path/paysite{$paysite['record_num']}.jpg")) {
            $paysiteImgUrl = "$misc_url/paysite{$paysite['record_num']}.jpg";
        }
        $paysiteBgUrl = "";
        if (file_exists("$misc_path/paysite-bg{$paysite['record_num']}.jpg")) {
            $paysiteBgUrl = "$misc_url/paysite-bg{$paysite['record_num']}.jpg";
        }
        $paysiteName = $paysite['name'];
        ?>
        <?php if (file_exists("$misc_path/paysite{$paysite['record_num']}.jpg") || !empty($description)) { ?>

            <section class="regular-sec g-sec -sec-top-content">
                <div class="wrapper">
                    <div class="row">
                        <div class="box col">
                            <div class="box__inner">
                                <div class="box__hd">
                                    <div class="box__hd-inner">
                                        <span class="box__hd-icon">
                                            <span class="icon -money"></span>
                                        </span>
                                        <h2 class="box__h"><?php echo $paysiteName; ?></h2>
                                    </div>
                                </div>
                                <div class="box__bd<?php echo !empty($paysiteBgUrl) ? " -has-bg" : ""; ?>">
                                    <?php if(!empty($paysiteBgUrl)) { ?>
                                        <div class="box__bg-img">
                                            <img class="paysite__bg-img" src="<?php echo $paysiteBgUrl; ?>" alt="<?php echo $paysiteName; ?>">
                                        </div>
                                    <?php } ?>
                                    
                                    <div class="box__bd-inner">
                                        <div class="row -flex">

                                            <div class="paysite__thumb col">
                                                <img class="paysite__thumb-img" src="<?php echo $paysiteImgUrl; ?>" alt="<?php echo $paysiteName; ?>" >
                                            </div>

                                            <div class="paysite__stats col">
                                                <div class="paysite__stat">
                                                    <span class="icon -video"></span>
                                                    <span class="sub-label"><?php echo paysiteVideoNum($_GET['paysite']); ?></span>
                                                </div>
                                                
                                                <div class="paysite__stat">
                                                    <span class="icon -photo"></span>
                                                    <span class="sub-label"><?php echo paysiteGalleryNum($_GET['paysite']); ?></span>
                                                </div>
                                            </div>
                                            
                                            <div class="paysite__bd col">
                                                <div class="paysite__bd-inner">
                                                    <?php echo $description; ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php } ?>
    <?php } ?>
<?php } ?>