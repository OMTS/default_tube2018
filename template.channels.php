<?php

if (is_array($results)) {
    $i = 0;
    foreach ($results as $row) {
        if ($row['langName']) {
            $row['name'] = $row['langName'];
        }
        if ($results_per_row > 0) {
            $i++;
            $rest = $i % $results_per_row;
            if ($rest == 0) {
                $class = ' last';
            } else {
                $class = '';
            }
        }
        getTemplate("template.channel_item.php");
    }
} else {
    echo setMessage(_t("No categories found"), 'error', true);
}