<?php


//shows a list of channels, for instance: showChannels('<li>','</li>'); 
function showChannels2($pre, $post, $counter = false) {
    global $basehttp;
    global $basepath;
    global $currentLang;
    global $template_url;
    global $config;

    if ($currentLang  && strtoupper($currentLang) != $config['core_language']) {
         $langSelect = ", `niches_languages`.`name` AS `langName`";
        $langJoin = " LEFT JOIN `niches_languages` ON `niches_languages`.`niche` = `niches`.`record_num` AND `niches_languages`.`language` = '$currentLang'";
        $langWhere = " ";
    }

    $result = dbQuery("SELECT niches.* $langSelect FROM niches $langJoin WHERE 1=1 $langWhere ORDER BY name ASC");

    if (is_array($result)) {
        foreach ($result as $row) {
            $c = "";

            if ($counter) {
                $count_content = dbValue("SELECT COUNT(`content`) AS `count` FROM `content_niches` LEFT JOIN `content` ON `content_niches`.`content` = `content`.`record_num` WHERE `content_niches`.`niche` = '$row[record_num]' AND `content`.`approved` = 2 AND `content`.`enabled` = 1", 'count', true);
                $c = '<span class="counter-list__link-counter">' . $count_content . '</span>';
            }
            $class = ($counter) ? 'has-counter' : "";

            if ($row['langName']) {
                $row['name'] = $row['langName'];
            }
            if ($_GET['channel'] == $row['record_num']) {
                $class .= " is-active";
            }
            $link = generateUrl('channel', $row['name'], $row['record_num']);
            $icon = '<span class="counter-list__icon"><span class="icon -caret-right"></span></span>';
            echo $pre . "<a title='$row[name]' href='$link' class='counter-list__link " . $class . "'>".$icon."<span class='counter-list__link-label'>$row[name]</span>$c</a>" . $post;
        }
    }
}

//shows a list of channels, for instance: showChannels('<li>','</li>'); 
function showPaysites2($pre, $post) {
    global $cache_path;
    global $overall_cache_time;
    global $basehttp;
    global $basepath;
    global $currentLang;
    global $template_url;


    $result = dbQuery("SELECT * FROM paysites ORDER BY name ASC", true);
    if (is_array($result)) {
        foreach ($result as $row) {
            if ($row['langName']) {
                $row['name'] = $row['langName'];
            }
            $link = generateUrl('paysite', $row['name'], $row['record_num']);
            $icon = '<span class="counter-list__icon"><span class="icon -caret-right"></span></span>';
            echo $pre . "<a class=\"counter-list__link\" title='$row[name]' href='$link'>".$icon."<span class='counter-list__link-label'>$row[name]</span></a>" . $post;
        }
    }
}

function getUserAvatarUrl($id) {
    global $basehttp;
    global $basepath;

    $av = dbRow("SELECT avatar, username, gender FROM users WHERE record_num = '$id'", true, 3600, "getUserAvatar.$id");
    if ($av['avatar'] != '' && file_exists("$basepath/media/misc/{$av['avatar']}")) {
        return $basehttp . "/media/misc/" . $av['avatar'];
    } else {
        if (strtolower($av['gender']) == 'male') {
            return $basehttp . "/core/images/avatar_male.png";
        } elseif (strtolower($av['gender']) == 'female') {
            return $basehttp . "/core/images/avatar_female.png";
        } else {
            return $basehttp . "/core/images/avatar_default.png";
        }
    }
}

function showPornstar2($contentID, $prefix, $suffix, $thumb = true) {
    global $basehttp;
    global $video_cache_time;
    $result = dbQuery("SELECT pornstar FROM `content_pornstars` WHERE content = '" . $contentID . "'", true, $video_cache_time);

    foreach ($result as $row) {
        $pornstars_array[] = $row['pornstar'];
    }
    if (!empty($pornstars_array)) {
        $links = "";
        $thumb_img = "";
        foreach ($pornstars_array as $p) {
            $query = dbQuery("SELECT name, thumb FROM pornstars WHERE record_num = $p", true);
            if ($thumb) {
                $thumb_img = "";
                $thumb_img .= '<span class="tag-list__thumb"><img class="tag-list__thumb-img" src="';
                if ($query[0]['thumb'] != '') {
                    $thumb_img .= $basehttp . "/media/misc/" . $query[0]['thumb'];
                } else {
                    $thumb_img .= $basehttp . "/core/images/catdefault.jpg";
                }
                $thumb_img .= '" alt="' . $query[0]['name'] . '"></span>';
            }

            $links .= $prefix . '<a class="tag-list__link-img" title="' . $query[0]['name'] . '" href="' . generateUrl('pornstar', $query[0]['name'], $p) . '">' . $thumb_img . '<span class="tag-list__label">' . $query[0]['name'] . '</span></a>' . $suffix;
        }
    } else {
        $links = false;
    }
    return $links;
}

function buildTags2($var, $prefix = '', $suffix = '', $split = ',') {
    global $basehttp;
    $var = str_replace(', ', ',', $var);
    $tags = explode(',', $var);
    foreach ($tags as $i) {
        $i2 = str_replace(' ', '-', $i);
        if (!empty($i)) {
            $string .= $prefix."<a class=\"tag-list__link\" href=\"$basehttp/search/$i2/page1.html\">$i</a>$suffix$split ";
        }
    }
    return substr($string, 0, -2);
}

//builds list of channels for specific videos
function buildChannels2($id, $prefix = '', $suffix = '', $split ='', $mode = 'channels') {
    global $currentLang;
    global $basehttp;
    global $config;

    if ($currentLang  && strtoupper($currentLang) != $config['core_language']) {
        $langSelect = ", niches_languages.name AS langName";
        $langJoin = " RIGHT JOIN niches_languages ON niches_languages.niche = niches.record_num";
        $langWhere = " AND niches_languages.language = '$currentLang'";
    }

    $cresult = dbQuery("SELECT niches.* $langSelect FROM niches $langJoin WHERE niches.record_num IN (SELECT content_niches.niche FROM content_niches WHERE content_niches.content = '$id') $langWhere GROUP BY niches.record_num ORDER BY name ASC");
    foreach ($cresult as $crow) {
        if ($crow['langName']) {
            $crow['name'] = $crow['langName'];
        }
        $categoriesIn .= $crow[record_num] . ',';
        if ($mode == 'channels') {
            $catstring .= $prefix."<a class=\"tag-list__link\" href='$basehttp/channels/" . $crow[record_num] . "/" . str_replace(' ', '-', strtolower($crow[name])) . "/page1.html'>$crow[name]</a>$suffix$split";
        } else {
            $catstring .= $prefix."<a class=\"tag-list__link\" href='$basehttp/search/" . $crow[record_num] . "/" . str_replace(' ', '-', strtolower($crow[name])) . "/page1.html'>$crow[name]</a>$suffix$split";
        }
    }
    return substr($catstring, 0, -2);
}