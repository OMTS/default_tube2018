<?php

$_SESSION['_message_context'] = 'default';

function setMessageContext($context) {
    $_SESSION['_message_context'] = $context;
}

function setMessage($message, $type = 'success', $print = false, $classes = array(), $debug_only = false, $prefix = '', $suffix = '') {
    global $config;
    if ($debug_only === true && !$config['development_mode']) {
        return;
    }
    $_classes = (array) $classes;
    array_unshift($_classes, $type);
    $_type = preg_replace('/[^a-z]/', '_', strtolower($type));
    if ($print) {
        return "$prefix<div class=\"notification " . implode(' ', $_classes) . "\">$message</div>$suffix";
    } else {
        $_SESSION['messages'][$_SESSION['_message_context']][$_type][] = $message;
    }
}

function getMessages($print = true, $type = null, $context = 'default') {
    $message_group = array();
    if (!empty($_SESSION['messages'][$context])) {
        if ($print === true) {
            if (array_key_exists('error', $_SESSION['messages'][$context])) {
                addJs('$().ready(function() { setTimeout(function() { $(".notification.error").addClass("blink"); $("body,html").animate({ "scrollTop" : $(".notification.error").first().offset().top - 50}, 500, "easeOutExpo"); }, 500); });', 'inline', 'closure');
            }
            if ($type !== null && isset($_SESSION['messages'][$context][$type])) {
                $messages_array = array();
                foreach ($_SESSION['messages'][$context][$type] as $message) {
                    $messages_array[] = "$message<br>";
                }
                if (count($messages_array) > 0) {
                    $message_group[] = "<div class=\"notification $type\">" . substr(implode("", $messages_array), 0, -4) . "</div>";
                }
                $_SESSION['messages'][$context][$type] = array();
            } else {
                foreach ($_SESSION['messages'][$context] as $type => $messages) {
                    $messages_array = array();
                    foreach ($messages as $index => $message) {
                        $messages_array[] = "$message<br>";
                    }
                    if (count($messages_array) > 0) {
                        $message_group[] = "<div class=\"notification $type\">" . substr(implode("", $messages_array), 0, -4) . "</div>";
                    }
                }
                $_SESSION['messages'][$context] = array();
            }
            return implode("", $message_group) . getJs();
        } else {
            if ($type !== null) {
                return count($_SESSION['messages'][$context][$type]);
            } else {
                return count($_SESSION['messages'][$context]);
            }
        }
    }
}

function addJs($js, $type = 'inline', $mode = 'head') {
    if (!getJs(false, $mode)) {
        $_SESSION['scripts'][$mode] = array();
    }
    switch ($type) {
        case 'inline':
            $_SESSION['scripts'][$mode][] = $js;
            break;
        case 'file':
            $_SESSION['js_scripts'][] = $js;
            break;
    }
}

function getJs($print = true, $mode = 'head', $files = false) {
    if ($files === true && !empty($_SESSION['js_scripts'])) {
        if ($print === true) {
            $html = '';
            foreach ($_SESSION['js_scripts'] as $js) {
                $html .= '<script src="' . $basehttp . '/' . $js . '"></script>' . "\n";
            }
            $_SESSION['js_scripts'] = array();
            return $html;
        } else {
            return count($_SESSION['js_scripts']);
        }
    } else if (!empty($_SESSION['scripts'][$mode])) {
        if ($print === true) {
            $_SESSION['scripts'][$mode] = array_unique($_SESSION['scripts'][$mode]);
            $scripts = '';
            if ($mode === 'all') {
                foreach ($_SESSION['scripts'] as $_key => $_scripts) {
                    foreach ($_scripts as $js) {
                        $scripts .= "$js";
                    }
                    $_SESSION['scripts'][$_key] = array();
                }
            } else {
                foreach ($_SESSION['scripts'][$mode] as $js) {
                    $scripts .= "$js";
                }
                $_SESSION['scripts'][$mode] = array();
            }
            return $scripts ? "<script>$scripts</script>" : '';
        } else {
            return count($_SESSION['scripts'][$mode]);
        }
    }
}

function addCss($css, $type = 'inline') {
    if (!getCss(false)) {
        $_SESSION['stylesheets'] = array();
    }
    if ($css == "") {
        return;
    }
    switch ($type) {
        case 'inline':
            $_SESSION['stylesheets'][] = $css;
            break;
        case 'file':
            break;
    }
}

function getCss($print = true) {
    if (!empty($_SESSION['stylesheets'])) {
        if ($print) {
            $stylesheets = '';
            foreach ($_SESSION['stylesheets'] as $css) {
                $stylesheets .= "$css\r\n";
            }
            $_SESSION['stylesheets'] = array();
            return $stylesheets ? "<style media=\"all\" type=\"text/css\">\r\n$stylesheets\r\n</style>" : '';
        } else {
            return count($_SESSION['stylesheets']);
        }
    }
}

//get difference between two dates
function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {
    if (!$using_timestamps) {
        $datefrom = strtotime($datefrom, 0);
        $dateto = strtotime($dateto, 0);
    }
    $difference = $dateto - $datefrom; // Difference in seconds

    if ($difference < 86400 && $difference >= 3600) {
        $datediff = floor($difference / 3600);
        if ($datediff > 1) {
            $date_string = _t('%count hrs', array('%count' => $datediff));
        } else {
            $date_string = _t('1 hr');
        }
    } elseif ($difference < 3600) {
        $datediff = floor($difference / 60);
        if ($datediff > 1) {
            $date_string = _t('%count mins', array('%count' => $datediff));
        } else {
            $date_string = _t('1 min');
        }
    } else {
        $datediff = floor($difference / 86400);
        if ($datediff > 1) {
            $date_string = _t('%count days', array('%count' => $datediff));
        } else {
            $date_string = _t('1 day');
        }
    }
    return $date_string;
}

function showRandom($template, $amount, $cacheName = 'random', $type = 0, $slider = false, $slider_per_page = 8, $userid = false) {
    global $basehttp;
    global $basepath;
    global $gallery_url;
    global $template_path;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $bad;
    global $good;
    global $results_per_row;
    global $template_url;

    if (is_numeric($userid)) {
        $filterAnd = "AND content.submitter = '$userid'";
    }

    $result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views FROM content WHERE enabled = 1 AND photos = '$type' $filterAnd ORDER BY RAND() LIMIT 0,$amount");

    foreach ($result as $row) {
        if ($row['photos'] == 1) {
            $grow = dbRow("SELECT filename FROM images WHERE record_num = '$row[thumbnail]'", true);
            $row[thumbfile] = $grow[filename];
        }
        $farray[] = $row;
    }

    if (is_array($farray)) {
        $i = 0;
        $j = 0;
        $maxRes = count($farray);
        foreach ($farray as $row) {
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }

            if ($slider) {
                if ($j % $slider_per_page == 0) {
                    echo '<div class="window">';
                }
            }
            if ($row['photos'] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/' . $template);
            }
            if ($slider) {
                if (($j + 1) % $slider_per_page == 0 || $j == ($maxRes - 1)) {
                    echo '</div>';
                }
            }
            ++$j;
        }
    }
}

//generate lighttpd hash
function getMediaLink($filename) {
    global $modsec_secret; // example: s3cr3tk3y (from your lighttpd config)
    global $modsec_url; // example: http://media.asfd.com/dl/ with trailing slash
    $filename = rawurldecode($filename);
    $f = "/" . $filename;
    $t = time();
    $t_hex = sprintf("%08x", $t);
    $m = md5($modsec_secret . $f . $t_hex);
    $link = $modsec_url . $m . '/' . $t_hex . $f;
    return $link;
}

function buildTags($var, $split = ',') {
    global $basehttp;
    $var = str_replace(', ', ',', $var);
    $tags = explode(',', $var);
    foreach ($tags as $i) {
        $i2 = str_replace(' ', '-', $i);
        if (!empty($i)) {
            $string .= "<a href=\"$basehttp/search/$i2/page1.html\">$i</a>$split ";
        }
    }
    return substr($string, 0, -2);
}

//shows related videos
function showRelated($related, $template, $exclude, $amount) {
    global $thumb_url;
    global $basehttp;
    global $thumbwidth;
    global $results_per_row;
    global $thumbheight;
    global $gallery_url;
    global $template_path;
    global $template_url;
    global $bad;
    global $good;
    global $currentLang;
    global $basepath;
    global $dblink;
    global $config;

    if ($currentLang && strtoupper($currentLang) != $config['core_language']) {
        $langSelect .= ", `content_languages`.`title` AS `langTitle`, `content_languages`.`description` AS `langDescription`, `content_languages`.`keywords` AS `langKeywords`";
        $langJoin .= " LEFT JOIN `content_languages` ON `content_languages`.`content` = `content`.`record_num`  AND `content_languages`.`language` = '$currentLang'";
        $langWhere .= "";
    }
    if ($_SESSION['filterContent']) {
        if ($_SESSION['filterContent'] == 'photosOnly') {
            $filterAnd = "AND photos = 1";
        }
        if ($_SESSION['filterContent'] == 'videosOnly') {
            $filterAnd = "AND photos = 0";
        }
    }

    $string = mysqli_real_escape_string($dblink, $string);
    if ($related) {
        $result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views $langSelect FROM content $langJoin WHERE enabled = 1 $langWhere $filterAnd AND record_num != '$exclude' AND record_num IN ($related) ORDER BY FIELD(record_num,$related) LIMIT 0,$amount");
    } else {
		$tmp = dbQuery("SELECT content_niches.niche FROM content_niches WHERE content = '$exclude'");
		foreach($tmp as $i) { 
			$tmp2[] = $i['niche']; 
		}
		if(count($tmp) > 0) { 
			$cats = implode(",",$tmp2); 
			$result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views $langSelect FROM content $langJoin WHERE enabled = 1 $langWhere $filterAnd AND record_num != '$exclude' AND content.record_num IN (SELECT content_niches.content FROM content_niches WHERE niche IN ($cats)) ORDER BY encoded_date DESC LIMIT 0,$amount");
		} else { 
			$result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views $langSelect FROM content $langJoin WHERE enabled = 1 $langWhere $filterAnd AND record_num != '$exclude' ORDER BY encoded_date DESC LIMIT 0,$amount");
		}
    }

    foreach ($result as $row) {
        $farray[] = $row;
    }

    if (is_array($farray)) {
        $i = 0;
        foreach ($farray as $row) {
            if ($row['langTitle']) {
                $row['title'] = $row['langTitle'];
            }
            if ($row['langDescription']) {
                $row['description'] = $row['langDescription'];
            }
            if ($row['langKeywords']) {
                $row['keywords'] = $row['langKeywords'];
            }
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }
            if ($row[photos] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/' . $template);
            }
        }
    }
}

//builds list of channels for specific videos
function buildChannels($id, $mode = 'channels') {
    global $currentLang;
    global $basehttp;
    global $config;

    if ($currentLang  && strtoupper($currentLang) != $config['core_language']) {
        $langSelect = ", niches_languages.name AS langName";
        $langJoin = " RIGHT JOIN niches_languages ON niches_languages.niche = niches.record_num";
        $langWhere = " AND niches_languages.language = '$currentLang'";
    }

    $cresult = dbQuery("SELECT niches.* $langSelect FROM niches $langJoin WHERE niches.record_num IN (SELECT content_niches.niche FROM content_niches WHERE content_niches.content = '$id') $langWhere GROUP BY niches.record_num ORDER BY name ASC");
    foreach ($cresult as $crow) {
        if ($crow['langName']) {
            $crow['name'] = $crow['langName'];
        }
        $categoriesIn .= $crow[record_num] . ',';
        if ($mode == 'channels') {
            $catstring .= "<a href='$basehttp/channels/" . $crow[record_num] . "/" . str_replace(' ', '-', strtolower($crow[name])) . "/page1.html' class='link12' >$crow[name]</a>, ";
        } else {
            $catstring .= "<a href='$basehttp/search/" . $crow[record_num] . "/" . str_replace(' ', '-', strtolower($crow[name])) . "/page1.html' class='link12' >$crow[name]</a>, ";
        }
    }
    return substr($catstring, 0, -2);
}

//resizes embedded video html code to a specified width and height
function resizeEmbedded($subject, $width, $height) {
    if (stripos($subject, '&lt;') !== false) {
        $subject = html_entity_decode($subject);
    }
    $subject = str_replace("'", '"', $subject);
    $pattern = "/height=\"[0-9]*\"/";
    $subject = preg_replace($pattern, "height='" . $height . "'", $subject);
    $pattern = "/width=\"[0-9]*\"/";
    $subject = preg_replace($pattern, "width='" . $width . "'", $subject);
    return $subject;
}

//shows a list of channels, for instance: showChannels('<li>','</li>'); 
function showChannels($pre, $post, $counter = false) {
    global $basehttp;
    global $basepath;
    global $currentLang;
    global $template_url;
    global $config;

    if ($currentLang  && strtoupper($currentLang) != $config['core_language']) {
         $langSelect = ", `niches_languages`.`name` AS `langName`";
        $langJoin = " LEFT JOIN `niches_languages` ON `niches_languages`.`niche` = `niches`.`record_num` AND `niches_languages`.`language` = '$currentLang'";
        $langWhere = " ";
    }

    $result = dbQuery("SELECT niches.* $langSelect FROM niches $langJoin WHERE 1=1 $langWhere ORDER BY name ASC");

    if (is_array($result)) {
        foreach ($result as $row) {
            $c = "";

            if ($counter) {
                $count_content = dbValue("SELECT COUNT(`content`) AS `count` FROM `content_niches` LEFT JOIN `content` ON `content_niches`.`content` = `content`.`record_num` WHERE `content_niches`.`niche` = '$row[record_num]' AND `content`.`approved` = 2 AND `content`.`enabled` = 1", 'count', true);
                $c = '<span class="counter">' . $count_content . '</span>';
            }
            $class = ($counter) ? 'has-counter' : "";

            if ($row['langName']) {
                $row['name'] = $row['langName'];
            }
            if ($_GET['channel'] == $row['record_num']) {
                $class .= " active";
            }
            $link = generateUrl('channel', $row['name'], $row['record_num']);
            echo $pre . "<a title='$row[name]' href='$link' class='" . $class . "'><span class='sub-label'>$row[name]</span>$c</a>" . $post;
        }
    }
}

function showChannelsBlocks($args = array()) {
    global $basehttp, $basepath, $currentLang;
    global $template_url, $template_path;
    global $misc_path, $misc_url;
    global $config;
    
    $args += array(
        'type' => 'navigation',
        'template' => 'template.channel_item.php',
        'limit' => 8,
    );
    
    if ($currentLang  && strtoupper($currentLang) != $config['core_language']) {
        $langSelect = ", `niches_languages`.`name` AS `langName`";
        $langJoin = " LEFT JOIN `niches_languages` ON `niches_languages`.`niche` = `niches`.`record_num` AND `niches_languages`.`language` = '$currentLang'";
        $langWhere = " ";
    }
	
		
		
    switch ($args['type']) {
        case 'navigation':
		
            $results = dbQuery("SELECT `niches`.* $langSelect FROM `niches` $langJoin WHERE 1=1 AND `flag_navigation` = 1 $langWhere ORDER BY `name` LIMIT " . (int) $args['limit'], true, 300);
            break;
    }
    
    if (is_array($results)) {
        foreach ($results as $row) {
            include "$template_path/$args[template]";
        }
    }
}

//shows a list of channels, for instance: showChannels('<li>','</li>'); 
function showPaysites($pre, $post) {
    global $cache_path;
    global $overall_cache_time;
    global $basehttp;
    global $basepath;
    global $currentLang;
    global $template_url;


    $result = dbQuery("SELECT * FROM paysites ORDER BY name ASC", true);
    if (is_array($result)) {
        foreach ($result as $row) {
            if ($row['langName']) {
                $row['name'] = $row['langName'];
            }
            $link = generateUrl('paysite', $row['name'], $row['record_num']);
            echo $pre . "<a title='$row[name]' href='$link'><span class='sub-label'>$row[name]</span></a>" . $post;
        }
    }
}

//truncates a string to a certain length - example: "truncate($string,50)" would return the first 50 characters and then "..."
function truncate($tekst, $ile) {
    $znaki = strlen($tekst);
    if ($znaki >= $ile) {
        $tnij = substr($tekst, 0, $ile);
        $skrocone = $tnij . "...";
    } else {
        $skrocone = $tekst;
    }
    return $skrocone;
}

//prints a tag cloud
function printTagCloud($tags) {
    arsort($tags);
    $max_size = 18; // max font size in pixels
    $min_size = 10; // min font size in pixels
    $max_qty = max(array_values($tags));
    $min_qty = min(array_values($tags));
    $spread = $max_qty - $min_qty;
    if ($spread == 0) {
        $spread = 1;
    }
    $step = ($max_size - $min_size) / ($spread);
    foreach ($tags as $key => $value) {
        $size = round($min_size + (($value - $min_qty) * $step));
        if ($size == 18) {
            $color = 'color: #ff1c04;';
        } else {
            unset($color);
        }
        echo '<a href="/search/' . $key . '/" style="' . $color . ' font-size: ' . $size . 'px" >' . $key . '</a> ';
    }
}

//related to above printTagCloud function
function tagCloud() {
    $result = dbQuery("SELECT * FROM keywords ORDER BY amount DESC LIMIT 0,50", false);
    foreach ($result as $row) {
        $tags[$row[word]] = $row[amount];
    }
    printTagCloud($tags);
}

//gets the url to a thumbnail (or large thumbnail)
function getThumbUrl($orig_filename, $filename, $number, $big = false) {
    global $thumb_url;
    $dirname = str_replace('.flv', '', $orig_filename);
    $subdir = $filename[0] . '/' . $filename[1] . '/' . $filename[2] . '/' . $filename[3] . '/' . $filename[4] . '/';
    $dirname = $subdir . rawurlencode($dirname);
    if ($big) {
        $overlay = $thumb_url . '/' . $dirname . '/' . rawurlencode($orig_filename) . '-' . $number . 'b.jpg';
    } else {
        $overlay = $thumb_url . '/' . $dirname . '/' . rawurlencode($orig_filename) . '-' . $number . '.jpg';
    }
    return $overlay;
}

//builds the subsorting links used to sort searches by newest/mostviewed/etc, most viewed by last day, week, month and so forth.
function buildSubSortLinks($mode) {
    global $basehttp;
    global $q;
    global $catname;
    $catname = strtolower($catname);
    if ($_GET[dateRange] == 'day') {
        if ($mode == 'select') {
            $day = 'selected="selected"';
        } else {
            $day = "class='subSortBold'";
        }
    } elseif ($_GET[dateRange] == 'week') {
        if ($mode == 'select') {
            $week = 'selected="selected"';
        } else {
            $week = "class='subSortBold'";
        }
    } elseif ($_GET[dateRange] == 'month') {
        if ($mode == 'select') {
            $month = 'selected="selected"';
        } else {
            $month = "class='subSortBold'";
        }
    } else {
        if ($mode == 'select') {
            $alltime = 'selected="selected"';
        } else {
            $alltime = "class='subSortBold'";
        }
    }

    if ($_GET[sortby] == 'views') {
        $orderBy = 'views DESC';
        if ($mode == 'select') {
            $views = 'selected="selected"';
        } else {
            $views = "class='subSortBold'";
        }
    } elseif ($_GET[sortby] == 'rating') {
        $orderBy = 'rating DESC';
        if ($mode == 'select') {
            $rating = 'selected="selected"';
        } else {
            $rating = "class='subSortBold'";
        }
    } elseif ($_GET[sortby] == 'longest') {
        $orderBy = 'length DESC';
        if ($mode == 'select') {
            $length = 'selected="selected"';
        } else {
            $length = "class='subSortBold'";
        }
    } elseif ($_GET[sortby] == 'relevancy') {
        $orderBy = 'score DESC';
        if ($mode == 'select') {
            $score = 'selected="selected"';
        } else {
            $score = "class='subSortBold'";
        }
    } elseif ($_GET[sortby] == 'newest') {
        $orderBy = 'date_published DESC';
        if ($mode == 'select') {
            $newest = 'selected="selected"';
        } else {
            $newest = "class='subSortBold'";
        }
    } else {
        if ($_GET[mode] == 'search') {
            $orderBy = 'score DESC';
            if ($mode == 'select') {
                
            } else {
                $score = "class='subSortBold'";
            }
        } else {
            $orderBy = 'date_published DESC';
            if ($mode == 'select') {
                $newest = 'selected="selected"';
            } else {
                $newest = "class='subSortBold'";
            }
        }
    }

    if ($_GET[mode] == 'most-viewed') {
        if ($mode == 'default') {
            echo "<a $day href='$basehttp/most-viewed/day/'>" . _t('Last 24 hours') . "</a> <a $week href='$basehttp/most-viewed/week/'>" . _t('Last Week') . "</a> <a $month href='$basehttp/most-viewed/month/'>" . _t('Last Month') . "</a> <a $alltime href='$basehttp/most-viewed/'>" . _t('All Time') . "</a>";
        } else {
            echo '<select name="sortby" id="mainSortBy">';
            echo "<option $day value='$basehttp/most-viewed/day/'>" . _t('Last 24 hours') . "</option> <option $week value='$basehttp/most-viewed/week/'>" . _t('Last Week') . "</option> <option $month value='$basehttp/most-viewed/month/'>" . _t('Last Month') . "</option> <option $alltime value='$basehttp/most-viewed/'>" . _t('All Time') . "</option>";
            echo '</select>';
        }
    }
    if ($_GET[mode] == 'top-rated') {
        if ($mode == 'default') {
            echo "<a $day href='$basehttp/top-rated/day/'>" . _t('Last 24 hours') . "</a> <a $week href='$basehttp/top-rated/week/'>" . _t('Last Week') . "</a> <a $month href='$basehttp/top-rated/month/'>" . _t('Last Month') . "</a> <a $alltime href='$basehttp/top-rated/'>" . _t('All Time') . "</a>";
        } else {
            echo '<select name="sortby" id="mainSortBy">';
            echo "<option $day value='$basehttp/top-rated/day/'>" . _t('Last 24 hours') . "</option> <option $week value='$basehttp/top-rated/week/'>" . _t('Last Week') . "</option> <option $month value='$basehttp/top-rated/month/'>" . _t('Last Month') . "</option> <option $alltime value='$basehttp/top-rated/'>" . _t('All Time') . "</option>";
            echo '</select>';
        }
    }
    if ($_GET[mode] == 'longest') {
        if ($mode == 'default') {
            echo "<a $day href='$basehttp/longest/day/'>" . _t('Last 24 hours') . "</a> <a $week href='$basehttp/longest/week/'>" . _t('Last Week') . "</a> <a $month href='$basehttp/longest/month/'>" . _t('Last Month') . "</a> <a $alltime href='$basehttp/longest/'>" . _t('All Time') . "</a>";
        } else {
            echo '<select name="sortby" id="mainSortBy">';
            echo "<option $day value='$basehttp/longest/day/'>" . _t('Last 24 hours') . "</option> <option $week value='$basehttp/longest/week/'>" . _t('Last Week') . "</option> <option $month value='$basehttp/longest/month/'>" . _t('Last Month') . "</option> <option $alltime value='$basehttp/longest/'>" . _t('All Time') . "</option>";
            echo '</select>';
        }
    }
    if ($_GET[mode] == 'search' && ($_GET[type] == 'videos' || !$_GET[type])) {
        if ($mode == 'default') {
            echo "<a $score href='$basehttp/search/videos/$q/'>" . _t('Relevancy') . "</a> <a $newest href='$basehttp/search/videos/$q/newest/'>" . _t('Most Recent') . "</a> <a $rating href='$basehttp/search/videos/$q/rating/'>" . _t('Top Rated') . "</a> <a $views href='$basehttp/search/videos/$q/views/'>" . _t('Most Viewed') . "</a> <a $length href='$basehttp/search/videos/$q/longest/'>" . _t('Longest') . "</a>";
        } else {
            echo '<select name="sortby" id="mainSortBy">';
            echo "<option $score value='$basehttp/search/videos/$q/'>" . _t('Relevancy') . "</option> <option $newest value='$basehttp/search/videos/$q/newest/'>" . _t('Most Recent') . "</option> <option $rating value='$basehttp/search/videos/$q/rating/'>" . _t('Top Rated') . "</option> <option $views value='$basehttp/search/videos/$q/views/'>" . _t('Most Viewed') . "</option> <option $length value='$basehttp/search/videos/$q/longest/'>" . _t('Longest') . "</option>";
            echo '</select>';
        }
    }
    if ($_GET[mode] == 'channel') {
        if ($mode == 'default') {
            echo "<a $newest href='$basehttp/channels/$_GET[channel]/$catname/newest/'>" . _t('Most Recent') . "</a> <a $rating href='$basehttp/channels/$_GET[channel]/$catname/rating/'>" . _t('Top Rated') . "</a> <a $views href='$basehttp/channels/$_GET[channel]/$catname/views/'>" . _t('Most Viewed') . "</a> <a $length href='$basehttp/channels/$_GET[channel]/$catname/longest/'>" . _t('Longest') . "</a>";
        } else {
            echo '<select name="sortby" id="mainSortBy">';
            echo "<option $newest value='$basehttp/channels/$_GET[channel]/$catname/newest/'>" . _t('Most Recent') . "</option> <option $rating value='$basehttp/channels/$_GET[channel]/$catname/rating/'>" . _t('Top Rated') . "</option> <option $views value='$basehttp/channels/$_GET[channel]/$catname/views/'>" . _t('Most Viewed') . "</option> <option $length value='$basehttp/channels/$_GET[channel]/$catname/longest/'>" . _t('Longest') . "</option>";
            echo '</select>';
        }
    }
}

if (!function_exists('buildPornstars')) {

    function buildPornstars($id) {
        global $basehttp;

        $bad = array('?', '!', ' ', '&', '*', '$', '#', '@');
        $good = array('', '', '-', '', '', '', '', '');

        $cresult = dbQuery("SELECT pornstars.name, pornstars.record_num FROM pornstars, content_pornstars WHERE content_pornstars.content = '$id' AND content_pornstars.pornstar = pornstars.record_num GROUP BY pornstars.record_num ORDER BY name ASC");
        foreach ($cresult as $crow) {
            $link = "$basehttp/pornstars/" . strtolower(str_replace($bad, $good, $crow[name])) . "-" . $crow[record_num] . ".html";

            $catstring .= "<a href='$link' style='text-decoration:none;'><strong>$crow[name]</strong></a>, ";
        }
        return substr($catstring, 0, -2);
    }

}


if (!function_exists('buildPornstarsAdmin')) {

    function buildPornstarsAdmin($id) {
        global $basehttp;

        $bad = array('?', '!', ' ', '&', '*', '$', '#', '@');
        $good = array('', '', '-', '', '', '', '', '');

        $cresult = dbQuery("SELECT pornstars.name, pornstars.record_num FROM pornstars, content_pornstars WHERE content_pornstars.content = '$id' AND content_pornstars.pornstar = pornstars.record_num GROUP BY pornstars.record_num ORDER BY name ASC");
        foreach ($cresult as $crow) {
            $link = "$basehttp/admin/edit_model.php?id=$crow[record_num]";

            $catstring .= "<a href='$link' style='text-decoration:none;'><strong>$crow[name]</strong></a>, ";
        }
        return substr($catstring, 0, -2);
    }

}

if (!function_exists('buildModels')) {

    function buildModels($id) {
        global $basehttp;

        $bad = array('?', '!', ' ', '&', '*', '$', '#', '@');
        $good = array('', '', '-', '', '', '', '', '');

        $cresult = dbQuery("SELECT pornstars.name, pornstars.record_num FROM pornstars, content_pornstars WHERE content_pornstars.content = '$id' AND content_pornstars.pornstar = pornstars.record_num GROUP BY pornstars.record_num ORDER BY name ASC", false);
        foreach ($cresult as $crow) {
            $link = "$basehttp/models/" . strtolower(str_replace($bad, $good, $crow[name])) . "-" . $crow[record_num] . ".html";

            $catstring .= "<a href='$link' style='text-decoration:none;'><strong>$crow[name]</strong></a>, ";
        }
        return substr($catstring, 0, -2);
    }

}

//show being watched
function showBeingWatched($template, $amount, $cacheName = 'random', $type = 0, $slider = false, $slider_per_page = 8) {
    global $basehttp;
    global $basepath;
    global $gallery_url;
    global $template_path;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $picthumbwidth;
    global $picthumbheight;
    global $bad;
    global $good;
    global $results_per_row;
    global $template_url;
    global $currentLang;
    global $dblink;
    global $config;

    if ($currentLang  && strtoupper($currentLang) != $config['core_language']) {
        $langSelect .= ", `content_languages`.`title` AS `langTitle`, `content_languages`.`description` AS `langDescription`, `content_languages`.`keywords` AS `langKeywords`";
        $langJoin .= " LEFT JOIN `content_languages` ON `content_languages`.`content` = `content`.`record_num`  AND `content_languages`.`language` = '$currentLang'";
        $langWhere .= "";
    }


    $result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views $langSelect FROM content $langJoin RIGHT JOIN watch_log ON content.record_num = watch_log.content WHERE content.enabled = 1 $langWhere $filterAnd AND content.photos = '$type' ORDER BY `time` DESC LIMIT 0,$amount");
    foreach ($result as $row) {
        if ($row['photos'] == 1) {
            $grow = dbRow("SELECT filename FROM images WHERE record_num = '$row[thumbnail]'", true);
            $row[thumbfile] = $grow[filename];
        }
        $farray[] = $row;
    }

    if (is_array($farray)) {
        $i = 0;
        $j = 0;
        $maxRes = count($farray);
        foreach ($farray as $row) {
            if ($row['langTitle']) {
                $row['title'] = $row['langTitle'];
            }
            if ($row['langDescription']) {
                $row['description'] = $row['langDescription'];
            }
            if ($row['langKeywords']) {
                $row['keywords'] = $row['langKeywords'];
            }
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }

            if ($slider) {
                if ($j % $slider_per_page == 0) {
                    echo '<div class="window">';
                }
            }
            if ($row['type'] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/' . $template);
            }
            if ($slider) {
                if (($j + 1) % $slider_per_page == 0 || $j == ($maxRes - 1)) {
                    echo '</div>';
                }
            }
            ++$j;
        }
    }
}


function userRecentUploads($userid, $limit, $mode = 'video') {
    global $template_path;
    global $results_per_row;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $picthumbwidth;
    global $picthumbheight;
    global $gallery_url;
    global $template_url;

    if ($mode == 'video') {
        $andPhotos = 'AND photos = 0';
    } else {
        $andPhotos = 'AND photos = 1';
    }

    $result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) AS views,  (SELECT images.filename FROM images WHERE images.record_num = content.thumbnail) AS thumbfile FROM content WHERE enabled = 1 AND content.submitter = '$userid' $andPhotos ORDER BY content.encoded_date DESC LIMIT 0,$limit", true);
    if (count($result) == 0) {
        echo "<p>User has not uploaded anything!</p>";
    } else {
        $i = 0;
        foreach ($result as $row) {
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }
            if ($row['photos'] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/template.content_item.php');
            }
        }
    }
}

function getUsersFriends($userid, $limit) {
    global $template_path;
    global $basepath;
    global $basehttp;
    global $results_per_row;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $picthumbwidth;
    global $picthumbheight;
    global $gallery_url;
    global $template_url;


    $result = dbQuery("SELECT   users.record_num, users.username, users.avatar, users.gender FROM friends, users WHERE  friends.friend = '$userid' AND users.record_num =  friends.user AND approved = 1 ORDER BY username ASC LIMIT 0,$limit", true);
    if (count($result) < 1) {
        echo "<p>" . _t('You do not have any friends added!') . "</p>";
    } else {
        $i = 0;
        foreach ($result as $row) {
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }

            include($template_path . '/template.member_item.php');
        }
    }
}

function getModelsContent($modelid, $type = 0) {
    global $template_path;
    global $results_per_row;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $picthumbwidth;
    global $picthumbheight;
    global $gallery_url;
    global $template_url;
    global $currentLang;
    global $config;

    if ($currentLang && strtoupper($currentLang) != $config['core_language']) {
        $langSelect .= ", `content_languages`.`title` AS `langTitle`, `content_languages`.`description` AS `langDescription`, `content_languages`.`keywords` AS `langKeywords`";
        $langJoin .= " LEFT JOIN `content_languages` ON `content_languages`.`content` = `content`.`record_num`  AND `content_languages`.`language` = '$currentLang'";
        $langWhere .= "";
    }

    $result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) AS views, (SELECT images.filename FROM images WHERE images.record_num = content.thumbnail) AS thumbfile $langSelect FROM content $langJoin WHERE enabled = 1 $langWhere AND photos = '$type' AND record_num IN (SELECT content FROM content_pornstars WHERE pornstar = '$modelid')", true);
    if (count($result) < 1) {
        echo "<div class='notification alert'><p>" . _t('There is no content for this model yet.') . "</p></div>";
    } else {
        $i = 0;
        foreach ($result as $row) {
            if ($row['langTitle']) {
                $row['title'] = $row['langTitle'];
            }
            if ($row['langDescription']) {
                $row['description'] = $row['langDescription'];
            }
            if ($row['langKeywords']) {
                $row['keywords'] = $row['langKeywords'];
            }
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }

            if ($row['photos'] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/template.content_item.php');
            }
        }
    }
}

function checkNav($item) {
    global $thisController;
    global $_GET;

    switch ($item) {
        case 'index':
            return ($thisController == 'index' && !$_GET['mode'] && !$_GET['page']) ? true : false;
            break;
        case 'most-recent':
            return ($thisController == 'index' && $_GET['mode'] == 'most-recent') ? true : false;
            break;
        case 'videos':
            return ($thisController == 'index' && $_GET['mode'] == 'videos') ? true : false;
            break;
        case 'top-rated':
            return ($thisController == 'index' && $_GET['mode'] == 'top-rated') ? true : false;
            break;
        case 'most-viewed':
            return ($thisController == 'index' && $_GET['mode'] == 'most-viewed') ? true : false;
            break;
        case 'longest':
            return ($thisController == 'index' && $_GET['mode'] == 'longest') ? true : false;
            break;
        case 'most-discussed':
            return ($thisController == 'index' && $_GET['mode'] == 'most-discussed') ? true : false;
            break;
        case 'random':
            return ($thisController == 'index' && $_GET['mode'] == 'random') ? true : false;
            break;
        case 'channels':
            return ($thisController == 'channels') ? true : false;
            break;
        case 'photos':
            return ($_GET['mode'] == 'photos') ? true : false;
            break;
        case 'pornstars':
            return ($thisController == 'pornstars') ? true : false;
            break;
        case 'paysites':
            return ($thisController == 'paysites') ? true : false;
            break;
        case 'members':
            return ($thisController == 'members') ? true : false;
            break;
        default:
            return false;
    }
}

function _showNewest($template, $amount, $cacheName = 'showNewest', $type = 0, $slider = false, $slider_per_page = 8, $userid = false) {
    global $basehttp;
    global $basepath;
    global $gallery_url;
    global $template_path;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $bad;
    global $good;
    global $results_per_row;
    global $template_url;
    global $currentLang;
    global $config;

    if ($currentLang && strtoupper($currentLang) != $config['core_language']) {
        $langSelect .= ", `content_languages`.`title` AS `langTitle`, `content_languages`.`description` AS `langDescription`, `content_languages`.`keywords` AS `langKeywords`";
        $langJoin .= " LEFT JOIN `content_languages` ON `content_languages`.`content` = `content`.`record_num`  AND `content_languages`.`language` = '$currentLang'";
        $langWhere .= "";
    }

    if (is_numeric($userid)) {
        $filterAnd = "AND content.submitter = '$userid'";
    }

    $result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views $langSelect FROM content $langJoin WHERE enabled = 1 $langWhere AND photos = '$type' $filterAnd ORDER BY encoded_date DESC LIMIT 0,$amount");
    foreach ($result as $row) {
        if ($row['photos'] == 1) {
            $grow = dbRow("SELECT filename FROM images WHERE record_num = '$row[thumbnail]'", true);
            $row[thumbfile] = $grow[filename];
        }
        $farray[] = $row;
    }

    if (is_array($farray)) {
        $i = 0;
        $j = 0;
        $maxRes = count($farray);
        foreach ($farray as $row) {
            if ($row['langTitle']) {
                $row['title'] = $row['langTitle'];
            }
            if ($row['langDescription']) {
                $row['description'] = $row['langDescription'];
            }
            if ($row['langKeywords']) {
                $row['keywords'] = $row['langKeywords'];
            }
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }

            if ($slider) {
                if ($j % $slider_per_page == 0) {
                    echo '<div class="window">';
                }
            }
            if ($row['photos'] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/' . $template);
            }
            if ($slider) {
                if (($j + 1) % $slider_per_page == 0 || $j == ($maxRes - 1)) {
                    echo '</div>';
                }
            }
            ++$j;
        }
    }
}

function _showMostViewed($template, $amount, $cacheName = 'showMostViewed', $type = 0, $slider = false, $slider_per_page = 8, $userid = false) {
    global $basehttp;
    global $basepath;
    global $gallery_url;
    global $template_path;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $bad;
    global $good;
    global $results_per_row;
    global $template_url;
    global $currentLang;
    global $config;

    if ($currentLang && strtoupper($currentLang) != $config['core_language']) {
        $langSelect .= ", `content_languages`.`title` AS `langTitle`, `content_languages`.`description` AS `langDescription`, `content_languages`.`keywords` AS `langKeywords`";
        $langJoin .= " LEFT JOIN `content_languages` ON `content_languages`.`content` = `content`.`record_num`  AND `content_languages`.`language` = '$currentLang'";
        $langWhere .= "";
    }
    if (is_numeric($userid)) {
        $filterAnd = "AND content.submitter = '$userid'";
    }


    $result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views $langSelect FROM content $langJoin WHERE enabled = 1 $langWhere AND photos = '$type' $filterAnd ORDER BY views DESC LIMIT 0,$amount");
    foreach ($result as $row) {
        if ($row['photos'] == 1) {
            $grow = dbRow("SELECT filename FROM images WHERE record_num = '$row[thumbnail]'", true);
            $row[thumbfile] = $grow[filename];
        }
        $farray[] = $row;
    }

    if (is_array($farray)) {
        $i = 0;
        $j = 0;
        $maxRes = count($farray);
        foreach ($farray as $row) {
            if ($row['langTitle']) {
                $row['title'] = $row['langTitle'];
            }
            if ($row['langDescription']) {
                $row['description'] = $row['langDescription'];
            }
            if ($row['langKeywords']) {
                $row['keywords'] = $row['langKeywords'];
            }
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }

            if ($slider) {
                if ($j % $slider_per_page == 0) {
                    echo '<div class="window">';
                }
            }
            if ($row['photos'] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/' . $template);
            }
            if ($slider) {
                if (($j + 1) % $slider_per_page == 0 || $j == ($maxRes - 1)) {
                    echo '</div>';
                }
            }
            ++$j;
        }
    }
}

function _showLongest($template, $amount, $cacheName = 'showLongest', $type = 0, $slider = false, $slider_per_page = 8, $userid = false) {
    global $basehttp;
    global $basepath;
    global $gallery_url;
    global $template_path;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $bad;
    global $good;
    global $results_per_row;
    global $template_url;
    global $currentLang;
    global $config;

    if ($currentLang && strtoupper($currentLang) != $config['core_language']) {
        $langSelect .= ", `content_languages`.`title` AS `langTitle`, `content_languages`.`description` AS `langDescription`, `content_languages`.`keywords` AS `langKeywords`";
        $langJoin .= " LEFT JOIN `content_languages` ON `content_languages`.`content` = `content`.`record_num`  AND `content_languages`.`language` = '$currentLang'";
        $langWhere .= "";
    }
    if (is_numeric($userid)) {
        $filterAnd = "AND content.submitter = '$userid'";
    }

    $result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views $langSelect FROM content $langJoin WHERE enabled = 1 $langWhere AND photos = '$type' $filterAnd ORDER BY length DESC LIMIT 0,$amount");
    foreach ($result as $row) {
        if ($row['photos'] == 1) {
            $grow = dbRow("SELECT filename FROM images WHERE record_num = '$row[thumbnail]'", true);
            $row[thumbfile] = $grow[filename];
        }
        $farray[] = $row;
    }

    if (is_array($farray)) {
        $i = 0;
        $j = 0;
        $maxRes = count($farray);
        foreach ($farray as $row) {
            if ($row['langTitle']) {
                $row['title'] = $row['langTitle'];
            }
            if ($row['langDescription']) {
                $row['description'] = $row['langDescription'];
            }
            if ($row['langKeywords']) {
                $row['keywords'] = $row['langKeywords'];
            }
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }

            if ($slider) {
                if ($j % $slider_per_page == 0) {
                    echo '<div class="window">';
                }
            }
            if ($row['photos'] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/' . $template);
            }
            if ($slider) {
                if (($j + 1) % $slider_per_page == 0 || $j == ($maxRes - 1)) {
                    echo '</div>';
                }
            }
            ++$j;
        }
    }
}

function _showMostDiscussed($template, $amount, $cacheName = 'showMostDiscussed', $type = 0, $slider = false, $slider_per_page = 8, $userid = false) {
    global $basehttp;
    global $basepath;
    global $gallery_url;
    global $template_path;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $bad;
    global $good;
    global $results_per_row;
    global $template_url;
    global $currentLang;
    global $config;

    if ($currentLang && strtoupper($currentLang) != $config['core_language']) {
        $langSelect .= ", `content_languages`.`title` AS `langTitle`, `content_languages`.`description` AS `langDescription`, `content_languages`.`keywords` AS `langKeywords`";
        $langJoin .= " LEFT JOIN `content_languages` ON `content_languages`.`content` = `content`.`record_num`  AND `content_languages`.`language` = '$currentLang'";
        $langWhere .= "";
    }

    $result = dbQuery("SELECT content.*, (SELECT COUNT(comments.record_num) FROM comments WHERE comments.type = 0 AND comments.content = content.record_num) AS counter, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views , (SELECT images.filename FROM images WHERE images.record_num = content.thumbnail) AS thumbfile $langSelect FROM content $langJoin WHERE content.enabled = 1 $langWhere AND photos = '$type' $filterAnd  $dateIf ORDER BY counter DESC  LIMIT 0,$amount");
    foreach ($result as $row) {
        if ($row['photos'] == 1) {
            $grow = dbRow("SELECT filename FROM images WHERE record_num = '$row[thumbnail]'", true);
            $row[thumbfile] = $grow[filename];
        }
        $farray[] = $row;
    }

    if (is_array($farray)) {
        $i = 0;
        $j = 0;
        $maxRes = count($farray);
        foreach ($farray as $row) {
            if ($row['langTitle']) {
                $row['title'] = $row['langTitle'];
            }
            if ($row['langDescription']) {
                $row['description'] = $row['langDescription'];
            }
            if ($row['langKeywords']) {
                $row['keywords'] = $row['langKeywords'];
            }
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }

            if ($slider) {
                if ($j % $slider_per_page == 0) {
                    echo '<div class="window">';
                }
            }
            if ($row['photos'] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/' . $template);
            }
            if ($slider) {
                if (($j + 1) % $slider_per_page == 0 || $j == ($maxRes - 1)) {
                    echo '</div>';
                }
            }
            ++$j;
        }
    }
}

function _showTopRated($template, $amount, $cacheName = 'showTopRated1', $type = 0, $slider = false, $slider_per_page = 8, $userid = false) {
    global $basehttp;
    global $basepath;
    global $gallery_url;
    global $template_path;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $bad;
    global $good;
    global $results_per_row;
    global $minimum_votes;
    global $template_url;
    global $currentLang;
    global $config;

    if ($currentLang && strtoupper($currentLang) != $config['core_language']) {
        $langSelect .= ", `content_languages`.`title` AS `langTitle`, `content_languages`.`description` AS `langDescription`, `content_languages`.`keywords` AS `langKeywords`";
        $langJoin .= " LEFT JOIN `content_languages` ON `content_languages`.`content` = `content`.`record_num`  AND `content_languages`.`language` = '$currentLang'";
        $langWhere .= "";
    }

    if (is_numeric($userid)) {
        $filterAnd = "AND content.submitter = '$userid'";
    }

    $result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) as views , (SELECT images.filename FROM images WHERE images.record_num = content.thumbnail) AS thumbfile  $langSelect FROM content $langJoin WHERE content.enabled = 1 $langWhere AND photos = '$type' $filterAnd  $dateIf GROUP BY content.record_num ORDER BY content.rating DESC LIMIT 0,$amount");
    foreach ($result as $row) {
        if ($row['photos'] == 1) {
            $grow = dbRow("SELECT filename FROM images WHERE record_num = '$row[thumbnail]'", true);
            $row[thumbfile] = $grow[filename];
        }
        $farray[] = $row;
    }

    if (is_array($farray)) {
        $i = 0;
        $j = 0;
        $maxRes = count($farray);
        foreach ($farray as $row) {
            if ($row['langTitle']) {
                $row['title'] = $row['langTitle'];
            }
            if ($row['langDescription']) {
                $row['description'] = $row['langDescription'];
            }
            if ($row['langKeywords']) {
                $row['keywords'] = $row['langKeywords'];
            }
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }

            if ($slider) {
                if ($j % $slider_per_page == 0) {
                    echo '<div class="window">';
                }
            }
            if ($row['photos'] == 1) {
                include($template_path . '/template.content_item_photo.php');
            } else {
                include($template_path . '/' . $template);
            }
            if ($slider) {
                if (($j + 1) % $slider_per_page == 0 || $j == ($maxRes - 1)) {
                    echo '</div>';
                }
            }
            ++$j;
        }
    }
}

function getUserNumVideos($userid) {
    if (!is_numeric($userid)) {
        return false;
    }

    $row = dbRow("SELECT COUNT(content.record_num) AS counter FROM content WHERE submitter = '$userid' AND enabled = 1 AND photos = 0", true);

    return $row['counter'];
}

function getUserNumGalleries($userid) {
    if (!is_numeric($userid)) {
        return false;
    }

    $row = dbRow("SELECT COUNT(content.record_num) AS counter FROM content WHERE submitter = '$userid' AND enabled = 1 AND photos = 1", true);

    return $row['counter'];
}

function getUserNumFavorites($userid) {
    if (!is_numeric($userid)) {
        return false;
    }

    $row = dbRow("SELECT COUNT(favorites.content) AS counter FROM favorites WHERE user = '$userid'", true);

    return $row['counter'];
}

function getUserNumFriends($userid) {
    if (!is_numeric($userid)) {
        return false;
    }

    $row = dbRow("SELECT COUNT(record_num) AS counter FROM friends WHERE (user = '$userid' OR friend = '$userid') AND approved = 1", true);

    return $row['counter'];
}

function getNumVideos() {
    return (int) dbValue("SELECT COUNT(*) AS `count` FROM `content` WHERE `enabled` = 1", 'count', true);
}

function getNumTokens($id) {
    return dbValue("SELECT `tokens` FROM `users` WHERE `record_num` = '$id'", 'tokens', false);
}

function showVotesNo($id) {
    return (int) dbValue("SELECT `total_votes` FROM `ratings` WHERE `content` = '$id'", 'total_votes', true);
}

//grab ads for specific paysite
function getAds($paysite) {
    $row = dbRow("SELECT * FROM `paysites` WHERE `record_num` = '" . (int) $paysite . "'", true);
    if ($row['ad0']) {
        $array['ad0'] = $row['ad0'];
    }
    if ($row['ad1']) {
        $array['ad1'] = $row['ad1'];
    }
    if ($row['ad2']) {
        $array['ad2'] = $row['ad2'];
    }
    if ($row['ad3']) {
        $array['ad3'] = $row['ad3'];
    }
    if ($row['ad4']) {
        $array['ad4'] = $row['ad4'];
    }
    if ($row['ad5']) {
        $array['ad5'] = $row['ad5'];
    }
    if ($row['ad6']) {
        $array['ad6'] = $row['ad6'];
    }
    if ($row['ad7']) {
        $array['ad7'] = $row['ad7'];
    }
    if ($row['ad8']) {
        $array['ad8'] = $row['ad8'];
    }
    if ($row['ad9']) {
        $array['ad9'] = $row['ad9'];
    }
    if ($row['postroll']) {
        $array['postroll'] = $row['postroll'];
    }
    return $array;
}

function _getNumVideos() {
    $result = dbQuery("SELECT COUNT(record_num) as counter FROM content WHERE enabled = 1 AND photos = 0", true, 300);
    return $result[0]['counter'];
}

function _getNumGalleries() {
    $result = dbQuery("SELECT COUNT(record_num) as counter FROM content WHERE enabled = 1 AND photos = 1", true, 300);
    return $result[0]['counter'];
}

function _getNumUsers() {
    $result = dbQuery("SELECT COUNT(record_num) as counter FROM users", true, 300);
    return $result[0]['counter'];
}

//show top stars
function showTopStars($template, $amount, $cacheName = 'showHomeTopStars') {
    global $basehttp;
    global $basepath;
    global $gallery_url;
    global $template_path;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $picthumbwidth;
    global $picthumbheight;
    global $bad;
    global $good;
    global $results_per_row;
    global $template_url;

    $result = dbQuery("SELECT pornstars.*,(SELECT COUNT(record_num) FROM content WHERE content.record_num IN(SELECT content_pornstars.content FROM content_pornstars WHERE content_pornstars.pornstar = pornstars.record_num) AND content.photos = '0') AS videos,(SELECT COUNT(record_num) FROM content WHERE record_num IN(SELECT content_pornstars.content FROM content_pornstars WHERE content_pornstars.pornstar = pornstars.record_num) AND content.photos = '1') AS photos FROM pornstars ORDER BY rating DESC LIMIT 0,$amount");
    foreach ($result as $row) {
        $farray[] = $row;
    }

    if (is_array($farray)) {
        $i = 0;
        $maxRes = count($farray);
        foreach ($farray as $row) {
            if ($results_per_row > 0) {
                $i++;
                $rest = $i % $results_per_row;
                if ($rest == 0) {
                    $class = ' last';
                } else {
                    $class = '';
                }
            }
            include($template_path . '/' . $template);
        }
    }
}

// VIDEO RATIO
function videoRatio($r) {
    $width = $r[movie_width];
    $height = $r[movie_height];
    if (($height / $width) * 100 != 0) {
        return "style='padding-top: " . ($height / $width) * 100 . "%'";
    } else {
        return "style='padding-top: " . (9 / 16) * 100 . "%'";
    }
}

// BODY CLASSES
function bodyClasses() {
    $page_name_suffix = "";
    $page_name = "home";
    if (isset($_GET['controller'])) {
        $page_name = $_GET['controller'];
    }
    if (isset($_GET['mode'])) {
        $page_name_suffix = "-" . $_GET['mode'];
    }
    return $page_name . $page_name_suffix;
}

// NUMBER OF COMMENTS
function getTotalComments($vid, $type = 0) {
    global $dbconn;
    return dbValue("SELECT COUNT(*) AS `count` FROM `comments` WHERE `content` = '" . (int) $vid . "' AND type = '" . mysqli_real_escape_string($dbconn, $type) . "'", 'count', true);
}

// NUMBER OF CATEGORIES
function getTotalChannels() {
    global $currentLang;
    global $config;
    
    $langJoin = '';
    if ($currentLang && strtoupper($currentLang) != $config['core_language']) {
        $langJoin .= " JOIN `niches_languages` ON `niches_languages`.`niche` = `niches`.`record_num` AND `niches_languages`.`language` = '$currentLang' ";
    }
    return dbValue("SELECT COUNT(`niches`.`record_num`) AS `count` FROM `niches` $langJoin", 'count', false);
}

// NUMBER OF FAVORITES
function getTotalFavs($vid) {
    return dbValue("SELECT COUNT(*)  AS `count` FROM `favorites` WHERE `content` = '" . (int) $vid . "'", 'count', false);
}

// TOTAL VIDEOS NUMBER
function showVideosCounter() {
    return dbValue("SELECT COUNT(*) AS `count` FROM `content` WHERE `enabled` = 1 AND `photos` = 0", 'count', true);
}

function userCounter($userid, $mode = 'video') {
    global $template_path;
    global $results_per_row;
    global $thumb_url;
    global $thumbwidth;
    global $thumbheight;
    global $picthumbwidth;
    global $picthumbheight;
    global $gallery_url;
    global $template_url;

    if ($mode == 'video') {
        $andPhotos = 'AND photos = 0';
    } else {
        $andPhotos = 'AND photos = 1';
    }

    //$result = dbQuery("SELECT content.*, (SELECT content_views.views FROM content_views WHERE content_views.content = content.record_num) AS views,  (SELECT images.filename FROM images WHERE images.record_num = content.thumbnail) AS thumbfile FROM content WHERE enabled = 1 AND content.submitter = '$userid' $andPhotos", true);
    $result = dbQuery("SELECT COUNT(submitter) FROM content WHERE enabled = 1 AND content.submitter = '$userid' $andPhotos", true);
    return $result[0]["COUNT(submitter)"];
}

// SHOWS USER'S AVATAR
function getUserAvatar($id) {
    global $basehttp;
    global $basepath;

    $av = dbRow("SELECT avatar, username, gender FROM users WHERE record_num = '$id'", true, 3600, "getUserAvatar.$id");
    if ($av['avatar'] != '' && file_exists("$basepath/media/misc/{$av['avatar']}")) {
        return "<img src='" . $basehttp . "/media/misc/" . $av['avatar'] . "' alt= '" . ucwords($av['username']) . "'>";
    } else {
        if (strtolower($av['gender']) == 'male') {
            return "<img src='" . $basehttp . "/core/images/avatar_male.png'  alt= '" . ucwords($av['username']) . "'>";
        } elseif (strtolower($av['gender']) == 'female') {
            return "<img src='" . $basehttp . "/core/images/avatar_female.png'  alt= '" . ucwords($av['username']) . "'>";
        } else {
            return "<img src='" . $basehttp . "/core/images/avatar_default.png'  alt= '" . ucwords($av['username']) . "'>";
        }
    }
}

//get difference between two dates
function datediff2($datefrom, $dateto = null, $using_timestamps = false) {
    $mprefix = "<span class='counter'>";
    $msufix = "</span>";
    $prefix = "<span class='sub-label'>";
    $sufix = "</span>";
    if ($dateto === null) {
        if ($using_timestamps) {
            $dateto = time();
        } else {
            $dateto = date('Y-m-d H:i:s');
        }
    }
    if (!$using_timestamps) {
        $datefrom = strtotime($datefrom, 0);
        $dateto = strtotime($dateto, 0);
    }
    $difference = $dateto - $datefrom; // Difference in seconds

    if ($difference < 86400 && $difference >= 3600) {
        $datediff = floor($difference / 3600);
        if ($datediff > 1) {
            $datediff = $mprefix . $datediff . $msufix . $prefix . ' hrs ago' . $sufix;
        } else {
            $datediff = $mprefix . $datediff . $msufix . $prefix . ' hr ago' . $sufix;
        }
    } elseif ($difference < 3600) {
        $datediff = floor($difference / 60);
        if ($datediff > 1) {
            $datediff = $mprefix . $datediff . $msufix . $prefix . ' mins ago' . $sufix;
        } else {
            $datediff = $mprefix . $datediff . $msufix . $prefix . ' min ago' . $sufix;
        }
    } else {
        $datediff = floor($difference / 86400);
//        if ($datediff > 30) {
//            $datediff = $mprefix . date('j', $datefrom) . $msufix . $prefix . date('F', $datefrom) . $sufix;
//        } else if ($datediff > 1) {
        if ($datediff > 1) {
            $datediff = $mprefix . $datediff . $msufix . $prefix . ' days ago' . $sufix;
        } else {
            $datediff = $mprefix . $datediff . $msufix . $prefix . ' day ago' . $sufix;
        }
    }
    return $datediff;
}

// STRAIGHT - S, GAY - G, SHEMALE - T
function filterUrl($f) {
    global $basehttp;
    $currentUrl = $_SERVER['REQUEST_URI'];
    $prefix = "";




    // check url to remove FILTER if exists
    if (strpos($currentUrl, "?")) {

        //$currentUrl = preg_replace('/filter=+[a-zA-Z]/', "", $currentUrl);
        $paramsArray = explode("?", $currentUrl);
        $subparamsArray = array();

        $prefixURL = $paramsArray[0];
        $suffixURL = $paramsArray[1];
        $subparamsArray = explode("&", $suffixURL);
        $newparamsArray = array();
        $newSuffixURL = "?";

        $addflag = false;
        foreach ($subparamsArray as $a) {

            if (is_numeric(strpos($a, "filter="))) {
                $a = preg_replace('/filter=+[a-zA-Z]/', "filter=" . $f, $a);
                $addflag = true;
            } else {
                $addflag = false;
            }
            array_push($newparamsArray, $a);
        }

        if (!$addflag && !in_array("filter=" . $f, $newparamsArray)) {
            array_push($newparamsArray, "filter=" . $f);
        }



        $i;
        foreach ($newparamsArray as $p) {
            $i++;
            if ($i == 1) {
                $newSuffixURL .= $p;
            } else {
                $newSuffixURL .= "&" . $p;
            }
        }
        // new url
        $newUrl = $basehttp . $prefixURL . $newSuffixURL;
        return $newUrl;
    } else {
        $newUrl = $basehttp . $currentUrl . "?filter=" . $f;
        return $newUrl;
    }
}

// show pornstars
function showPornstar($contentID, $prefix, $suffix, $thumb = true) {
    global $basehttp;
    global $video_cache_time;
    $result = dbQuery("SELECT pornstar FROM `content_pornstars` WHERE content = '" . $contentID . "'", true, $video_cache_time);

    foreach ($result as $row) {
        $pornstars_array[] = $row['pornstar'];
    }
    if (!empty($pornstars_array)) {
        $links = "";
        $thumb_img = "";
        foreach ($pornstars_array as $p) {
            $query = dbQuery("SELECT name, thumb FROM pornstars WHERE record_num = $p", true);
            if ($thumb) {
                $thumb_img = "";
                $thumb_img .= '<span class="sub-img"><img src="';
                if ($query[0]['thumb'] != '') {
                    $thumb_img .= $basehttp . "/media/misc/" . $query[0]['thumb'];
                } else {
                    $thumb_img .= $basehttp . "/core/images/catdefault.jpg";
                }
                $thumb_img .= '" alt="' . $query[0]['name'] . '"></span>';
            }

            $links .= $prefix . '<a title="' . $query[0]['name'] . '" href="' . generateUrl('pornstar', $query[0]['name'], $p) . '">' . $thumb_img . '<span class="sub-label">' . $query[0]['name'] . '</span></a>' . $suffix;
        }
    } else {
        $links = false;
    }
    return $links;
}

//partition array into several chunked ones
function partition( $list, $p ) {
    $listlen = count( $list );
    $partlen = floor( $listlen / $p );
    $partrem = $listlen % $p;
    $partition = array();
    $mark = 0;
    for ($px = 0; $px < $p; $px++) {
        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
        $partition[$px] = array_slice( $list, $mark, $incr );
        $mark += $incr;
    }
    return $partition;
}
?>