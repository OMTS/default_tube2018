<!-- textpage -->
<div class="textpage-col col">
    <div class="textpage-inner-col inner-col">
        <div class="row form-row">
            <!-- form -->
            <div class="form-col col">
                <div class="form-inner-col inner-col">
                    <?php if (!$_GET['success']) { ?>
                        <form data-mb="fine-uploader" data-opt-limit="<?php echo $sizeLimit; ?>" data-opt-accept="<?php echo $acceptFiles; ?>" data-opt-ext="<?php echo $allowedExtensions; ?>" data-opt-token="<?php echo $_SESSION['token']; ?>" data-opt-option-upload="<?php echo $optionUpload; ?>" data-opt-multiple="<?php echo $multiple; ?>" data-opt-userid="<?php echo $_SESSION['userid']; ?>" class="form-block" id="CaptchaForm"  method="post" action="javascript:void();">

                           
                                <div class="row">
                                    <!-- form-item -->
                                    <div class="form-item-col col col-full">
                                        <div class="form-item-inner-col inner-col">
                                            <div class="captcha-wrapper captcha">
                                                <img src="<?php echo $basehttp; ?>/captcha.php?<?php echo time(); ?>" class="captcha captcha-img captcha__img">
                                                <input class="form-control captcha-input -captcha captcha__input"  id="captchaUpload" name="captchaaa" type="text" value="" placeholder="<?php echo _t("Human?"); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- form-item END -->
                                </div>
                            
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col form-item--actions">
                                    <div class="form-item-inner-col inner-col">
                                        <button class="btn btn-default" id="btnCaptcha" type="submit" name="Submit"><span class="btn-label"><?php echo _t("Ok"); ?></span></button>
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                        </form>

                        <form name="formUpload" id="formUpload" method="post" action="" class="form-block" style="display:none;">
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col col-half hidden">
                                    <div class="form-item-inner-col inner-col">
                                        <input type="hidden" name="hidFileID" id="hidFileID" />
                                        <input class="hidden-field" type="text" id="txtFileName" disabled="true">
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col col-half">
                                    <div class="form-item-inner-col inner-col">
                                        <label><?php echo _t("File"); ?></label>
                                        <div id="uploadStatus" class="upload-status"></div>
                                        <div id="fine-uploader" class="fine-uploader"></div>  
                                    </div>
                                </div>
                                <!-- form-item END -->

                            </div>
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col col-half">
                                    <div class="form-item-inner-col inner-col">
                                        <input class="form-control validate[required,maxSize[60]]" name="title" type="text" id="titleUpload" value=""  placeholder="<?php echo _t("Title"); ?>">
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col col-full">
                                    <div class="form-item-inner-col inner-col">
                                        <textarea  name="description" id="descriptionUpload"  class="validate[required,maxSize[255]]" placeholder="<?php echo _t("Description"); ?>"></textarea>
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col col-full">
                                    <div class="form-item-inner-col inner-col">
                                        <label><?php echo _t("Categories"); ?></label>
                                        <div class="checkboxes">
                                            <?php
                                                if ($currentLang) {
                                                    $langSelect = ", niches_languages.name AS langName";
                                                    $langJoin = " RIGHT JOIN niches_languages ON niches_languages.niche = niches.record_num";
                                                    $langWhere = " AND niches_languages.language = '$currentLang'";
                                                }
                                                $result = dbQuery("SELECT niches.* $langSelect FROM niches $langJoin WHERE 1=1 $langWhere ORDER BY name ASC", false);
                                                foreach ($result as $srow) {
                                                    if ($srow['langName']) {
                                                        $srow['name'] = $srow['langName'];
                                                    }
                                                    $categoriesDrop[] = "<div class='cr-holder checkbox'><label class='checkbox-inline'><input type='checkbox' class='checkbox-inline validate[minCheckbox[1],maxCheckbox[6]] channelUpload' name='channels[]' id='channel$srow[record_num]' value='$srow[record_num]'><span class='checkbox-label sub-label'>$srow[name]</span></label></div>";
                                                }
                                                
                                                $categoriesChunked = array_chunk($categoriesDrop, ceil(count($categoriesDrop) / 4));
                                                foreach ($categoriesChunked as $z) {
                                            ?>
                                            <div class="group-checkboxes">
                                                <?php
                                                    foreach ($z as $i) {
                                                        echo $i;
                                                    }
                                                ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col col-half">
                                    <div class="form-item-inner-col inner-col">
                                        <input class="form-control validate[required,maxSize[60]]"  name="tags" type="text" id="tagsUpload"  placeholder="<?php echo _t("Tags"); ?>">
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col form-item--actions">
                                    <div class="form-item-inner-col inner-col">
                                        <button class="btn btn-default"  type="submit" name="button" id="btnSubmit"><span class="btn-label"><?php echo _t("Upload now!") ?></span></button>
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                        </form>
                    <?php } ?>	
                </div>
            </div>
            <!-- form END -->
        </div>
    </div>
</div>
<!-- textpage END -->





<script>
    function sendForm() {
        $('#CaptchaForm .loader').show();
        $.post('<?php echo $basehttp; ?>/includes/ajax.captcha.php', {post: $('#CaptchaForm').serialize()}, function (data) {
            $('#CaptchaForm .loader').hide();
            data.message = $.base64.decode(data.message);
            if (data.error == 'true') {
                $('#CaptchaForm .ajaxInfo').html(data.message);
            } else {
                $('#CaptchaForm .ajaxInfo').html(data.message);
                $('#hidFileID').val(data.videoid);
                $('#CaptchaForm').hide();
                $('#formUpload').show();
            }
        }, 'json');
    }

    $(document).ready(function () {

        $('#CaptchaForm').find('input').each(function () {
            $(this).keypress(function (e) {
                if (e.which == 13) {
                    sendForm();
                }
            });
        });

        $('#btnCaptcha').bind('click', function () {
            sendForm();
            return false;
        });

        jQuery("#formUpload").validationEngine();

        $('#fine-uploader').fineUploader({
            multiple: <?php echo $multiple; ?>,
            maxConnections: 1,
            debug: true,
            maxChunkSize: 10000000,
            disableCancelForFormUploads: true,
            request: {
                endpoint: '<?php echo $basehttp . "/includes/uploader/upload_ajax.php"; ?>',
                params: {
                    userId: '<?php echo $_SESSION['userid']; ?>',
                    optionUpload: '<?php echo $optionUpload; ?>',
                    token: '<?php echo $_SESSION['token']; ?>',
                    allowedExtensions: <?php echo $allowedExtensions; ?>,
                    acceptFiles: '<?php echo $acceptFiles; ?>',
                    sizeLimit: <?php echo $sizeLimit; ?>
                }
            },
            validation: {
                allowedExtensions: <?php echo $allowedExtensions; ?>,
                acceptFiles: '<?php echo $acceptFiles; ?>',
                sizeLimit: <?php echo $sizeLimit; ?>
            }

        }).on('complete', function (event, id, filename, responseJSON) {
            if (responseJSON.success) {
                $('#uploadStatus').html(responseJSON.message);
            } else {
                $('#uploadStatus').html(responseJSON.message);
            }

        });

    });
</script>