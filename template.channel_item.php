<?php
$link = generateUrl('channel', $row['name'], $row['record_num']);
$classMods = $args && $args['mods'] ? $args['mods'] : '';
?>
<!-- citem :: column :: start -->
<div class="citem col <?php echo $classMods; ?>">
    <div class="citem__inner">
        <a class="citem__link" href="<?php echo $link; ?>" title="<?php echo htmlentities($row['name'], ENT_QUOTES, 'UTF-8'); ?>">
            <span class="citem__thumb">
                <?php if (file_exists("$misc_path/cat{$row[record_num]}.jpg")) { ?>
                <img class="citem__thumb-img" src="<?php echo "$misc_url/cat{$row[record_num]}.jpg"; ?>" alt="<?php echo htmlentities($row['name'], ENT_QUOTES, 'UTF-8'); ?>">
                     <?php } else { ?>
                     <img class="citem__thumb-img" src="<? echo $basehttp; ?>/core/images/catdefault.jpg" alt="<? echo htmlentities($row['name'], ENT_QUOTES, 'UTF-8'); ?>" >
                <?php } ?>
            </span>
            <span class="citem__bd">
                <span class="citem__title"><?php echo $row['name']; ?></span>
            </span>
        </a>
    </div>
</div>
<!-- citem :: column :: end -->