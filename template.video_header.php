<?php $colorClass = ($_COOKIE["mbColorScheme"] == 1) || ($_COOKIE["user.mb.invertedColors"] == true) ? " inverted-colors js-inverted-colors" : ""; ?><!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7-only<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8-only<?php echo $colorClass ?>"><![endif]-->
<!--[if gte IE 9]><!--> <html class="no-js no-filter<?php echo $colorClass ?>"><!--<![endif]-->
    <head>
        <?php getWidget('widget.header.scripts.php'); ?>
    </head>
    <body class="page-<?php echo bodyClasses(); ?>">

        <section class="page-wrapper">
        <div class="page-wrapper__inner">

            <section class="header-sec">
                <?php getTemplate('template.nav.php'); ?>
            </section>
            <!-- header-sec END -->
            
            <!-- page-main START -->
            <section class="page-main">
            
                <?php getWidget('widget.top.php'); ?>
                
                <section class="regular-sec g-sec -sec-media">
                    <div class="wrapper">
                        <div class="row -content-aside -aside-right">
                            
                            