<?php

if (is_array($result)) {
    $i = 0;
    foreach ($result as $row) {
        if ($results_per_row > 0) {
            $i++;
            $rest = $i % $results_per_row;
            if ($rest == 0) {
                $class = ' last';
            } else {
                $class = '';
            }
        }
        getTemplate("template.paysite_item.php");
    }
} else {
    echo setMessage(_t("No paysites found."), 'error', true);
}