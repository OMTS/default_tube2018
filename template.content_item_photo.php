<?php
$link = generateUrl('galleries', $row['title'], $row['record_num']);
$dirname = str_replace('.flv', '', $row['orig_filename']);
$subdir = $row['filename'][0] . '/' . $row['filename'][1] . '/' . $row['filename'][2] . '/' . $row['filename'][3] . '/' . $row['filename'][4] . '/';
$dirname = $subdir . $dirname;
$counter = dbValue("SELECT COUNT(*) AS `count` FROM `images` WHERE `gallery` = '$row[record_num]'", 'count', true);
if (!$row['thumbfile']) {
    $row['thumbfile'] = dbValue("SELECT `filename` FROM `images` WHERE `record_num` = '$row[thumbnail]'", 'filename', true);
}
?>
<!-- item :: column :: start -->
<div class="item col">
    <div class="item__inner">

        <a href="<? echo $link; ?>" title="<? echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>" class="item__link" data-mb="shuffle">
            <span class="item__hd">
                <span class="item__stats-bar">

                    <span class="item__stat -bg-t1 -rating">
                        <span class="item__stat-icon">
                            <span class="icon -thumb-up"></span>
                        </span>
                        <span class="item__stat-label"><? echo $row['rating']; ?>%</span>
                    </span>

                    <span class="item__stat -views">
                        <span class="item__stat-icon">
                            <span class="icon -eye"></span>
                        </span>
                        <span class="item__stat-label"><? echo $row['views']; ?></span>
                    </span>

                    <span class="item__stat -duration">
                        <span class="item__stat-icon">
                            <span class="icon -photo"></span>
                        </span>
                        <span class="item__stat-label"><? echo $counter; ?></span>
                    </span>

                    <span class="item__stat -quality -bg-t3">
                        <span class="item__stat-label"><?php echo _t("Photos") ?></span>
                    </span>

                </span>
            </span>
            <span class="item__bd">
                <span class="item__thumb">
                    <img class="item__thumb-img" src="<? echo $gallery_url; ?>/<? echo $row['filename']; ?>/thumbs/<? echo $row['thumbfile']; ?>" alt="<? echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>">
                </span>
                <span class="item__title">
                    <span class="item__title-label"><? echo $row['title']; ?></span>
                </span>
            </span>
        </a>

        <a data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close") ?>" href="<? if ($_GET['mode'] == 'favorites') { ?><? echo $basehttp; ?>/action.php?action=remove_favorites&id=<? echo $row['record_num']; ?><? } else { ?><? echo $basehttp; ?>/action.php?action=add_favorites&id=<? echo $row['record_num']; ?><? } ?>" class="item__link-fav">
            <span class="icon -plus"></span>
        </a>
        <? if ($_SESSION['userid'] && ($row['submitter'] == $_SESSION['userid'])) { ?><a href="<? echo $basehttp; ?>/edit-content/?id=<? echo $row['record_num']; ?>" title="<?php echo _t("Edit") ?>" class="item__link-edit"><span class="icon -edit"></span></a><? } ?>
    </div>
</div>
<!-- item :: column :: end -->
