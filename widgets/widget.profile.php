

<!-- profile :: column :: start -->
<div class="profile col">
    <div class="profile__inner">


        <div class="profile__thumb">
            <?php
            $isPornstarBio = $_GET['controller'] !== 'my_profile' && $_GET['controller'] !== 'user_profile';
            $isProfile = $_GET['controller'] === 'my_profile' || $_GET['controller'] === 'user_profile';
            $isMyProfile = $_GET['controller'] === 'my_profile';
            $isUserProfile = $_GET['controller'] === 'user_profile';

            $avatarImgUrl = $basehttp . '/core/images/avatar_default.png';

            if ($isProfile) {

                if ($urow['avatar'] != '' && file_exists("$basepath/media/misc/$urow[avatar]")) {
                    $avatarImgUrl = $basehttp . '/media/misc/' . $urow[avatar];
                } else {
                    if (strtolower($urow['gender']) == 'male') {
                        $avatarImgUrl = $basehttp . '/core/images/avatar_male.png';
                    } elseif (strtolower($urow['gender']) == 'female') {
                        $avatarImgUrl = $basehttp . '/core/images/avatar_female.png';
                    }
                }
            }

            if ($_GET['controller'] === 'pornstar_bio' && $rrow['thumb'] != '') {
                $avatarImgUrl = $basehttp . '/media/misc/' . $rrow['thumb'];
            }
            ?>
            <img src="<?php echo $avatarImgUrl; ?>" class="profile__thumb-img" alt="<?php echo ucwords($urow['username']); ?>" />
        </div>


        <?php if ($isPornstarBio) { ?>
            <div class="profile__rating">
                <?php include($basepath . '/includes/rating/templateTh.php'); ?>
                <div class="rating-desc mini-text">
                    <?php $count_votes = dbQuery("SELECT total_votes FROM ratings WHERE content = '$rrow[record_num]' AND type = '1'"); ?>
                    <?php if (is_array($count_votes)) { ?>
                        <?php $count_votes = $count_votes[0]['total_votes']; ?>
                    <?php } else { ?>
                        <?php $count_votes = '0'; ?>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>


        <?php if ($isProfile) { ?>
            <div class="profile__bd">
                <div class="profile__bd-inner">

                    <?php if ($_SESSION['userid'] && ($urow[record_num] !== $_SESSION['userid'])) { ?>
                        <div class="profile__row">
                            <a title="<?php echo _t("Add To Friends") ?>" data-mb="modal" data-opt-close="<?php echo _t("Close") ?>" data-opt-type="iframe" data-opt-iframe-width="100%"  data-opt-iframe-height="136px" href="<? echo $basehttp; ?>/includes/inc.add_friend.php?option=add&id=<? echo $urow['record_num']; ?>" id="addAsFriend3" class="btn -width-full -primary btn-full btn-default btn-add-to-friends"><?php echo _t("Add To Friends") ?></a>
                        </div>
                    <?php } ?>

                    <?php if ($isMyProfile) { ?>
                        <div class="profile__row">
                            <a href="<?php echo $basehttp; ?>/edit-profile" class="profile__link">
                                <span class="profile__link-icon">
                                    <span class="icon -edit"></span> 
                                </span>
                                <span class="profile__link-label">
                                    <?php echo _t("Edit profile") ?>
                                </span>
                            </a>
                        </div>

                        <div class="profile__row">
                            <a href="<?php echo $basehttp; ?>/mailbox/" class="profile__link">
                                <span class="profile__link-icon">
                                    <span class="icon -email"></span> 
                                </span>
                                <span class="profile__link-label">
                                    <?php echo _t("Messages") ?>
                                </span>
                            </a>
                        </div>

                        <div class="profile__row">
                            <a href="<?php echo $basehttp; ?>/favorites/" class="profile__link">
                                <span class="profile__link-icon">
                                    <span class="icon -heart"></span> 
                                </span>
                                <span class="profile__link-label">
                                    <?php echo _t("Favorites") ?>
                                </span>
                            </a>
                        </div>

                        <div class="profile__row">
                            <a href="<?php echo $basehttp; ?>/my-friends" class="profile__link">
                                <span class="profile__link-icon">
                                    <span class="icon -group"></span> 
                                </span>
                                <span class="profile__link-label">
                                    <?php echo _t("Friends") ?>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                </div>

            </div>
        <?php } ?>

    </div>
</div>
<!-- profile :: column :: end -->