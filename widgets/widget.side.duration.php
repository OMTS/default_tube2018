<?php if ((($_GET['controller'] == "index" || (checkNav('channels') && is_numeric($_GET['channel']))) && $_GET['mode'] != "search")) { ?>
    <?php if ((($_GET['controller'] == "index" || (checkNav('channels') && is_numeric($_GET['channel']))) && $_GET['mode'] != "search") && $_GET[mode] != 'photos') { ?>
        <div class="row">
            <!-- box :: column :: start -->
            <div class="box col -mrb">
                <div class="box__inner">
                    <div class="box__hd">
                        <div class="box__hd-inner">

                            <span class="box__hd-icon -sm">
                                <span class="icon -time"></span>
                            </span>

                            <h2 class="box__h"><?php echo _t("Duration") ?> <span class="box__h-legend">(<?php echo _t("minutes") ?>)</span></h2>

                            <div class="box__hd-utils">
                                <button class="btn -outlined g--hidden-sm-up" data-mb-expand="box-duration" data-mb-options="{'activeToMaxScreenWidth': 768}">
                                    <span class="btn__icon">
                                        <span class="icon -expand"></span>
                                    </span>
                                    <span class="btn__label -when-inactive"><?php echo _t("More") ?></span>
                                    <span class="btn__label -when-active"><?php echo _t("Hide") ?></span>
                                </button>
                            </div>

                        </div>
                    </div>
                    <div class="box__bd" data-mb-expand-target="box-duration">
                        <div class="box__bd-inner">
                            
                            <div class="irs-range-slider">
                                <div class="irs-range-slider__inner">
                                    <?php
                                    $range_from = (!empty($_GET['durationFrom'])) ? (int) $_GET['durationFrom'] / 60 : 0;
                                    $range_to = (!empty($_GET['durationTo'])) ? (int) $_GET['durationTo'] / 60 : 50;
                                    ?>
                                    <input type="text" data-from="<?php echo $range_from; ?>" data-to="<?php echo $range_to; ?>" data-max="50" data-min="0" data-attr-from="durationFrom" data-attr-to="durationTo" data-multiplication="60" id="range_length_filter" name="filter_length" value="" >
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- box :: column :: end -->
        </div>
    <?php } ?>
<?php } ?>