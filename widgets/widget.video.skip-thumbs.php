<?php if(((detectMobile() && $sceneSelectionMobile) || (!detectMobile() && $sceneSelectionDesktop)) && !$rrow['embed'] && !$rrow['vr']) { ?>
    <?php if ($showVideoRatio) { ?>
        <div class="row">
            <!-- mmedias :: column :: start -->
            <div class="mmedias col">
                <div class="mmedias__inner">
                     <?php echo displaySceneSelection(); ?>
                </div>
            </div>
            <!-- mmedias :: column :: end -->
        </div>
    <?php } ?>
<?php } ?>