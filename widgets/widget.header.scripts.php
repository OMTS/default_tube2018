        <?php
        require_once($template_path . "/template.functions/functions.custom.php");
        if ($_GET['controller'] != 'members') {
            unset($_SESSION['ms']); // reset MEMBERS FILTER
        }
        ?>
        <meta charset="UTF-8">
        <title><?php echo $title; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="Mechbunny.com">
        <meta name="description" content="<?php echo $metadescription; ?>">
        <meta name="keywords" content="<?php echo $metakeywords; ?>">
        <meta name="robots" content="<?php echo $metarobots; ?>">
        <?php
        if ($rrow['photos'] == 1) {
            $link = generateUrl('galleries', $rrow['title'], $rrow['record_num']);
        } else if (isset($rrow['photos']) && $rrow['photos'] == 0) {
            $link = generateUrl('video', $rrow['title'], $rrow['record_num']);
        } else {
            $link = $basehttp . $_SERVER['REQUEST_URI'];
        }

        $clink = $link;
        if (strpos($clink, "?")) {
            $varray = explode("?", $link);
            $clink = $varray[0];
        }
        ?>
        <link rel="canonical" href="<?php echo $clink; ?>">

        <?php
        if (($thisController == 'index' || checkNav('pornstars') || checkNav('members') || checkNav('channels')) && !checkNav('random') && $total_pages > 0) {
            if (!isset($_GET['page']) || $_GET['page'] == '') {
                $page = 1;
            } else {
                $page = $_GET['page'];
            }

            if ($page > 1) {
                echo '<link rel="prev" href="page' . ($page - 1) . '.html" />';
            }

            if ($page < $total_pages) {
                echo '<link rel="next" href="page' . ($page + 1) . '.html" />';
            }
        }
        ?>

        <?php $urlArray = parse_url($basehttp); ?>
        <script>
            var domainName = '<?php echo $urlArray['host'] ?>';
            var _basehttp = '<?php echo $basehttp; ?>', settings = {};
            var _templateUrl = '<?php echo $template_url; ?>', settings = {};
        </script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/es6-shim/0.33.3/es6-shim.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/systemjs/0.19.20/system-polyfills.js"></script>
        <!-- [if lte IE 9]>
        <script src="//npmcdn.com/angular2/es6/dev/src/testing/shims_for_IE.js"></script>
        <![endif] -->
        <!--<script src="<?php echo $template_url; ?>/js/app.min.js"></script>-->
        <script src="<?php echo $template_url; ?>/js/app.js"></script>
        <script src="<?php echo $template_url; ?>/js/custom.js"></script>

        <link rel="stylesheet"  media="screen" href="<?php echo $template_url; ?>/css/styles.css">
        <link rel="stylesheet"  media="screen" href="<?php echo $template_url; ?>/css/overwrite.css">

        <link rel="manifest" href="<?php echo $template_url; ?>/js/manifest.json">
        <link rel="shortcut icon" href="<?php echo $template_url; ?>/images/touch/fav.png" type="image/x-icon">
        <?php /* <link rel="image_src" href='<?php echo $template_url; ?>/images/touch/icon-128x128.png'> */ ?>

        <!-- Disable tap highlight on IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- Add to homescreen for Chrome on Android -->
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="application-name" content="<?php echo $sitename; ?>">
        <link rel="icon" sizes="192x192" href="<?php echo $template_url; ?>/images/touch/chrome-touch-icon-192x192.png">

        <!-- Add to homescreen for Safari on iOS -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="<?php echo $sitename; ?>">
        <link rel="apple-touch-icon" href="<?php echo $template_url; ?>/iimages/touch/apple-touch-icon.png">

        <!-- Tile icon for Win8 (144x144 + tile color) -->
        <meta name="msapplication-TileImage" content="<?php echo $template_url; ?>/images/touch/ms-touch-icon-144x144-precomposed.png">
        <meta name="msapplication-TileColor" content="#000000">

        <!-- Color the status bar on mobile devices -->
        <meta name="theme-color" content="#000000">

        <?php
        if ($_GET['controller'] == "gallery" || $_GET['controller'] == "video") {
            if ($rrow['photos'] == 0) { //videos
                $dirname = str_replace('.flv', '', $rrow['orig_filename']);
                $subdir = $rrow['filename'][0] . '/' . $rrow['filename'][1] . '/' . $rrow['filename'][2] . '/' . $rrow['filename'][3] . '/' . $rrow['filename'][4] . '/';
                $dirname = $subdir . $dirname;

                if ($rrow['embed'] != "") {
                    $content_image_url = $thumb_url . '/embedded/' . $rrow['record_num'] . '.jpg';
                    $content_image_info = getimagesize($thumb_path . '/embedded/' . $rrow['record_num'] . '.jpg');
                } else {
                    $content_image_url = $thumb_url . '/' . $dirname . '/' . $rrow['orig_filename'] . '-' . $rrow['main_thumb'] . '.jpg';
                    $content_image_info = getimagesize($thumb_path . '/' . $dirname . '/' . $rrow['orig_filename'] . '-' . $rrow['main_thumb'] . '.jpg');
                }
                $content_link = generateUrl('video', $rrow['title'], $rrow['record_num']);
            } else { //galleries
                if (!$rrow['thumbfile']) {
                    $srow = dbRow("SELECT filename FROM images WHERE record_num = '" . $rrow['thumbnail'] . "'", false);
                    $rrow['thumbfile'] = $srow['filename'];
                }

                $content_image_url = "$gallery_url/" . $rrow['filename'] . "/thumbs/" . $rrow['thumbfile'];
                $content_image_info = getimagesize("$gallery_path/" . $rrow['filename'] . "/thumbs/" . $rrow['thumbfile']);
                $content_link = generateUrl('galleries', $rrow['title'], $rrow['record_num']);
            };
            ?>
            <meta property="og:image" content="<?php echo $content_image_url; ?>">
            <meta property="og:title" content="<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>">
            <meta property="og:description" content="<?php echo htmlentities($rrow['description'], ENT_QUOTES, 'UTF-8'); ?>">
            <meta property="og:url" content="<?php echo $content_link; ?>">
            <meta property="og:image:type" content="<?php echo $content_image_info['mime']; ?>">
            <meta property="og:image:width" content="<?php echo $content_image_info[0]; ?>">
            <meta property="og:image:height" content="<?php echo $content_image_info[1]; ?>">
        <?php } ?><!-- Web Application Manifest -->
        
		<?php
		$langsNav = flags(); 
		foreach ($langsNav['list'] as $item) { ?>
		<link rel="alternate" hreflang="<? echo $item['iso']; ?>" href="<? echo $item['http'].$_SERVER['REQUEST_URI']; ?>" />
		<? } ?>
