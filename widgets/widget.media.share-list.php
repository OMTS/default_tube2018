<ul class="share-list">
    <li class="share-list__li">
        <a class="share-list__link"data-mb="popup" data-opt-width="500" data-opt-height="400" href="http://www.facebook.com/share.php?u=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>&title=<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>">
            <span class="icon -share-fb"></span>
        </a>
    </li>
    <li class="share-list__li">
        <a class="share-list__link"data-mb="popup" data-opt-width="500" data-opt-height="400" href="http://twitter.com/home?status=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>+<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>">
            <span class="icon -share-tw"></span>
        </a>
    </li>
    <li class="share-list__li">
        <a class="share-list__link"data-mb="popup" data-opt-width="500" data-opt-height="400" href="http://www.tumblr.com/share?v=3&u=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>&t=<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>">
            <span class="icon -share-tr"></span>
        </a>
    </li>
    <li class="share-list__li">
        <a class="share-list__link"data-mb="popup" data-opt-width="500" data-opt-height="400" href="https://plus.google.com/share?url=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>">
            <span class="icon -share-gp"></span>
        </a>
    </li>
</ul>