<div class="row">
    <!-- media :: column :: start -->
    <div class="media col -video">
        <?php
        //check if videoRatio should be used. It should be false only when in VOD Mode user does not have access to video or in Paysite Mode where user is not premium and mode is set to thumbnails (mode 2)
        $showVideoRatio = false;
        if (!$vodMode && $paysiteMode != 2) {
            $showVideoRatio = true;
        } else {
            if ($_SESSION['premium']) {
                $showVideoRatio = true;
            }
            if ($vodMode && billingCheckUserContentAuth($_SESSION['userid'], $rrow['record_num'])) {
                $showVideoRatio = true;
            }
        }
        ?>
        <div class="media__inner" <?php
        if ($showVideoRatio) {
            echo videoRatio($rrow);
        } else {
            echo 'style="padding-top:0;"';
        }
        ?>>
            <?php if (($vodMode && !billingCheckUserContentAuth($_SESSION['userid'], $rrow['record_num'])) || (!$_SESSION['premium'] && $paysiteMode == '2' && !billingCheckUserContentAuth($_SESSION['userid'], $rrow['record_num']))) { ?>
                <center><span style='font-size: 18px; font-weight: bold;'><?php echo _t("You must purchase access to this video in order to view it."); ?></span></center><br>
                <div class="preview-thumbs">
                    <?php
                    $dirname = str_replace('.flv', '', $rrow['orig_filename']);
                    $subdir = $rrow['filename'][0] . '/' . $rrow['filename'][1] . '/' . $rrow['filename'][2] . '/' . $rrow['filename'][3] . '/' . $rrow['filename'][4] . '/';
                    $dirname = $subdir . $dirname;
                    ?>
                    <?php for ($i = 1; $i <= 8; $i++) { ?>

                        <img src="<?php echo $thumb_url; ?>/<?php echo $dirname; ?>/<?php echo $rrow['orig_filename']; ?>-<?php echo $i; ?>.jpg" title="<?php echo $row['title'] . '-' . $i ?>" style='width: 24%'>

                    <?php } ?>
                    <div class="signup-link" style='text-align: center;'>
                        <br>
                        <?php
                        if ($vodMode) {
                            $purchaseLink = "$basehttp/purchase/{$rrow['record_num']}";
                        } else {
                            $purchaseLink = "$basehttp/signup";
                        }
                        ?>
                        <a class='btn btn-default btn-sm' href="<?php echo $purchaseLink; ?>"><?php echo _t("Click Here To Purchase Access!"); ?></a><br><br>
                    </div>
                </div>
            <?php } else { ?>

                <?php displayPlayerWrapper($rrow, "100%", "100%"); ?>

            <?php } ?>
        </div>
    </div>
    <!-- media :: column :: end -->
</div>