<div class="row">
    <div class="media col -gallery">
        <div class="media__inner">
            <div class="canvas mod">
                <?php
                //display single image if is selected
                if (is_numeric($_GET['image'])) {
                    echo "<div class='canvas__main' data-mb='ajax-container' data-opt-close='" . _t("Close") . "' data-opt-title='" . _t("Error") . "' id='singleImage' data-opt-current='" . $_GET['image'] . "'>";
                    echo '<div class="loader canvas__main-loader"><div class="loader-inner ball-triangle-path"><div></div><div></div><div></div></div></div>';



                    //generate previous and next button links
                    $goToNext = "";
                    $goToPrev = "";
                    if ($_GET['image'] - 1 > 0) {
                        $imagePrevLink = "$galleryUrl?image=" . ($_GET['image'] - 1);
                        $goToPrev = $_GET['image'] - 1;
                    } else {
                        $imagePrevLink = $galleryUrl;
                    }
                    if ($_GET['image'] + 1 <= count($result)) {
                        $imageNextLink = "$galleryUrl?image=" . ($_GET['image'] + 1);
                        $goToNext = $_GET['image'] + 1;
                    } else {
                        $imageNextLink = $galleryUrl;
                    }
                    $hasMultipleImages = count($result) > 1;
                    $canUsePrevious = $_GET['image'] > 1;
                    $canUseNext = $_GET['image'] < count($result)
                    //show image and prev/next
                    ?>

                    <a class="canvas__main-link" href="<?php echo $imageUrl; ?>" data-lightbox="gallery" title="<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>">
                        <img class="canvas__main-link-img" src="<?php echo $imageUrl; ?>" alt="<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?> (<?php echo $_GET['image']; ?>/<?php echo count($result) ?>)">
                    </a>

                    <?php if ($hasMultipleImages) { ?>

                        <div class="gallery-controls canvas__controls">

                            <?php if ($canUsePrevious) { ?>
                                <a  rel="prev" data-opt-next="<?php echo $goToPrev; ?>" data-mb="load-img" data-toggle="tooltip" class="gallery-nav gallery-prev canvas__controls-link -prev" title="<?php echo _t("Previous") ?> (<?php echo $goToPrev ?>/<?php echo count($result) ?>)" href="<?php echo $imagePrevLink; ?>">
                                    <span class="canvas__controls-icon">
                                        <span class="icon -prev"></span>
                                    </span>
                                </a>
                            <?php } ?>

                            <?php if ($canUseNext) { ?>
                                <a rel="next" data-opt-next="<?php echo $goToNext; ?>" data-mb="load-img" data-toggle="tooltip" class="gallery-nav gallery-next canvas__controls-link -next" title="<?php echo _t("Next") ?> (<?php echo $goToNext ?>/<?php echo count($result) ?>)" href="<?php echo $imageNextLink; ?>">
                                    <span class="canvas__controls-icon">
                                        <span class="icon -next"></span>
                                    </span>
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <?php echo "</div>";  } // .canvas__main END ?>
                
                <?php 
                //display all images
                echo "<div class='row' id='galleryImages' >";
                if (is_numeric($_GET['image'])) {
                    echo '<div class="h-slider canvas__h-slider-thumbs" data-mb="slider" data-opt-current-img="' . $_GET['image'] . '" data-opt-img-max="' . count($result) . '">';
                }

                foreach ($result as $k => $row) {
                    $j++;
                    ?>
                    <div class='canvas__thumb gallery-item-col col gi-<?php echo $j; ?> <?php echo ($j == $_GET['image']) ? "active" : ""; ?>' >
                        <div class="canvas__thumb-inner inner-block">
                            <a class="canvas__thumb-link" <?php if (is_numeric($_GET['image'])) { ?>data-mb="load-img" data-opt-next="<?php echo $k + 1; ?>"<?php } ?> title="Image <?php echo $k + 1; ?>/<?php echo count($result) ?>" href="<?php echo $galleryUrl; ?>?image=<?php echo $k + 1; ?>">
                                <img class="canvas__thumb-img" src="<?php echo $gallery_url; ?>/<?php echo $rrow[filename]; ?>/thumbs/<?php echo $row[filename]; ?>" alt="<?php echo $row[title]; ?>" >
                            </a>
                        </div>
                    </div>




                    <?php
                }

                if (is_numeric($_GET['image'])) {
                    echo '</div>';
                }

                if (is_numeric($_GET['image'])) {
                    echo '<div class="btm -primary gallery-thumbs-option text-center"><a href="' . $galleryUrl . '" class="btn btn-default">' . _t("Show all thumbs") . '</a></div>';
                }

                if (false) {
                    ?>
                    <div class="gallery-item-col-paginator col">
                        <div class="inner-block">
                            <div class="row pagination">
                                <div class="pagination-items col">
                                    <div class="pagination-bg">
                                        <div class="contents">
                                            <?php foreach ($result as $k => $row) { ?>
                                                <a <?php if ($_GET['image'] == $k + 1) { ?>class="jp-current"<?php } ?> href="<?php echo $galleryUrl; ?>?image=<?php echo $k + 1; ?>"><?php echo $k + 1; ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php
                if (count($result) == 0) {
                    echo "Sorry, this gallery contains no photos.";
                }
                ?>
                <?php echo "</div>";
                ?>
            </div>
        </div>
    </div>
</div>
