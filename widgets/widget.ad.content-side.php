<!-- aside :: column :: start -->
<aside class="aside col">
    <div class="aside__inner">
        <div class="row">
            <!-- aff :: column :: start -->
            <div class="aff col">
                <div class="aff__inner">
                    <a href="#hrefText" class="aff__link">
                         <img src="<?php echo $template_url; ?>/images/ad-280.png" class="aff__img" alt="" />
                    </a>
                </div>
            </div>
            <!-- aff :: column :: end -->
        </div>
        <div class="row">
            <!-- aff :: column :: start -->
            <div class="aff col">
                <div class="aff__inner">
                    <a href="#hrefText" class="aff__link">
                        <img src="<?php echo $template_url; ?>/images/ad-280-2.png" class="aff__img" alt="" />
                    </a>
                </div>
            </div>
            <!-- aff :: column :: end -->
        </div>


        <div class="row">
            <!-- aff :: column :: start -->
            <div class="aff col">
                <div class="aff__inner">
                    <a href="#hrefText" class="aff__link">
                        <img src="<?php echo $template_url; ?>/images/ad-280-3.png" class="aff__img" alt="" />
                    </a>
                </div>
            </div>
            <!-- aff :: column :: end -->
        </div>



    </div>
</aside>
<!-- aside :: column :: end -->