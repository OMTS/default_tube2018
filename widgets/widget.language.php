<?php $langsNav = flags(); ?>
<li class="ucp-list__li -lang-select">
    <?php if (is_array($langsNav['list']) && count($langsNav['list']) > 0) { ?>
    <a href="#hrefText" class="ucp-list__link is-active" data-mb="dropdown">
        <span class="ucp-list__icon">
            <span class="icon -globe"></span>
        </span>
        <span class="ucp-list__label"><?php echo strtolower($langsNav['current']['iso']); ?></span>
        <span class="ucp-list__icon">
            <span class="icon -caret-down"></span>
        </span>
    </a>

    <ul class="drop-list g--dropdown">
            <?php foreach ($langsNav['list'] as $item) { ?>
                <li class="drop-list__li">
                    <a href="<?php echo $item['http']; ?>" title="<?php echo $item['iso']; ?>" class="drop-list__link">
                        <?php echo $item['iso']; ?>
                    </a>
                </li>
            <?php } ?>
    </ul>
    
    <?php } else { ?>
    <a href="#" class="ucp-list__link is-active" data-mb="dropdown">
        <span class="ucp-list__icon">
            <span class="icon -globe"></span>
        </span>
        <span class="ucp-list__label">
            <img class="ucp-list__label-img" src="<?php echo $basehttp; ?>/core/images/flags/<?php echo strtolower($langsNav['current']['iso']); ?>.png" alt="">
        </span>
    </a>
    <?php } ?>

</li>
