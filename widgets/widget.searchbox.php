<!-- search :: column :: start -->
<div class="search col">
    <div class="search__inner">

        <form class="search__form" action="<?php echo $basehttp; ?>/searchgate.php" method="get">
            <input type="text" placeholder="<?php echo _t("Search") ?>" value="<?php echo str_replace("-", " ", $_GET['q']); ?><?php echo str_replace("-", " ", $_GET['letter']); ?>" name="q" class="search__input-text" />
            <input type="text" value="" name="type" class="search__input-text -hidden" data-mb-search-target />

            <div class="search__type">
                <div class="search__type-selected" data-mb-search-label="all">
                    <span class="search__type-icon">
                        <span class="icon -db"></span>
                    </span>
                    <span class="search__type-label">All</span>
                </div>

                <ul class="search-list">
                    <li class="search-list__li" data-mb-search-select="">
                        <span class="search__type-icon">
                            <span class="icon -db"></span>
                        </span>
                        <span class="search__type-label"><?php echo _t("All") ?></span>
                    </li>
                    <li class="search-list__li" data-mb-search-select="videos">
                        <span class="search__type-icon">
                            <span class="icon -video"></span>
                        </span>
                        <span class="search__type-label"><?php echo _t("Videos") ?></span>
                    </li>
                    <li class="search-list__li" data-mb-search-select="photos">
                        <span class="search__type-icon">
                            <span class="icon -camera"></span>
                        </span>
                        <span class="search__type-label"><?php echo _t("Photos") ?></span>
                    </li>
                    <li class="search-list__li" data-mb-search-select="models">
                        <span class="search__type-icon">
                            <span class="icon -star"></span>
                        </span>
                        <span class="search__type-label"><?php echo _t("Models") ?></span>
                    </li>
                    <li class="search-list__li" data-mb-search-select="members">
                        <span class="search__type-icon">
                            <span class="icon -users"></span>
                        </span>
                        <span class="search__type-label"><?php echo _t("Community") ?></span>
                    </li>
                </ul>

            </div>

            <button class="search__submit" type="submit">
                <span class="search__submit-icon">
                    <span class="icon -search"></span>
                </span>
            </button>

        </form>

    </div>
</div>
<!-- search :: column :: end -->
