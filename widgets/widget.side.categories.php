<?php if ($thisController !== 'paysites') { ?>
    <div class="row">
        <!-- box :: column :: start -->
        <div class="box col -mrb">
            <div class="box__inner">
                <div class="box__hd">
                    <div class="box__hd-inner">

                        <span class="box__hd-icon -sm">
                            <span class="icon -list"></span>
                        </span>

                        <h2 class="box__h"><?php echo ($thisController == 'channels') ? _t("Alphabetical") : _t("Categories"); ?> <span class="box__h-legend">(<?php echo getTotalChannels(); ?>)</span></h2>

                        <div class="box__hd-utils">
                            <button class="btn -outlined g--hidden-sm-up" data-mb-expand="box-channels" data-mb-options="{'activeToMaxScreenWidth': 768}">
                                <span class="btn__icon">
                                    <span class="icon -expand"></span>
                                </span>
                                <span class="btn__label -when-inactive"><?php echo _t("More") ?></span>
                                <span class="btn__label -when-active"><?php echo _t("Hide") ?></span>
                            </button>
                        </div>

                    </div>
                </div>
                <div class="box__bd" data-mb-expand-target="box-channels">
                    <div class="box__bd-inner">
                        <ul class="counter-list">
                            <?php echo showChannels2("<li  class=\"counter-list__li\">", "</li>", true); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- box :: column :: end -->
    </div>
<?php } ?>
