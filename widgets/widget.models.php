<?php if (checkNav('index')) { ?>
<section class="regular-sec g-sec -sec-models">
    <div class="wrapper">
        <div class="row">
            <!-- box :: column :: start -->
            <div class="box col">
                <div class="box__inner">

                    <div class="box__hd">
                        <div class="box__hd-inner">

                            <span class="box__hd-icon">
                                <span class="icon -star"></span>
                            </span>

                            <h2 class="box__h"><?php echo _t("Models"); ?></h2>

                            <div class="box__hd-utils">
                                <a class="btn -outlined" href="<? echo $basehttp; ?>/models/" title="<?php echo _t("Models"); ?>">
                                    <span class="btn__icon">
                                        <span class="icon -plus"></span>
                                    </span>
                                    <span class="btn__label"><?php echo _t("See all"); ?></span>
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="box__bd">
                        <div class="box__bd-inner">

                            <div class="row">
                                <?php showTopStars('template.pornstar_item.php', 4); ?>
                               
                            </div>

                        </div>
                    </div>

                    <div class="box__ft"></div>

                </div>
            </div>
            <!-- box :: column :: end -->
        </div>
    </div>
</section>
<?php } ?>
