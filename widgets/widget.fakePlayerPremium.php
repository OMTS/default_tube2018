

		<div id="playerOverlay">
                    <span class="label"><? echo _t("You must be a premium member to view this video!"); ?></span>
                    <a class='btn btn-default btn-sm' href='<? echo $basehttp; ?>/signup'><? echo _t("Click here to sign up!"); ?></a>
		</div>
		<style>
		#playerOverlay {
			background:rgba(0,0,0,0.5);
			width: 490px;
			position:absolute;
			text-align: center;
			top:50%;
			left: 50%;
			padding-top: 10px;
			padding-bottom: 15px;
			margin-left: -245px;
			margin-top: -57px;
			z-index:3;
			font-size: 15px;
			font-weight: bold;
			color: #ffffff;
		}
		#playerOverlay .label {
                    display: block;
                    padding: 0 15px;
                    margin-bottom: 15px;
                }
		</style>

