<footer class="footer">
    <div class="wrapper">
        <div class="row">
            <!-- uft :: column :: start -->
            <div class="footer__upper col">
                <div class="footer__upper_inner">

                    <div class="row">

                        <!-- ftcol :: column :: start -->
                        <div class="ftcol col">
                            <div class="ftcol__inner">

                                <div class="ftcol__hd">
                                    <h3 class="ftcol__h"><?php echo _t("Movies") ?></h3>
                                </div>
                                
                                <div class="ftcol__bd">
                                    <ul class="simple-list">
                                        <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/most-recent/" class="simple-list__link" title="<?php echo _t("New Releases") ?>"><?php echo _t("New Releases") ?></a>
                                        </li>
                                        <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/most-viewed/" class="simple-list__link" title="<?php echo _t("Most Viewed") ?>" ><?php echo _t("Most Viewed") ?></a>
                                        </li>
                                        <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/top-rated/" class="simple-list__link" title="<?php echo _t("Top Rated") ?>"><?php echo _t("Top Rated") ?></a>
                                        </li>
                                        <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/most-discussed/" class="simple-list__link" title="<?php echo _t("Most Discussed") ?>"><?php echo _t("Most Discussed") ?></a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <!-- ftcol :: column :: end -->

                        <!-- ftcol :: column :: start -->
                        <div class="ftcol col">
                            <div class="ftcol__inner">

                                <div class="ftcol__hd">
                                    <h3 class="ftcol__h"><?php echo _t("Account") ?></h3>
                                </div>

                                <div class="ftcol__bd">
                                    <ul class="simple-list">
                                        <? if (!isLoggedIn()) { ?>
                                        <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/login" class="simple-list__link" title="<?php echo _t("Login") ?>"><?php echo _t("Login") ?></a>
                                        </li>
                                        <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/signup" class="simple-list__link" title="<?php echo _t("Create free account") ?>"><?php echo _t("Create free account") ?></a>
                                        </li>
                                        <? } else { ?>
                                        <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/my-profile" class="simple-list__link" title="<?php echo _t("My profile") ?>"><?php echo _t("My profile") ?></a>
                                        </li>
                                        <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/edit-profile" class="simple-list__link" title="<?php echo _t("Edit profile") ?>"><?php echo _t("Edit profile") ?></a>
                                        </li>
                                         <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/logout"class="simple-list__link" title="<?php echo _t("Logout") ?>"><?php echo _t("Logout") ?></a>
                                        </li>
                                        <? } ?>
                                        <li class="simple-list__li">
                                            <a href="<? echo $basehttp; ?>/contact" class="simple-list__link" title="<?php echo _t("Contact") ?>"><?php echo _t("Contact") ?></a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <!-- ftcol :: column :: end -->

                        <!-- ftcol :: column :: start -->
                        <div class="ftcol col -last">
                            <div class="ftcol__inner">
                                
                                <div class="ftcol__hd">
                                    <h3 class="ftcol__h"><?php echo _t("Other products") ?></h3>
                                </div>

                                <div class="ftcol__bd">
                                    <ul class="inline-list">
                                        <li class="inline-list__li">
                                            <a href="https://www.mechbunny.com/" class="inline-list__link">
                                                <img src="<?php echo $template_url; ?>/images/icon-ad-1.png" class="inline-list__img" alt="" />
                                            </a>
                                        </li>
                                        <li class="inline-list__li">
                                            <a href="http://affil.mechbunny.com/" class="inline-list__link">
                                                <img src="<?php echo $template_url; ?>/images/icon-ad-2.png" class="inline-list__img" alt="" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <!-- ftcol :: column :: end -->

                        <!-- scrollup :: column :: start -->
                        <div class="scrollup col">
                            <div class="scrollup__inner">
                                <a href="#" class="scrollup__link" data-mb="scrollup">
                                    <span class="icon -caret-up"></span>
                                </a>
                            </div>
                        </div>
                        <!-- scrollup :: column :: end -->

                    </div>


                </div>
            </div>
            <!-- uft :: column :: end -->
        </div>
        <div class="row">
            <!-- legals :: column :: start -->
            <div class="legals col">
                <div class="legals__inner">
              
                    <ul class="sep-list">
                        <li class="sep-list__li">
                            <a title="<?php echo _t("DMCA Notice") ?>" href="<? echo $basehttp; ?>/static/dmca.html" class="sep-list__link">
                                <?php echo _t("DMCA Notice") ?>
                            </a>
                        </li>
                        <li class="sep-list__li">
                            <a title="<?php echo _t("Terms of Use") ?>" href="<? echo $basehttp; ?>/static/tos.html" href="#hrefText" class="sep-list__link">
                                <?php echo _t("Terms of Use") ?>
                            </a>
                        </li>
                        <li class="sep-list__li">
                            <a title="<?php echo _t("18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement") ?>" href="<? echo $basehttp; ?>/static/2257.html" class="sep-list__link">
                                <?php echo _t("18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement") ?>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <!-- legals :: column :: end -->

            <!-- copyright :: column :: start -->
            <div class="copyright col">
                <div class="copyright__inner">
                    Mechbunny Tube Script &copy; Copyright <? echo date('Y'); ?>
                </div>
            </div>
            <!-- copyright :: column :: end -->


        </div>
    </div>
</footer>