<?php if ((($_GET['controller'] == "index" || (checkNav('channels') && is_numeric($_GET['channel']))) && $_GET['mode'] != "search")) { ?>
    <div class="row">
        <!-- box :: column :: start -->
        <div class="box col -mrb">
            <div class="box__inner">
                <div class="box__hd">
                    <div class="box__hd-inner">

                        <span class="box__hd-icon -sm">
                            <span class="icon -filter"></span>
                        </span>

                        <h2 class="box__h"><?php echo _t('Filter content'); ?></h2>

                        <div class="box__hd-utils">
                            <button class="btn -outlined g--hidden-sm-up" data-mb-expand="box-filter" data-mb-options="{'activeToMaxScreenWidth': 768}">
                                <span class="btn__icon">
                                    <span class="icon -expand"></span>
                                </span>
                                <span class="btn__label -when-inactive"><?php echo _t("More") ?></span>
                                <span class="btn__label -when-active"><?php echo _t("Hide") ?></span>
                            </button>
                        </div>

                    </div>
                </div>
                <div class="box__bd" data-mb-expand-target="box-filter">
                    <div class="box__bd-inner">
                        <?php if ($_GET['mode'] == 'channel' && is_numeric($_GET['channel'])) { ?>
                            <ul class="counter-list -filters">

                                <li class="counter-list__li">
                                    <a class="counter-list__link<?php if ($_SESSION['filterContent'] != "videosOnly" && $_SESSION['filterContent'] != "photosOnly") { ?> is-active<?php } ?>" href="<?php echo $basehttp . "/filter/a"; ?>" title="<?php echo _t("Videos & Photos") ?>">
                                        <span class="counter-list__icon"><span class="icon -caret-right"></span></span>
                                        <span class="counter-list__link-label"><?php echo _t("Videos & Photos") ?></span>
                                    </a>
                                </li>
                                <li class="counter-list__li">
                                    <a class="counter-list__link<?php if ($_SESSION['filterContent'] == "videosOnly") { ?> is-active<?php } ?>" href="<?php echo $basehttp . "/filter/videos"; ?>" title="<?php echo _t("Videos Only") ?>">
                                        <span class="counter-list__icon"><span class="icon -caret-right"></span></span>
                                        <span class="counter-list__link-label"><?php echo _t("Videos Only") ?></span>
                                    </a>
                                </li>
                                <li class="counter-list__li">
                                    <a class="counter-list__link<?php if ($_SESSION['filterContent'] == "photosOnly") { ?> is-active<?php } ?>" href="<?php echo $basehttp . "/filter/photos"; ?>" title="<?php echo _t("Photos Only") ?>">
                                        <span class="counter-list__icon"><span class="icon -caret-right"></span></span>
                                        <span class="counter-list__link-label"><?php echo _t("Photos Only") ?></span>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>

                        <ul class="counter-list -filters">

                            <li class="counter-list__li">
                                <a class="counter-list__link<?php if ($_SESSION['filterType'] != "S" && $_SESSION['filterType'] != "G" && $_SESSION['filterType'] != "T") { ?> is-active<?php } ?>" href="<?php echo $basehttp . "/filter-content/a"; ?>" title="<?php echo _t("All") ?>">
                                    <span class="counter-list__icon"><span class="icon -caret-right"></span></span>
                                    <span class="counter-list__link-label"><?php echo _t("All") ?></span>
                                </a>
                            </li>
                            <li class="counter-list__li">
                                <a class="counter-list__link<?php if ($_SESSION['filterType'] == "S") { ?> is-active<?php } ?>" href="<?php echo $basehttp . "/filter-content/S"; ?>" title="<?php echo _t("Straight") ?>">
                                    <span class="counter-list__icon"><span class="icon -caret-right"></span></span>
                                    <span class="counter-list__link-label"><?php echo _t("Straight") ?></span>
                                </a>
                            </li>
                            <li class="counter-list__li">
                                <a class="counter-list__link<?php if ($_SESSION['filterType'] == "G") { ?> is-active<?php } ?>" href="<?php echo $basehttp . "/filter-content/G"; ?>" title="<?php echo _t("Gay") ?>">
                                    <span class="counter-list__icon"><span class="icon -caret-right"></span></span>
                                    <span class="counter-list__link-label"><?php echo _t("Gay") ?></span>
                                </a>
                            </li>
                            <li class="counter-list__li">
                                <a class="counter-list__link<?php if ($_SESSION['filterType'] == "T") { ?> is-active<?php } ?>" href="<?php echo $basehttp . "/filter-content/T"; ?>" title="<?php echo _t("Shemale") ?>">
                                    <span class="counter-list__icon"><span class="icon -caret-right"></span></span>
                                    <span class="counter-list__link-label"><?php echo _t("Shemale") ?></span>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>
        </div>
        <!-- box :: column :: end -->
    </div>

<?php } ?>