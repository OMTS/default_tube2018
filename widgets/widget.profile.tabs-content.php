<?php if ($_GET['controller'] === 'my_profile' || $_GET['controller'] === 'user_profile' || $_GET['controller'] === 'pornstar_bio') { ?>
    <?php
    $currentId;
    $name = 'NONE';
    $isProfile = $_GET['controller'] === 'my_profile' || $_GET['controller'] === 'user_profile';
    $isMyProfile = $_GET['controller'] === 'my_profile';
    $isUserProfile = $_GET['controller'] === 'user_profile';
    $isPornstar = $_GET['controller'] === 'pornstar_bio';

    if ($isMyProfile) {
        $currentId = $_SESSION[userid];
    }
    if ($isUserProfile) {
        $currentId = $id;
    }
    if ($isPornstar) {
        $currentId = $rrow['record_num'];
    }

    if ($isProfile) {
        $name = ucwords($urow['username']);
    }
    if ($isPornstar) {
        $name = $rrow['name'];
    }
    ?>

    <section class="regular-sec g-sec -sec-profile">
        <div class="wrapper">

            <div class="row">
                <!-- tab :: module :: start -->
                <div class="tab mod" data-tab-group="profile" data-tab-id="1">
                    <div class="tab__inner">

                        <div class="row">
                            <!-- box :: column :: start -->
                            <div class="box col -large-mrb">
                                <div class="box__inner">

                                    <div class="box__hd">
                                        <div class="box__hd-inner">
                                            <span class="box__hd-icon">
                                                <span class="icon -video"></span>
                                            </span>
                                            <h1 class="box__h"><?php echo $name; ?>'s <?php echo $isProfile ? _t("Recent Video Uploads") : _t('Videos'); ?></h1>

                                            <?php if ($isProfile) { ?>
                                                <div class="box__hd-utils">
                                                    <a class="btn -outlined" href="<?php echo $basehttp; ?>/uploads-by-user/<?php echo $currentId; ?>/" title="<?php echo _t("View all") ?>">
                                                        <span class="btn__icon">
                                                            <span class="icon -video"></span>
                                                        </span>
                                                        <span class="btn__label"><?php echo _t("View all") ?></span>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="box__bd">
                                        <div class="box__bd-inner">
                                            <div class="row">
                                                <?php
                                                if ($isProfile) {
                                                    userRecentUploads($currentId, 8);
                                                } else {
                                                    getModelsContent($currentId, 0);
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- box :: column :: end -->
                        </div>

                    </div>
                </div>
                <!-- tab :: module :: end -->

                <!-- tab :: module :: start -->
                <div class="tab mod" data-tab-group="profile" data-tab-id="2">
                    <div class="tab__inner">

                        <div class="row">
                            <!-- box :: column :: start -->
                            <div class="box col -large-mrb">
                                <div class="box__inner">

                                    <div class="box__hd">
                                        <div class="box__hd-inner">
                                            <span class="box__hd-icon">
                                                <span class="icon -camera"></span>
                                            </span>
                                            <h1 class="box__h"><?php echo $name; ?>'s <?php echo $isProfile ? _t("Recent Photo Uploads") : _t('Photos'); ?></h1>
                                            <?php if ($isProfile) { ?>
                                                <div class="box__hd-utils">
                                                    <a class="btn -outlined" href="<?php echo $basehttp; ?>/uploads-by-user/<?php echo $currentId; ?>/" title="<?php echo _t("View all") ?>">
                                                        <span class="btn__icon">
                                                            <span class="icon -camera"></span>
                                                        </span>
                                                        <span class="btn__label"><?php echo _t("View all") ?></span>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="box__bd">
                                        <div class="box__bd-inner">
                                            <div class="row">
                                                <?php if ($isProfile) { ?>
                                                    <?php userRecentUploads($currentId, 8, 'photos'); ?>
                                                <?php } else { ?>
                                                    <?php getModelsContent($currentId, 1); ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- box :: column :: end -->
                        </div>

                    </div>
                </div>
                <!-- tab :: module :: end -->

                <!-- tab :: module :: start -->
                <div class="tab mod" data-tab-group="profile" data-tab-id="3">
                    <div class="tab__inner">

                        <div class="row">
                            <!-- box :: column :: start -->
                            <div class="box col -large-mrb">
                                <div class="box__inner">

                                    <div class="box__hd">
                                        <div class="box__hd-inner">
                                            <span class="box__hd-icon">
                                                <span class="icon -wall"></span>
                                            </span>
                                            <h1 class="box__h"><?php echo $name; ?>'s <?php echo _t("Wall") ?></h1>
                                            <div class="box__hd-utils">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="box__bd">
                                        <div class="box__bd-inner">
                                            <div class="row">
                                                <?php if ($allowDisplayComments) { ?>


                                                    <?php $contentID = $currentId; //$urow['record_num'];  ?>
                                                    <?php $commentsType = $isProfile ? 2 : 1; ?>
                                                    <?php include('widget.comments.php'); ?>



                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- box :: column :: end -->
                        </div>

                    </div>
                </div>
                <!-- tab :: module :: end -->

                <!-- tab :: module :: start -->
                <div class="tab mod" data-tab-group="profile" data-tab-id="4">
                    <div class="tab__inner">

                        <div class="row">
                            <!-- box :: column :: start -->
                            <div class="box col -large-mrb">
                                <div class="box__inner">

                                    <div class="box__hd">
                                        <div class="box__hd-inner">
                                            <span class="box__hd-icon">
                                                <span class="icon -group"></span>
                                            </span>
                                            <h1 class="box__h"><?php echo ucwords($urow['username']); ?>'s <?php echo _t("Friends") ?></h1>
                                            <div class="box__hd-utils">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="box__bd">
                                        <div class="box__bd-inner">
                                            <div class="row">
                                                <?php getUsersFriends($currentId, 8); ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- box :: column :: end -->
                        </div>

                    </div>
                </div>
                <!-- tab :: module :: end -->



                <!-- tab :: module :: start -->
                <div class="tab mod" data-tab-group="profile" data-tab-id="5">
                    <div class="tab__inner">

                        <div class="row">
                            <!-- box :: column :: start -->
                            <div class="box col -large-mrb">
                                <div class="box__inner">

                                    <div class="box__hd">
                                        <div class="box__hd-inner">
                                            <span class="box__hd-icon">
                                                <span class="icon -send"></span>
                                            </span>
                                            <h1 class="box__h"><?php echo _t("Send message to") ?> <?php echo ucwords($urow['username']); ?></h1>
                                            <div class="box__hd-utils">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="box__bd">
                                        <div class="box__bd-inner">

                                            <?php
                                            $canSendPm = $_SESSION['userid'] !== $urow['record_num'];
                                            $isLogged = !!$_SESSION['userid'];
                                            $isMsgSent = !!$success;
                                            ?>

                                            <?php if ($canSendPm) { ?>
                                                <?php if (!$isLogged) { ?>
                                                    <div class="notification alert-warning">
                                                        <p><?php echo _t("You must be logged in to send messages") ?>. <?php echo _t("Please") ?> <a href='<? echo $basehttp; ?>/login'><?php echo _t("login") ?></a> <?php echo _t("or") ?> <a href='<? echo $basehttp; ?>/signup'><?php echo _t("signup (free)") ?></a></p>
                                                    </div>
                                                <?php } else { ?>
                                                    <?php if (!$isMsgSent) { ?>
                                                        <div class="row">
                                                            <!-- fr :: column :: start -->
                                                            <div class="fr col">
                                                                <div class="fr__inner">
                                                                    <form class="fr__form" method="post" action="">
                                                                        <div class="fr__row">
                                                                            <div class="fr__col">
                                                                                <input type="text" placeholder="<?php echo _t("Subject") ?>" value="" name="subject" class="fr__input -input-text" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="fr__row">
                                                                            <div class="fr__col">
                                                                                <textarea name="text" placeholder="<?php echo _t("Your Message") ?>" class="fr__input -input-text-area"></textarea>
                                                                            </div>
                                                                        </div>
																		<div class="row">
									
																			<div class="form-item-col col col-half">
																				<div class="form-item-inner-col inner-col">
																				 
																					<input id="captchaaa" type="text" size="50" placeholder="<?php echo _t("Human?") ?>" name="captchaaa" />
																				</div>
																			</div>
																			
																			
																			<div class="form-item-col col col-half">
																				<div class="form-item-inner-col inner-col">
																					<img src='<? echo $basehttp; ?>/captcha.php' height=38>
																				</div>
																			</div>
																			
																			
																			
																		</div>
                                                                        <div class="fr__row">
                                                                            <div class="fr__col -center">
                                                                                <button class="btn -primary fr__btn -login" type="submit" name="send"><?php echo _t("Send Message") ?></button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <!-- fr :: column :: end -->
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>


                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- box :: column :: end -->
                        </div>

                    </div>
                </div>
                <!-- tab :: module :: end -->

            </div>
        </div>
    </section>
<?php } ?>