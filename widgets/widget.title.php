<div class="box__hd">
    <?php 
       
        // echo 'mod:'.$_GET[mode];
        $icon = '-video';
        
        $ctr = $_GET[controller];
        $md = $_GET[mode];
        
        if($ctr === 'login') { $icon = '-user'; }
        if($ctr === 'signup') { $icon = '-user'; }
        if($ctr === 'my_profile') { $icon = '-user'; }
        if($ctr === 'forgot_pass') { $icon = '-user'; }
        if($ctr === 'user_profile') { $icon = '-user'; }
        if($ctr === 'edit_profile') { $icon = '-edit'; }
        if($ctr === 'my_friends') { $icon = '-group'; }
        if($ctr === 'paysites') { $icon = '-money'; }
        if($ctr === 'tags') { $icon = '-tag'; }
        if($md === 'photos') { $icon = '-camera'; }
        if($ctr === 'pornstar_bio') { $icon = '-star'; }
        if($ctr === 'pornstars') { $icon = '-star'; }
        if($ctr === 'displayStatic') { $icon = '-doc'; }
        if($ctr === 'upload') { $icon = '-upload'; }
        
        if($ctr === 'index' && $md === 'favorites') { $icon = '-heart'; }
        
        if(strpos($_SERVER['REQUEST_URI'], 'mailbox') !== false) { $icon = '-email'; }
        
    ?>
    <div class="box__hd-inner">

        <span class="box__hd-icon">
            <span class="icon <?php echo $icon; ?>"></span>
        </span>
        
        <h2 class="box__h"><?php echo $headertitle; ?> <?php if (checkNav('index')) { ?><span class="box__h-legend">(<?php echo showVideosCounter(); ?>)</span><?php } ?></h2>
        
        <?php getWidget('widget.profile.tabs.php'); ?>
        
        <?php getWidget('widget.profile.tabs-friends.php'); ?>
        

        <?php if (checkNav('index') || checkNav('most-viewed') || checkNav('top-rated') || checkNav('most-recent')) { ?>
            <?php
            $fake_select = _t("- select -");
            if (checkNav('most-recent')) {
                $fake_select = _t("Newest");
            }
            if (checkNav('top-rated')) {
                $fake_select = _t("Top Rated");
            }
            if (checkNav('most-viewed')) {
                $fake_select = _t("Most Viewed");
            }
            ?>
            <div class="box__hd-utils">
                <button class="btn -outlined" data-mb="dropdown">
                    <span class="btn__label"><?php echo $fake_select; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li"><a title="<?php echo _t("Newest") ?>" href="<?php echo $basehttp; ?>/most-recent/" class="drop-list__link"><?php echo _t("Newest") ?></a></li>
                    <li class="drop-list__li"><a title="<?php echo _t("Most Viewed") ?>" href="<?php echo $basehttp; ?>/most-viewed/" class="drop-list__link"><?php echo _t("Most Viewed") ?></a></li>
                    <li class="drop-list__li"><a title="<?php echo _t("Top Rated") ?>" href="<?php echo $basehttp; ?>/top-rated/" class="drop-list__link"><?php echo _t("Top Rated") ?></a></li>
                </ul>
            </div>
        <?php } ?>


        <?php if ($_GET[controller] == "index" && $_GET['mode'] != "channel" && $_GET['mode'] != "search" && $_GET['mode'] != "paysites" && $_GET['mode'] != 'photos') { ?>
            <?php // filtry po dacie - widoczne na podstronach z wyjatkiem HOME i CHANNELS i SEARCH i PAYSITES ?>
            <?php
            $fake_select2 = _t("All time");
            if ($_GET['dateRange'] == 'day') {
                $fake_select2 = _t("Today");
            }
            if ($_GET['dateRange'] == 'week') {
                $fake_select2 = _t("Last 7 days");
            }
            if ($_GET['dateRange'] == 'month') {
                $fake_select2 = _t("Last 30 days");
            }

            if ($_GET['mode']) {
                $modeUrl = $_GET['mode'];
            } else {
                $modeUrl = "most-recent";
            }

            if (is_numeric($_GET['user'])) {
                $modeUrl .= '/' . (int) $_GET['user'];
            }
            ?>
            <div class="box__hd-utils">
                <button class="btn -outlined" data-mb="dropdown">
                    <span class="btn__label"><?php echo $fake_select2; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("All Time") ?>" href="<?php echo $basehttp; ?>/<?php echo $modeUrl; ?>/"><?php echo _t("All time") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Today") ?>" href="<?php echo $basehttp; ?>/<?php echo $modeUrl; ?>/day/"><?php echo _t("Today") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Last 7 days") ?>" href="<?php echo $basehttp; ?>/<?php echo $modeUrl; ?>/week/"><?php echo _t("Last 7 days") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Last 30 days") ?>" href="<?php echo $basehttp; ?>/<?php echo $modeUrl; ?>/month/"><?php echo _t("Last 30 days") ?></a></li>
                </ul>
            </div>

        <?php } ?>

        <?php if ($_GET['mode'] == 'channel' && is_numeric($_GET['channel'])) { ?>
            <?php // SORTOWANIE TYLKO W CHANNELS ?>
            <?php
            $fake_select3 = _t("Newest");
            if ($_GET[sortby] == 'length') {
                $fake_select3 = _t("Longest");
            }
            if ($_GET[sortby] == 'rating') {
                $fake_select3 = _t("Top Rated");
            }
            if ($_GET[sortby] == 'views') {
                $fake_select3 = _t("Most Viewed");
            }
            ?>

            <div class="box__hd-utils">
                <button class="btn -outlined" data-mb="dropdown">
                    <span class="btn__label"><?php echo $fake_select3; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Newest") ?>" href="<?php echo generateUrl('channel', $channel_name, $_GET['channel']); ?>"><?php echo _t("Newest") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Longest") ?>" href="<?php echo generateUrl('channel', $channel_name, $_GET['channel']); ?>longest/"><?php echo _t("Longest") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Top Rated") ?>" href="<?php echo generateUrl('channel', $channel_name, $_GET['channel']); ?>rating/"><?php echo _t("Top Rated") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Most Viewed") ?>" href="<?php echo generateUrl('channel', $channel_name, $_GET['channel']); ?>views/"><?php echo _t("Most Viewed") ?></a></li>

                </ul>
            </div>
        <?php } ?>

        <?php if ($_GET['mode'] == 'photos') { ?>
            <?php // SORTOWANIE TYLKO W GALERII ?>
            <?php
            $fake_selectphoto = _t("Newest");
            if ($_GET['sortby'] == 'most-discussed') {
                $fake_selectphoto = _t("Most Discussed");
            }
            if ($_GET['sortby'] == 'top-rated') {
                $fake_selectphoto = _t("Top Rated");
            }
            if ($_GET['sortby'] == 'most-viewed') {
                $fake_selectphoto = _t("Most Viewed");
            }
            ?>
            <div class="box__hd-utils">
                <button class="btn -outlined" data-mb="dropdown">
                    <span class="btn__label"><?php echo $fake_selectphoto; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Newest") ?>" href="<? echo $basehttp; ?>/photos/"><?php echo _t("Newest") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Top Rated") ?>" href="<? echo $basehttp; ?>/photos/top-rated/"><?php echo _t("Top Rated") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Most Viewed") ?>" href="<? echo $basehttp; ?>/photos/most-viewed/"><?php echo _t("Most Viewed") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Most Discussed") ?>" href="<? echo $basehttp; ?>/photos/most-discussed/"><?php echo _t("Most Discussed") ?></a></li>
                </ul>
            </div>

        <?php } ?>


        <?php if ($_GET['controller'] == 'pornstars') { ?>
            <?php // SORTOWANIE TYLKO PORNSTARS  ?>
            <?php
            $fake_select4 = _t("Alphabetical");
            if ($_GET[sortby] == 'rating') {
                $fake_select4 = _t("Top Rated");
            }
            $letter = "";
            if ($_GET['letter']) {
                $letter = $_GET['letter'] . "/";
            }
            ?>
            <div class="box__hd-utils">
                <button class="btn -outlined" data-mb="dropdown">
                    <span class="btn__label"><?php echo $fake_select4; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Alphabetical") ?>" href="<?php echo $basehttp; ?>/models/<?php echo $letter; ?>"><?php echo _t("Alphabetical") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Top Rated") ?>" href="<?php echo $basehttp; ?>/models/<?php echo $letter; ?>rating/"><?php echo _t("Top Rated") ?></a></li>
                </ul>
            </div>

        <?php } ?>
        
        
        <?php if ($_GET['mode'] == 'search' && $_GET['type'] != 'members') { ?>
            <?php // SORTOWANIE TYLKO W CHANNELS  ?>
            <?php
            $fake_select5 = _t("Most Relevant");
            if ($_GET[sortby] == 'length') {
                $fake_select5 = _t("Longest");
            }
            if ($_GET[sortby] == 'rating') {
                $fake_select5 = _t("Top Rated");
            }
            if ($_GET[sortby] == 'views') {
                $fake_select5 = _t("Most Viewed");
            }
            if ($_GET[sortby] == 'newest') {
                $fake_select5 = _t("Newest");
            }

            $q = $_GET[q] . "/";
            $t = "";
            if ($_GET[type]) {
                $t = $_GET[type] . "/";
            }
            ?>
        <div class="box__hd-utils">
                <button class="btn -outlined" data-mb="dropdown">
                    <span class="btn__label"><?php echo $fake_select5; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Most Relevant") ?>" href="<?php echo $basehttp . "/search/" . $t . $q; ?>"><?php echo _t("Most Relevant") ?></a></li>
                        <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Newest") ?>" href="<?php echo $basehttp . "/search/" . $t . $q; ?>newest/"><?php echo _t("Newest") ?></a></li>
                        <?php if ($_GET[type] == 'videos') { ?><li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Longest") ?>" href="<?php echo $basehttp . "/search/" . $t . $q . "longest/"; ?>"><?php echo _t("Longest") ?></a></li><?php } ?>
                        <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Top Rated") ?>" href="<?php echo $basehttp . "/search/" . $t . $q . "rating/"; ?>"><?php echo _t("Top Rated") ?></a></li>
                        <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Most Viewed") ?>" href="<?php echo $basehttp . "/search/" . $t . $q . "views/"; ?>"><?php echo _t("Most Viewed") ?></a></li>
                </ul>
            </div>
        <?php } ?>
        
        
        
        <?php if ($_GET['mode'] == 'paysites' && is_numeric($_GET['paysite'])) { ?>
            <?php // SORTOWANIE TYLKO W PAYSTIES ?>
            <?php
            $fake_select6 = _t("Newest");
            if ($_GET[sortby] == 'length') {
                $fake_select6 = _t("Longest");
            }
            if ($_GET[sortby] == 'rating') {
                $fake_select6 = _t("Top Rated");
            }
            if ($_GET[sortby] == 'views') {
                $fake_select6 = _t("Most Viewed");
            }
            ?>
        
        <div class="box__hd-utils">
                <button class="btn -outlined" data-mb="dropdown">
                    <span class="btn__label"><?php echo $fake_select6; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Newest") ?>" href="<?php echo generateUrl('paysite', $name, $_GET['paysite']); ?>"><?php echo _t("Newest") ?></a></li>
                        <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Longest") ?>" href="<?php echo generateUrl('paysite', $name, $_GET['paysite']); ?>longest/"><?php echo _t("Longest") ?></a></li>
                        <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Top Rated") ?>" href="<?php echo generateUrl('paysite', $name, $_GET['paysite']); ?>rating/"><?php echo _t("Top Rated") ?></a></li>
                        <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Most Viewed") ?>" href="<?php echo generateUrl('paysite', $name, $_GET['paysite']); ?>views/"><?php echo _t("Most Viewed") ?></a></li>
                </ul>
            </div>

        <?php } ?>
        
        
        <?php if ($_GET[controller] == "members" || ($_GET[mode] == "search" && $_GET[type] == "members")) { ?>
            <?php // SORTOWANIE TYLKO W MEMBERS ?>
            <?php
            $fake_select7 = _t("Alphabetical");

            if ($_SESSION['ms']['order'] == 'ORDER BY date_joined') {
                $fake_select7 = _t("Join Date");
            } else {
                $fake_select7 = _t("Alphabetical");
            }
            ?>
            <div class="box__hd-utils">
                <button class="btn -outlined" data-mb="dropdown">
                    <span class="btn__label"><?php echo $fake_select7; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Alphabetical") ?>" href="<?php echo $basehttp; ?>/members/?sortby=alphabetical"><?php echo _t("Alphabetical") ?></a></li>
                    <li class="drop-list__li"><a class="drop-list__link" title="<?php echo _t("Join Date") ?>" href="<?php echo $basehttp; ?>/members/?sortby=date_joined"><?php echo _t("Join Date") ?></a></li>
                </ul>
            </div>

        <?php } ?>
    </div>
</div>
