<!-- box :: column :: start -->
<div class="box col -large-mrb">
    <div class="box__inner">
        <div class="box__hd">
            <div class="box__hd-inner">
                <span class="box__hd-icon">
                    <span class="icon -comments"></span>
                </span>
                <h2 class="box__h -sm"><?php echo _t("Comments"); ?>
                    <span class="box__h-legend">(<?php echo getTotalComments($rrow['record_num']); ?>)</span>
                </h2>
            </div>
        </div>

        <div class="box__bd">
            <div class="box__bd-inner" data-mb="load-comments" data-opt-url="<?php echo $template_url; ?>/template.ajax_comments.php" data-opt-id="<?php echo $contentID; ?>" data-opt-type="<?php echo $commentsType; ?>">
        
            </div> 
        </div>
        <?php if ($allowAddComments) { ?>
            <?php if ($_SESSION['userid']) { ?>
                <div class="box__hd">
                    <div class="box__hd-inner">
                        <h2 class="box__h -sm"><?php echo _t("Add comment") ?>
                        </h2>
                    </div>
                </div>
                <!-- comment-alert -->
                <div class="box__bd g--hidden">
                    <div class="box__bd-inner" data-mb="comment-alert">
                        
                    </div>
                </div>
                <!-- comment-alert END -->
                <div class="box__bd">
                    <div class="box__bd-inner">
                        <div class="row">

                            <!-- form :: column :: start -->
                            <form class="form col" name="comments" action="" data-mb="add-comment">
                                <input type='hidden' name='id' id='id' value='<?php echo $contentID; ?>' />
                                <input type='hidden' name='type' id='type' value='<?php echo $commentsType; ?>' />
                                <div class="form__inner">
                                    <div class="row">
                                        <div class="form__input -textarea col">
                                            <textarea placeholder="<?php echo _t("Your comment") ?>" value="" name="comment" id="comment" class="form__input-textarea"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form__input -captcha col captcha">
                                            <img class="captcha captcha-img captcha__img" src="<?php echo $basehttp; ?>/captcha.php" data-mb="captcha-img">
                                            <input type="text" placeholder="<?php echo _t("Human?") ?>" value="" name="captcha" id="captchaCom" class="form__input-text -captcha captcha__input" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form__output -submit col">
                                            
                                            <button class="btn -primary" id="button" name="button" type="submit">
                                                <span class="btn__label"><?php echo _t("Post comment") ?></span>
                                            </button>
                                        
                                            
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- form :: column :: end -->

                        </div>
                    </div>
                </div>
                <?php } else { ?>
                <div class="box__ft">
                    <div class="box__ft-inner">
                        <?php echo _t("You must be logged in to post wall comments") ?>. <?php echo _t("Please") ?> <a class="box__ft-link" href='<?php echo $basehttp; ?>/login'><?php echo _t("Login") ?></a> <?php echo _t("or") ?> <a class="box__ft-link" href='<?php echo $basehttp; ?>/signup'><?php echo _t("Signup (free)") ?></a>.
                    </div>
                </div>
            <?php } ?>
        <?php } ?>

    </div>
</div>
