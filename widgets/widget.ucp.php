<!-- ucp :: column :: start -->
<div class="ucp col">
    <div class="ucp__inner">
        <ul class="ucp-list">
            <?php if (!isLoggedIn()) { ?>

                <li class="ucp-list__li -login">
                    <a href="<?php echo $basehttp; ?>/login" title="<?php echo _t("Login") ?>" class="ucp-list__link">
                        <span class="ucp-list__icon">
                            <span class="icon -user"></span>
                        </span>
                        <span class="ucp-list__label"><?php echo _t("Login") ?></span>
                    </a>
                </li>

                <li class="ucp-list__li -register">
                    <a href="<?php echo $basehttp; ?>/signup" title="<?php echo _t("Signup") ?>" class="ucp-list__link">
                        <span class="ucp-list__icon">
                            <span class="icon -edit"></span>
                        </span>
                        <span class="ucp-list__label"><?php echo _t("Sign up") ?></span>
                    </a>
                </li>

            <?php } else { ?>
                <li class="ucp-list__li">
                    <button class="ucp-list__link is-active" data-mb="dropdown">
                        <span class="ucp-list__icon">
                            <span class="icon -user"></span>
                        </span>
                        <span class="ucp-list__label"><?php echo $_SESSION['username']; ?></span>
                        <span class="ucp-list__icon">
                            <span class="icon -caret-down"></span>
                        </span>
                    </button>

                    <ul class="drop-list g--dropdown -md">

                        <li class="drop-list__li">
                            <a class="drop-list__link -align-left" title="<?php echo _t("My profile") ?>" href="<?php echo $basehttp; ?>/my-profile">
                                <span class="drop-list__icon">
                                    <span class="icon -user"></span>
                                </span>
                                <span class="drop-list__label">
                                    <?php echo _t("My profile") ?>
                                </span>

                            </a>
                        </li>

                        <li class="drop-list__li">
                            <a class="drop-list__link -align-left" title="<?php echo _t("Edit profile") ?>" href="<?php echo $basehttp; ?>/edit-profile">
                                <span class="drop-list__icon">
                                    <span class="icon -edit"></span>
                                </span>
                                <span class="drop-list__label">
                                    <?php echo _t("Edit profile") ?>
                                </span>
                            </a>
                        </li>

                        <li class="drop-list__li">
                            <?php $countInbox = dbValue("SELECT COUNT(`record_num`) AS `count` FROM `mail` WHERE `to_user` = '{$_SESSION['userid']}' AND `recipient_deleted` = 0 AND `recipient_read` = 0", 'count'); ?>
                            <a class="drop-list__link -align-left" title="<?php echo _t("Messages") ?>" href="<?php echo $basehttp; ?>/mailbox/">
                                <span class="drop-list__icon">
                                    <span class="icon -email"></span>
                                </span>
                                <span class="drop-list__label">
                                    <?php echo _t("Messages") ?> (<?php echo $countInbox; ?>)
                                </span>
                            </a>
                        </li>

                        <?php if ($allowAddFavorites) { ?>
                            <li class="drop-list__li">
                                <a class="drop-list__link -align-left" title="<?php echo _t("Favorites") ?>" href="<?php echo $basehttp; ?>/favorites/">
                                    <span class="drop-list__icon">
                                        <span class="icon -heart"></span>
                                    </span>
                                    <span class="drop-list__label">
                                        <?php echo _t("Favorites") ?>
                                    </span>
                                </a>
                            </li>
                        <?php } ?>

                        <li class="drop-list__li">
                            <?php $countFriendsInvites = dbValue("SELECT COUNT(`record_num`) AS `count` FROM `friends` WHERE `friend` = '{$_SESSION['userid']}' AND `approved` = 0", 'count'); ?>
                            <a class="drop-list__link -align-left" title="<?php echo _t("Friends") ?>" href="<?php echo $basehttp; ?>/my-friends">
                                <span class="drop-list__icon">
                                    <span class="icon -group"></span>
                                </span>
                                <span class="drop-list__label">
                                    <?php echo _t("Friends") ?> (<?php echo $countFriendsInvites; ?>)
                                </span>
                            </a>
                        </li>

                        <li class="drop-list__li">
                            <a class="drop-list__link -align-left" title="<?php echo _t("Logout") ?>" href="<?php echo $basehttp; ?>/logout">
                                <span class="drop-list__icon">
                                    <span class="icon -sign-out"></span>
                                </span>
                                <span class="drop-list__label">
                                    <?php echo _t("Logout") ?>
                                </span>
                            </a>
                        </li>

                    </ul>
                </li>
            <?php } ?>

            <?php getWidget('widget.colorselector.php'); ?>

            <?php getWidget('widget.language.php'); ?>

        </ul>
    </div>
</div>
<!-- ucp :: column :: end -->
