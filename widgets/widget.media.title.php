<div class="row">
    <!-- mhead :: column :: start -->
    <header class="mhead col">
        <div class="mhead__inner">
            <h1 class="mhead__h"><?php echo $rrow['title']; ?></h1>
            <div class="mhead__utility">
                <?php include($basepath . '/includes/rating/templateTh.php'); ?>
            </div>
        </div>
    </header>
    <!-- mhead :: column :: end -->
</div>