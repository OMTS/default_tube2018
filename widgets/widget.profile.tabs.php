<?php if ($_GET['controller'] === 'my_profile' || $_GET['controller'] === 'user_profile' || $_GET['controller'] === 'pornstar_bio') {
    $isProfile = $_GET['controller'] === 'my_profile' || $_GET['controller'] === 'user_profile';
    $isMyProfile = $_GET['controller'] === 'my_profile';
    $isUserProfile = $_GET['controller'] === 'user_profile';
    $isPornstar = $_GET['controller'] === 'pornstar_bio';
    ?>


    <div class="box__hd-utils">

        <ul class="tabs-list -in-box-hd">
            <li class="tabs-list__li">
                <a href="#" class="tabs-list__link" data-mb-tab="1">
                    <span class="tabs-list__icon">
                        <span class="icon -video"></span>
                    </span>
                    <span class="tabs-list__label"><?php echo _t("Videos") ?></span>
                </a>
            </li>

            <li class="tabs-list__li">
                <a href="#" class="tabs-list__link" data-mb-tab="2">
                    <span class="tabs-list__icon">
                        <span class="icon -camera"></span>
                    </span>
                    <span class="tabs-list__label"><?php echo _t("Photos") ?></span>
                </a>
            </li>

            <li class="tabs-list__li">
                <a href="#" class="tabs-list__link" data-mb-tab="3">
                    <span class="tabs-list__icon">
                        <span class="icon -wall"></span>
                    </span>
                    <span class="tabs-list__label"><?php echo _t("Wall") ?></span>
                </a>
            </li>
            <?php if ($isProfile) { ?>
            <li class="tabs-list__li">
                <a href="#" class="tabs-list__link" data-mb-tab="4">
                    <span class="tabs-list__icon">
                        <span class="icon -group"></span>
                    </span>
                    <span class="tabs-list__label"><?php echo _t("Friends") ?></span>
                </a>
            </li>
            <?php } ?>
            <?php if ($isUserProfile) { ?>
            
            <li class="tabs-list__li">
                <a href="#hrefText" class="tabs-list__link" data-mb-tab="5">
                    <span class="tabs-list__icon">
                        <span class="icon -send"></span>
                    </span>
                    <span class="tabs-list__label"><?php echo _t("Message") ?></span>
                </a>
            </li>
            <?php } ?>
        </ul>

    </div>      
<?php } ?>