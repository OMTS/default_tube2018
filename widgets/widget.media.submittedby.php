<?php
$tag = 'span';
$url = '';
$name = _t("Anonymous");
$avatarUrl = getUserAvatarUrl($rrow['submitter']);

if (!$rrow['submitter'] == 0) {
    $tag = 'a';
    $url = generateUrl('user', $rrow['username'], $rrow['submitter']);
    $name = $rrow['username'];
}
?>
<!-- submittedby :: mod :: start -->
<div class="submittedby mod">
    <div class="submittedby__inner">

        <div class="submittedby__thumb">
            <!-- avatar :: module :: start -->
            <div class="avatar mod">
                <div class="avatar__inner">
                    <span class="avata__thumb">
                        <img src="<?php echo $avatarUrl; ?>" class="avatar__img" alt="<?php echo $name; ?>">
                    </span>
                </div>
            </div>
            <!-- avatar :: module :: end -->
        </div>

        <div class="submittedby__ct">
            <div class="submittedby__label"><?php echo _t("Submitted by") ?></div>
            <div class="submittedby__bd">

                <<?php echo $tag; ?> <?php if ($url) { ?>href="<?php echo $url; ?>"<?php } ?> class="submittedby__link" title="<?php echo $name; ?>"><?php echo $name; ?></<?php echo $tag; ?>>
            </div>
        </div>

    </div>
</div>
<!-- submittedby :: mod :: end -->