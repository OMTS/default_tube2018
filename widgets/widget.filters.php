<!-- top-outlines :: column :: start -->
<div class="top-outlines col">
    <div class="top-outlines__inner">

        <ul class="filter-list">
            <li class="filter-list__li">
                <?php getWidget('widget.upload.php'); ?>
            </li>
            <?php /*
            <li class="filter-list__li">
                <a href="<?php echo $basehttp; ?>/most-recent/" class="btn -outlined <?php if ($_GET['mode'] == 'most-recent') { ?> is-active<?php } ?>">
                    <span class="btn__label"><?php echo _t("Newest") ?></span>
                </a>
            </li>
            <li class="filter-list__li">
                <a href="<?php echo $basehttp; ?>/longest/" class="btn -outlined <?php if ($_GET['mode'] == 'longest') { ?> is-active<?php } ?>">
                    <span class="btn__label"><?php echo _t("Longest") ?></span>
                </a>
            </li>
            <li class="filter-list__li">
                <?php
                $label = _t("All time");
                $mode = 'most-recent';
                if ($_GET['mode'] == 'most-recent' || $_GET['mode'] == 'favorites' || $_GET['mode'] == 'my-uploads' || $_GET['mode'] == 'most-discussed' || $_GET['mode'] == 'most-viewed' || $_GET['mode'] == 'longest' || $_GET['mode'] == 'top-rated' || $_GET['mode'] == 'random') {
                    $mode = $_GET['mode'];
                }

                $range = $_GET['dateRange'] ? $_GET['dateRange'] : '';

                if ($range) {
                    if ($_GET['dateRange'] == 'day') {
                        $label = _t('Today');
                    }
                    if ($_GET['dateRange'] == 'week') {
                        $label = _t('Last 7 days');
                    }
                    if ($_GET['dateRange'] == 'month') {
                        $label = _t('Last 30 days');
                    }
                }

                $link = $basehttp . '/' . $mode;
                ?>
                <button class="btn -outlined" data-mb="dropdown">

                    <span class="btn__label"><?php echo $label; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li">
                        <a href="<?php echo $link; ?>/" class="drop-list__link"><?php echo _t("All time") ?></a>
                    </li>
                    <li class="drop-list__li">
                        <a href="<?php echo $link; ?>/day/" class="drop-list__link"><?php echo _t("Today") ?></a>
                    </li>
                    <li class="drop-list__li">
                        <a href="<?php echo $link; ?>/week/" class="drop-list__link"><?php echo _t("Last 7 days") ?></a>
                    </li>
                    <li class="drop-list__li">
                        <a href="<?php echo $link; ?>/month/" class="drop-list__link"><?php echo _t("Last 30 days") ?></a>
                    </li>
                </ul>
            </li>

            <li class="filter-list__li">
                <?php
                    $label2 = _t('All');
                    
                    if($_GET['mode'] == 'most-discussed') { $label2 = _t('Comments'); }
                    if($_GET['mode'] == 'favorites') { $label2 = _t('Favorites'); }
                    if($_GET['mode'] == 'random') { $label2 = _t('Random'); }
                    
                ?>
                
                <button class="btn -outlined" data-mb="dropdown">
                    <span class="btn__icon">
                        <span class="icon -plus"></span>
                    </span>
                    <span class="btn__label"><?php echo $label2; ?></span>
                    <span class="btn__icon">
                        <span class="icon -caret-down"></span>
                    </span>
                </button>
                <ul class="drop-list -md g--dropdown">
                    <li class="drop-list__li">
                        <a href="<?php echo $basehttp; ?>/videos/" class="drop-list__link"><?php echo _t("All") ?></a>
                    </li>
                    <li class="drop-list__li">
                        <a href="<?php echo $basehttp; ?>/most-discussed/" class="drop-list__link"><?php echo _t("Comments") ?></a>
                    </li>
                    <li class="drop-list__li">
                        <a href="<?php echo $basehttp; ?>/favorites/" class="drop-list__link"><?php echo _t("Favorites") ?></a>
                    </li>
                    <li class="drop-list__li">
                        <a href="<?php echo $basehttp; ?>/random/" class="drop-list__link"><?php echo _t("Random") ?></a>
                    </li>
                </ul>
            </li>
            */ ?>
            <li class="filter-list__li">
                <div class="btn-group">
                    <a href="<?php echo $basehttp; ?>/filters?hd=false&ref=<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn -outlined -sqr<?php echo ($_SESSION['filters']['hd'] == '0' || !isset($_SESSION['filters']['hd'])) ? " is-active" : ""; ?>">
                        <span class="btn__label"><?php echo _t("ALL") ?></span>
                    </a>
                    <a href="<?php echo $basehttp; ?>/filters?hd=true&ref=<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn -outlined -sqr<?php echo $_SESSION['filters']['hd'] == '1' ? " is-active" : ""; ?>">
                        <span class="btn__label"><?php echo _t("HD") ?></span>
                    </a>
                </div>
            </li>
            
            <li class="filter-list__li">
                <a href="<?php echo $basehttp; ?>/filters?vr=<?php echo $_SESSION['filters']['vr'] == '1' ? "false" : "true"; ?>&ref=<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn -outlined -sqr<?php echo $_SESSION['filters']['vr'] == '1' ? " is-active" : ""; ?>">
                    <span class="btn__label"><?php echo _t("VR") ?></span>
                </a>
            </li>
        </ul>

    </div>
</div>
<!-- top-outlines :: column :: end -->