<section class="regular-sec g-sec -sec-top-content">
    <div class="wrapper">
        <div class="row">
            <?php getWidget('widget.ad.top.php'); ?>
        </div>
        <div class="row">
            <?php getTemplate('template.notifications.php'); ?>
        </div>
    </div>
</section>