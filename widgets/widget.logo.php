<!-- logo :: column :: start -->
<div class="logo col">
    <div class="logo__inner">
        <a href="<?php echo $basehttp; ?>" class="logo__link" title="<?php echo $sitename; ?>">
            <img src="<?php echo $template_url; ?>/images/<?php echo ($_COOKIE["mbColorScheme"] == 1) || ($_COOKIE["user.mb.invertedColors"] == true) ? "logo_2.png" : "logo.png"; ?>" class="logo__img" alt="<?php echo _t("Home") ?> - <?php echo $sitename; ?>" />
        </a>
    </div>
</div>
<!-- logo :: column :: end -->