<?php if ($_GET['controller'] === 'my_friends') { ?>
    <div class="box__hd-utils">

        <ul class="tabs-list -in-box-hd">
            <li class="tabs-list__li">
                <a href="#hrefText" class="tabs-list__link" data-mb-tab="1">
                    <span class="tabs-list__icon">
                        <span class="icon -hot"></span>
                    </span>
                    <span class="tabs-list__label"><?php echo _t("New invitations") ?></span>
                </a>
            </li>

            <li class="tabs-list__li">
                <a href="#hrefText" class="tabs-list__link" data-mb-tab="2">
                    <span class="tabs-list__icon">
                        <span class="icon -group"></span>
                    </span>
                    <span class="tabs-list__label"><?php echo _t("My friends") ?></span>
                </a>
            </li>

            <li class="tabs-list__li">
                <a href="#hrefText" class="tabs-list__link" data-mb-tab="3">
                    <span class="tabs-list__icon">
                        <span class="icon -email"></span>
                    </span>
                    <span class="tabs-list__label"><?php echo _t("Sent invitations") ?></span>
                </a>
            </li>
        </ul>

    </div>      
<?php } ?>