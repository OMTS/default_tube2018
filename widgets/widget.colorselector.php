<li class="ucp-list__li -color-select">
    <a href="<?php echo $basehttp; ?>/change-colors" class="ucp-list__link" title="<?php echo _t("Change colors") ?>" data-mb="change-color">
        <span class="ucp-list__icon">
            <span class="icon -bulp"></span>
        </span>
        <span class="ucp-list__label"><?php echo _t("Change colors") ?></span>
    </a>
</li>