<?php if ((checkNav('index') || $_GET['mode'] == 'search' || $_GET['controller'] == 'video') && $AWEmpireEnabled) { ?>
    <section class="regular-sec g-sec -sec-most-viewed">
        <div class="wrapper">
            <div class="row">
                <!-- box :: column :: start -->
                <div class="box col">
                    <div class="box__inner">

                        <div class="box__hd">
                            <div class="box__hd-inner">

                                <span class="box__hd-icon">
                                    <span class="icon -hot"></span>
                                </span>

                                <h2 class="box__h"><?php if($_GET['q']) { echo _t("Live Cam Search Results For: ").htmlentities($_GET['q']); } else { echo _t("Live Online Cams"); } ?></h2>

                                <div class="box__hd-utils">
                                    <a class="btn -outlined" href="<? echo $basehttp; ?>/online-cams/">
                                        <span class="btn__icon">
                                            <span class="icon -eye"></span>
                                        </span>
                                        <span class="btn__label"><?php echo _t("See more") ?></span>
                                    </a>
                                </div>

                            </div>
                        </div>

                        <div class="box__bd">
                            <div class="box__bd-inner">

                                <div class="row">

                                    <?php showAweVideos('template.awe_item.php', 8); ?>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <!-- box :: column :: end -->
            </div>
        </div>
    </section>
<?php } ?>