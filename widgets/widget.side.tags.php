<?php if($_GET[tagCloud]) { ?>
<div class="row">
        <!-- box :: column :: start -->
        <div class="box col -mrb">
            <div class="box__inner">
                <div class="box__hd">
                    <div class="box__hd-inner">

                        <span class="box__hd-icon -sm">
                            <span class="icon -money"></span>
                        </span>

                        <h2 class="box__h"><?php echo _t("Tag Cloud"); ?></h2>

                        <div class="box__hd-utils">
                            <button class="btn -outlined g--hidden-sm-up" data-mb-expand="box-tag-cloud" data-mb-options="{'activeToMaxScreenWidth': 768}">
                                <span class="btn__icon">
                                    <span class="icon -expand"></span>
                                </span>
                                <span class="btn__label -when-inactive"><?php echo _t("More") ?></span>
                                <span class="btn__label -when-active"><?php echo _t("Hide") ?></span>
                            </button>
                        </div>

                    </div>
                </div>
                <div class="box__bd" data-mb-expand-target="box-tag-cloud">
                    <div class="box__bd-inner">

                        <ul class="counter-list -paysites">
                            <?php echo tagCloud(); ?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <!-- box :: column :: end -->
    </div>
<?php } ?>