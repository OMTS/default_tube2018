<?php if (checkNav('index')) { ?>
<section class="regular-sec g-sec -sec-main-top">
    <div class="wrapper">
        <div class="row -content-aside -aside-left -stretch">

            <!-- aside :: column :: start -->
            <aside class="aside col">
                <div class="aside__inner">
                    <!-- CATEGORIES -->
                    <div class="row g--height-full">
                        <!-- box :: column :: start -->
                        <div class="box col g--height-full">
                            <div class="box__inner">
                                <div class="box__hd">
                                    <div class="box__hd-inner">

                                        <span class="box__hd-icon -sm">
                                            <span class="icon -list"></span>
                                        </span>

                                        <h2 class="box__h"><?php echo _t("Categories") ?> <span class="box__h-legend">(<?php echo getTotalChannels(); ?>)</span></h2>

                                        <div class="box__hd-utils">
                                            <button class="btn -outlined g--hidden-sm-up" data-mb-expand="box-1" data-mb-options="{'activeToMaxScreenWidth': 768}">
                                                <span class="btn__icon">
                                                    <span class="icon -expand"></span>
                                                </span>
                                                <span class="btn__label -when-inactive"><?php echo _t("More") ?></span>
                                                <span class="btn__label -when-active"><?php echo _t("Hide") ?></span>
                                            </button>
                                        </div>

                                    </div>
                                </div>
                                <div class="box__bd" data-mb-expand-target="box-1">
                                    <div class="box__bd-inner">

                                        <!-- scrl :: module :: start -->
                                        <div class="scrl mod" data-mb="scrollbar">
                                            <div class="scrl__inner">
                                                <ul class="counter-list">
                                                    <?php echo showChannels2("<li  class=\"counter-list__li\">", "</li>", true); ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- scrl :: module :: end -->

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- box :: column :: end -->
                    </div>
                    <!-- CATEGORIES :: END -->

                </div>
            </aside>
            <!-- aside :: column :: end -->


            <!-- main :: column :: start -->
            <main class="main col">
                <div class="main__inner">
                    <!-- MAIN -->
                    <div class="row">
                        <!-- box :: column :: start -->
                        <div class="box col">
                            <div class="box__inner">

                                <div class="box__hd">
                                    <div class="box__hd-inner">
                                        <span class="box__hd-icon">
                                            <span class="icon -eye"></span>
                                        </span>
                                        <h2 class="box__h"><?php echo _t("Being Watched Now") ?>
                                        </h2>
                                        <div class="box__hd-utils">
                                            <a class="btn -outlined" href="<? echo $basehttp; ?>/most-viewed/" title="<?php echo _t("Most Viewed") ?>">
                                                <span class="btn__icon">
                                                    <span class="icon -eye"></span>
                                                </span>
                                                <span class="btn__label"><?php echo _t("See more") ?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="box__bd">
                                    <div class="box__bd-inner">
                                        <div class="row -cols-narrow">
                                            <?php showBeingWatched('template.content_item.php', 6, 'indexRandom'); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- box :: column :: end -->
                    </div>
                    <!-- MAIN - END -->
                </div>
            </main>
            <!-- main :: column :: end -->


        </div>
    </div>
</section>
<?php } ?>