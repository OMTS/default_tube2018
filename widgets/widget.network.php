<!-- network :: column :: start -->
<div class="network col">
    <div class="network__inner">
        <ul class="network-list">
            <li class="network-list__li -prefix">
                <span class="network-list__prefix-label"><?php echo _t("Network") ?>:</span>
            </li>
            <li class="network-list__li">
                <a href="https://www.mechbunny.com/" class="network-list__link">
                    <span class="network-list__link-label">Mechbunny</span>
                </a>
            </li>
            <li class="network-list__li">
                <a href="http://tube.mechbunny.com/" class="network-list__link">
                    <span class="network-list__link-label">Mechbunny Tube Script</span>
                </a>
            </li>
			 
    </div>
</div>
<!-- network :: column :: end -->