<?php 
$isVideo = $_GET['controller'] === 'video';
$isGallery = $_GET['controller'] === 'gallery';
?>
<div class="row">
    <!-- tabs :: column :: start -->
    <div class="tabs col">
        <div class="tabs__inner">
            <ul class="tabs-list">
                <li class="tabs-list__li">
                    <a href="#hrefText" class="tabs-list__link" data-mb-tab="1" title="<?php echo _t("About") ?>">
                        <span class="tabs-list__icon">
                            <span class="icon -info"></span>
                        </span>
                        <span class="tabs-list__label"><?php echo _t("About") ?></span>
                    </a>
                </li>

                <li class="tabs-list__li">
                    <a href="#hrefText" class="tabs-list__link" data-mb-tab="2" title="<?php echo _t("Share") ?>">
                        <span class="tabs-list__icon">
                            <span class="icon -share"></span>
                        </span>
                        <span class="tabs-list__label"><?php echo _t("Share") ?></span>
                    </a>
                </li>
                <?php if($isVideo && $showVideoRatio) { ?>
                    <?php if (empty($rrow['embed'])) { ?>
                        <li class="tabs-list__li">
                            <a href="<?php echo $video_url . '/' . $rrow['filename'][0] . '/' . $rrow['filename'][1] . '/' . $rrow['filename'][2] . '/' . $rrow['filename'][3] . '/' . $rrow['filename'][4] . '/' . $rrow['filename']; ?>" class="tabs-list__link" title="<?php echo _t("Download") ?>">
                                <span class="tabs-list__icon">
                                    <span class="icon -download"></span>
                                </span>
                                <span class="tabs-list__label"><?php echo _t("Download") ?></span>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
                <?php if ($allowAddFavorites) { ?>
                    <li class="tabs-list__li">
                        <a href="<?php echo $basehttp; ?>/action.php?action=add_favorites&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close") ?>"
                           title="<?php echo _t("Add to favorites") ?>" class="tabs-list__link">
                            <span class="tabs-list__icon">
                                <span class="icon -heart"></span>
                            </span>
                        </a>
                    </li>
                <?php } ?>
                <li class="tabs-list__li">
                    <a href="<?php echo $basehttp; ?>/action.php?action=reportVideo&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="iframe" data-opt-iframe-width="100%" data-opt-iframe-height="522px"
                       title="<?php echo _t("Report content") ?>" class="tabs-list__link">
                        <span class="tabs-list__icon">
                            <span class="icon -flag"></span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- tabs :: column :: end -->
</div>
