<?php 
$isVideo = $_GET['controller'] === 'video';
$isGallery = $_GET['controller'] === 'gallery';
?>
<div class="row">
    <!-- tabs-group :: column :: start -->
    <div class="tabs-group col">
        <div class="tabs-group__inner">
            <!-- tab :: module :: start -->
            <div class="tab mod" data-tab-group="media" data-tab-id="1">
                <div class="tab__inner">

                    <div class="row">
                        <!-- main-info :: column :: start -->
                        <div class="main-info col">
                            <div class="main-info__inner">
                        
                                <?php getWidget('widget.media.submittedby.php'); ?>
                                
                                <?php getWidget('widget.media.stats.php'); ?>
                                
                                
                            </div>
                        </div>
                        <!-- main-info :: column :: end -->


                    </div>

                    <div class="row -sep"></div>
                    <?php $_description = $rrow['description']; ?>
                        <?php $_pornstar = showPornstar2($rrow['record_num'], "<li class=\"tag-list__li\">", "</li>"); ?>
                        <?php $_keywords = buildTags2($rrow['keywords'], "<li class=\"tag-list__li\">", "</li>", ''); ?>
                        <?php $_channels = buildChannels2($rrow['record_num'], "<li class=\"tag-list__li\">", "</li>", ''); ?>
                        <?php $canExpand = !empty($_keywords) || !empty($_channels) || !empty($_description) || $_pornstar != false; ?>
                    <?php if($canExpand) { ?>
                    <div class="row" data-mb-expand-target="box-1">
                        
                        <!-- expandable :: column :: start -->
                        <div class="expandable col">
                            <div class="expandable__inner">
                                <?php getWidget('widget.media.info.php'); ?>
                            </div>
                        </div>
                        <!-- expandable :: column :: end -->

                    </div>

                    <div class="box__expander">
                        <button class="btn -outlined js-expander-trigger" data-mb-expand="box-1" data-mb-options="{'activeToMaxScreenWidth': true}">
                            <span class="btn__icon">
                                <span class="icon -expand"></span>
                            </span>
                            <span class="btn__label -when-inactive"><?php echo _t("Show more") ?></span>
                            <span class="btn__label -when-active"><?php echo _t("Show less") ?></span>
                        </button>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- tab :: module :: end -->

            <!-- tab :: module :: start -->
            <div class="tab mod" data-tab-group="media" data-tab-id="2">
                <div class="tab__inner">
                    <div class="row">
                        <textarea class="share__textarea" readonly>&lt;iframe src='<?php echo $basehttp; ?>/embed/<?php echo $rrow['record_num']; ?>' frameborder='0' height='400' width='600'&gt;&lt;/iframe&gt;&lt;br&gt;&lt;strong&gt;<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>&lt;/strong&gt; - powered by &lt;a href='<?php echo $basehttp; ?>'&gt;<?php echo $sitename; ?>&lt;/a&gt;</textarea>
                    </div>
                    <div class="row -sep"></div>
                    <div class="row">
                        <!-- share :: column :: start -->
                        <div class="share col">
                            <div class="share__inner">
                                <?php getWidget('widget.media.share-list.php'); ?>
                            </div>
                        </div>
                        <!-- share :: column :: end -->
                    </div>
                </div>
            </div>
            <!-- tab :: module :: end -->

        </div>
    </div>
    <!-- tabs-group :: column :: end -->
</div>