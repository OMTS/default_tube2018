<?
$link = generateUrl('user', $rrow['username'], $rrow['submitter']);
?>

		<div id="playerOverlay">
                    <span class="label"><? echo _t("We're sorry, You must be friends with "); ?><? echo $rrow[username]; ?> <? echo _t("to watch this video!"); ?></span>
                    <a class='btn btn-default btn-sm' href='<? echo $link; ?>'><? echo _t("Click here to view their profile"); ?></a>
		</div>
		<style>
		#playerOverlay {
			background:rgba(0,0,0,0.5);
			width: 490px;
			position:absolute;
			text-align: center;
			top:50%;
			left: 50%;
			padding-top: 10px;
			padding-bottom: 15px;
			margin-left: -245px;
			margin-top: -57px;
			z-index:3;
			font-size: 15px;
			font-weight: bold;
			color: #ffffff;
		}
                #playerOverlay .label {
                    display: block;
                    padding: 0 15px;
                    margin-bottom: 15px;
                }
		</style>

