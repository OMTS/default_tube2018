<?php 
$isVideo = $_GET['controller'] === 'video';
$isGallery = $_GET['controller'] === 'gallery';
?>
<!-- mstats :: mod :: start -->
<div class="mstats mod">
    <div class="mstats__inner">
        <ul class="mstats-list">
            <?php if($isGallery) { ?>
            <li class="mstats-list__li">
                <span class="mstats-list__icon">
                    <span class="icon -photo"></span>
                </span>
                <span class="mstats-list__label"><?php echo count($result); ?></span>
            </li>
            <?php } ?>
            <?php if($isVideo) { ?>
            <li class="mstats-list__li">
                <span class="mstats-list__icon">
                    <span class="icon -time"></span>
                </span>
                <span class="mstats-list__label"><?php echo sec2time($rrow['length']); ?></span>
            </li>
            <?php } ?>
            <li class="mstats-list__li">
                <span class="mstats-list__icon">
                    <span class="icon -eye"></span>
                </span>
                <span class="mstats-list__label"><?php echo $rrow['views']; ?></span>
            </li>
            <li class="mstats-list__li">
                <span class="mstats-list__icon">
                    <span class="icon -calendar"></span>
                </span>
                <span class="mstats-list__label"><?php echo $rrow['date_added']; ?></span>
            </li>
        </ul>
    </div>
</div>
<!-- mstats :: mod :: end -->