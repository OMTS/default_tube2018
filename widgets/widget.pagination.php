<?php if ($total_pages > 1) { ?>


    <section class="regular-sec g-sec -sec-pagination">
        <div class="wrapper">
            <div class="row">
                <!-- pagination :: column :: start -->
                <div class="pagination col">
                    <div class="pagination__inner">
                        <?php // include($basepath . '/includes/inc.pagination.php'); ?>
                        <ul class="pagination-list">
                            <?php
                            if ($_GET['photos']) {
                                $paginationAppend = "?photos=" . (int) $_GET['photos'];
                            }
                            if (!isset($_GET['page']) || $_GET[page] == '') {
                                $page = 1;
                            } else {
                                $page = $_GET['page'];
                            }

                            if ($page > 1) {
                                $prev = ($page - 1);
                                if ($prev == 1) {
                                    $prev = '1';
                                }
                                echo "<li class=\"pagination-list__li -prev\"><a rel='prev' title='" . _t("Prev") . "' href='page$prev.html$paginationAppend' class=\"pagination-list__elem -prev\"><span class=\"icon -prev\"></span></a></li>";
                            }
                            if ($page > 8) {
                                for ($i = 1; $i <= $total_pages; $i++) {
                                    if (($page) == $i) {
                                        echo "<li class=\"pagination-list__li is-active\"><span class=\"pagination-list__elem  is-active\">$i</span></li>";
                                    } else {
                                        if ($i == 1) {
                                            $link = '1';
                                        } else {
                                            $link = $i;
                                        }
                                        if ($i < ($page + 7) && $i > ($page - 7)) {
                                             echo "<li class=\"pagination-list__li\"><a class=\"pagination-list__elem\" title='" . _t("Page") . " " . $i . "' href=\"page$link.html$paginationAppend\">$i</a></li>";
                                        }
                                    }
                                }
                            } else {
                                for ($i = 1; $i <= $total_pages; $i++) {
                                    if (($page) == $i) {
                                        echo "<li class=\"pagination-list__li is-active\"><span class=\"pagination-list__elem  is-active\">$i</span></li>";
                                    } else {
                                        if ($i == 1) {
                                            $link = '1';
                                        } else {
                                            $link = $i;
                                        }
                                        if ($i <= 14) {
                                            echo "<li class=\"pagination-list__li\"><a class=\"pagination-list__elem\" title='" . _t("Page") . " " . $i . "' href=\"page$link.html$paginationAppend\">$i</a></li>";
                                        }
                                    }
                                }
                            }
                            if ($page < $total_pages) {
                                $next = ($page + 1);
                                echo "<li class=\"pagination-list__li -next\"><a rel='next' title='" . _t("Next") . "' href='page$next.html$paginationAppend' class=\"pagination-list__elem -next\"><span class=\"icon -next\"></span></a></li>";
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <!-- pagination :: column :: end -->
            </div>
        </div>
    </section>
<?php } ?>