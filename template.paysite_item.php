<?php
$link = generateUrl('paysite', $row['name'], $row['record_num']);
$title = htmlentities($row['name'], ENT_QUOTES, 'UTF-8');
$titleRaw = $row['name'];
$imgUrl =  $basehttp . '/core/images/catdefault.jpg';

if (file_exists($misc_path . '/paysite' . $row['record_num'] . '.jpg')) {
     $imgUrl = $misc_url. '/paysite'.$row['record_num'].'.jpg';
}

?>
<!-- citem :: column :: start -->
<div class="citem col -paysite">
    <div class="citem__inner">
        <a class="citem__link" href="<?php echo $link; ?>" title="<?php echo $title; ?>">
            <span class="citem__thumb">
                <img class="citem__thumb-img" src="<?php echo $imgUrl; ?>" alt="<?php echo $title; ?>">
            </span>
            <span class="citem__bd">
                <span class="citem__title"><?php echo $titleRaw; ?></span>
            </span>
        </a>
    </div>
</div>
<!-- citem :: column :: end -->