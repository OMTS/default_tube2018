<div class="col-half col text-center" style="margin-bottom:0.75rem;">
    <a href="<?php echo $basehttp; ?>/upload" title="<?php echo _t("Upload Video"); ?>" class="btn btn-default"><?php echo _t("Upload Video"); ?></a>
</div>

<div class="col-half col text-center" style="margin-bottom:0.75rem;">
    <a href="<?php echo $basehttp; ?>/upload_photo" title="<?php echo _t("Upload Gallery"); ?>" class="btn btn-default"><?php echo _t("Upload Gallery"); ?></a>
</div>