<?
$link = generateUrl('models', $row['name'], $row['record_num']);

?>
<!-- mitem :: column :: start -->
<div class="mitem col">
    <div class="mitem__inner">
        <a href="<? echo $link; ?>" title="<? echo ucwords($row['name']); ?>" class="mitem__link">
            <span class="mitem__thumb">
                <? if ($row[thumb] != '') { ?>
                <img src="<? echo $basehttp; ?>/media/misc/<? echo $row[thumb]; ?>" class="mitem__img" alt="<? echo ucwords($row['name']); ?>">
                <? } else { ?>
                <img src="<? echo $basehttp; ?>/core/images/catdefault.jpg" class="mitem__img" alt="<? echo ucwords($row['name']); ?>">
                <? } ?>	
            </span>

            <span class="mitem__ct">
                <span class="mitem__bd">
                    <span class="mitem__title"><? echo ucwords($row['name']); ?></span>
                </span>
                <span class="mitem__ft">
                    <span class="mitem__stat">
                        <span class="mitem__stat-icon">
                            <span class="icon -thumb-up"></span>
                        </span>
                        <span class="mitem__stat-label"><? echo (!empty($row['rating']) ? $row['rating'] : '0'); ?>%</span>
                    </span>
                </span>
            </span>
        </a>
    </div>
</div>
<!-- mitem :: column :: end -->
