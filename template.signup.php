<!-- fr :: column :: start -->
<div class="fr col">
    <div class="fr__inner">

        <?php if (isset($_GET['done']) && $_GET['done'] == 'true') { ?>
            <div class="notification success">
                <p>
                    <?php if ($require_account_confirmation) { ?>
                        Verification mail was sent to you email address.<br />
                        To login please <strong>verify your account</strong> through email verification link.
                    <?php } else { ?>
                        You may now login at the <a href="<?php echo $basehttp; ?>/login">login page</a>
                    <?php } ?>
                </p>
            </div>
        <?php } ?>	

        <?php if (!isset($_GET['done']) && $_GET['done'] != true) { ?> 
            <form class="fr__form -center" name="signupForm" method="post">


                <div class="fr__row">
                    <div class="fr__col">
                        <input type="text" placeholder="<?php echo _t("Username") ?>" value="<?php echo $thisusername; ?>" name="signup_username" class="fr__input -input-text" />
                    </div>
                </div>

                <div class="fr__row">
                    <div class="fr__col">
                        <input type="password" placeholder="<?php echo _t("Password") ?>" value="" name="signup_password" class="fr__input -input-text" />
                    </div>
                </div>

                <div class="fr__row">
                    <div class="fr__col">
                        <input type="email" placeholder="<?php echo _t("Email") ?>" value="<?php echo $thisemail; ?>" name="signup_email" class="fr__input -input-text" />
                    </div>
                </div>




                <?php if ($enable_signup_captcha) { ?>
                    <div class="fr__row">
                        <div class="fr__col">
                            <div class="captcha">
                                <img src="<?php echo $basehttp; ?>/captcha.php?<?php echo time(); ?>" class="captcha captcha-img captcha__img">
                                <input class="form-control captcha-input -captcha captcha__input" id="signup_captchaaa" name="captchaaa" type="text" value="" placeholder="<?php echo _t("Human?") ?>">
                            </div>
                        </div>
                    </div>
                <?php } ?>
	
				<div class="fr__row">
                    <div class="fr__col">
                        <input type="checkbox" name="signup_tos" class="fr__input -input-text" /> <? echo _t("Click here to indicate that you have read and agree to the"); ?> <a href='<? echo $basehttp; ?>/static/tos.html'><? echo _t("terms of service and privacy policy"); ?></a>
                    </div>
                </div>



                <div class="fr__row">
                    <div class="fr__col -center">
                        <button class="btn -primary fr__btn" type="submit" name="Submit"><?php echo _t("Register") ?></button>
                    </div>


                    <?php if (!$_SESSION['userid'] && ($enable_facebook_login || $enable_twitter_login)) { ?>
                    <div class="fr__row">
                        <div class="fr__col">
                            <?php if (!$_SESSION['userid'] && $enable_facebook_login) { ?>
                            <?php include($basepath . '/facebook_login.php'); ?>					
                            <a class="fr__link" href="<? echo $basehttp; ?>/includes/facebook/facebook.php"><img src="<?php echo $basehttp; ?>/core/images/facebook-login-button.png" alt="<?php echo _t("Login by FaceBook") ?>" border="0" /></a>
                            <?php } ?>
                            <?php if (!$_SESSION['userid'] && $enable_twitter_login) { ?>
                            <a class="fr__link" href="<?php echo $twitter->getAuthenticateUrl(); ?>&force_login=true"><img src="<?php echo $basehttp; ?>/core/images/twitter-login-button.png" alt="<?php echo _t("Login by Twitter") ?>" /></a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
            </form>
        <?php } ?>
    </div>
</div>
<!-- fr :: column :: end -->