<!-- textpage -->
<div class="textpage-col col">
    <div class="textpage-inner-col inner-col">

        <form action="" method="post" enctype="multipart/form-data" name="form1" id="edit-profile-form" class="form-block offset-columns">
            <!-- aside-tabs -->
            <aside class="aside-tabs-col">
                <div class="aside-tabs-inner-col inner-col">
                    <div class="profile-img-avatar">
                        <? if ($urow['avatar'] != '' && file_exists("$basepath/media/misc/$urow[avatar]")) { ?>
                            <img src='<? echo $basehttp; ?>/media/misc/<? echo $urow[avatar]; ?>' alt= '<? echo ucwords($urow['username']); ?>'>
                        <? } else { ?>
                            <? if (strtolower($urow['gender']) == 'male') { ?>
                                <img src='<? echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<? echo ucwords($urow['username']); ?>'>
                            <? } elseif (strtolower($urow['gender']) == 'female') { ?>
                                <img src='<? echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<? echo ucwords($urow['username']); ?>'>
                            <? } else { ?>
                                <img src='<? echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<? echo ucwords($urow['username']); ?>'>
                            <? } ?>
                        <? } ?>
                    </div>
                    <div class="avatar-upload">
                        <div class="fake-upload">
                            <input type="file" name="file" id="fileField" data-opt="upload-btn">
                            <a href="#" title="" class="btn btn-default btn-full" data-mb="fake-upload" data-opt-target="upload-btn"><span class="sub-label"><?php echo _t("Upload avatar") ?></span> <small>(max. <? echo $max_avatar_size; ?>kB)</small></a>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- aside-tabs END -->

            <!-- profile-content -->
            <section class="edit-profile-content-col">
                <div class="edit-profile-content-inner-col inner-col">
                    <div class="row form-row">
                        <!-- form -->
                        <div class="form-col col">
                            <div class="form-inner-col inner-col">
                                <div class="pseudo-form">
                                    <div class="row">
                                        <!-- form-item -->
                                        <div class="form-item-col col col-half">
                                            <div class="form-item-inner-col inner-col">
                                                <label><?php echo _t("Change Password") ?></label>
                                                <input type="text" name="newpassword" id="textfield1" value="" class="form-control" placeholder="<?php echo _t("Change Password") ?>">
                                            </div>
                                        </div>
                                        <!-- form-item END -->
                                        
                                    </div>

                                    <div class="row">
                                        <!-- form-item -->
                                        <div class="form-item-col col col-half">
                                            <div class="form-item-inner-col inner-col">
                                                <label><?php echo _t("Email Address") ?></label>
                                                <input type="text" name="email" id="textfield2" value="<? echo $urow['email']; ?>" class="form-control" placeholder="<?php echo _t("Email Address") ?>">
                                            </div>
                                        </div>
                                        <!-- form-item END -->
                                        <!-- form-item -->
                                        <div class="form-item-col col col-half">
                                            <div class="form-item-inner-col inner-col">
                                                <label><?php echo _t("Location") ?></label>
                                                <input type="text" name="location" id="textfield3" value="<? echo $urow['location']; ?>" class="form-control" placeholder="<?php echo _t("Location") ?>">
                                            </div>
                                        </div>
                                        <!-- form-item END -->
                                        
                                    </div>

                                    <div class="row">
                                        <!-- form-item -->
                                        <div class="form-item-col col col-half">
                                            <div class="form-item-inner-col inner-col">
                                                <label><?php echo _t("Age") ?></label>
                                                <select name="age" id="select" class="select-xsshort" data-style="btn-selectpicker">
                                                    <?
                                                    for ($i = 18; $i < 100; $i++) {
                                                        if ($urow['age'] == $i) {
                                                            $selected = 'selected';
                                                        } else {
                                                            $selected = '';
                                                        }
                                                        echo "<option $selected value='$i'> $i </option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- form-item END -->
                                        <!-- form-item -->
                                        <div class="form-item-col col col-half">
                                            <div class="form-item-inner-col inner-col">
                                                <label><?php echo _t("Gender") ?></label>
                                                <select name="gender" id="select2" class="select-short" data-style="btn-selectpicker">
                                                    <option <?
                                                    if ($urow['gender'] == 'Male') {
                                                        echo 'selected';
                                                    }
                                                    ?> value='Male'><?php echo _t("Male") ?></option>
                                                    <option <?
                                                    if ($urow['gender'] == 'Female') {
                                                        echo 'selected';
                                                    }
                                                    ?> value='Female'><?php echo _t("Female") ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- form-item END -->

                                    </div>

                                    <div class="row">
                                        <? foreach ($custom_user_fields as $k => $v) { ?>
                                            <!-- form-item -->
                                            <div class="form-item-col col col-half">
                                                <div class="form-item-inner-col inner-col">
                                                    <label><? echo $k; ?></label>
                                                    <? if (is_array($v)) { ?>
                                                        <select name="custom[<? echo $k; ?>]" data-style="btn-selectpicker">
                                                            <? foreach ($v as $i) { ?>
                                                                <option<? echo ($custom[$k] == $i) ? ' selected' : ''; ?>><? echo $i; ?></option>
                                                            <? } ?>
                                                        </select>
                                                    <? } else { ?>
                                                        <input type="text" placeholder="<? echo $k; ?>" name="custom[<? echo $k; ?>]" value='<? echo htmlentities($custom[$k], ENT_QUOTES, 'UTF-8'); ?>' class="form-control custom-field">
                                                    <? } ?>	
                                                </div>
                                            </div>
                                            <!-- form-item END -->

                                        <? } ?>
                                    </div>
                                    <div class="row">
                                        <!-- form-item -->
                                        <div class="form-item-col col col-full">
                                            <div class="form-item-inner-col inner-col">
                                                <label><?php echo _t("About yourself...") ?></label>
                                                <textarea name="description" id="textarea-about" class="form-control" placeholder="<?php echo _t("About yourself") ?>"><? echo $urow['description']; ?></textarea>
                                            </div>
                                        </div>
                                        <!-- form-item END -->
                                    </div>
                                    
                                    <fieldset class="email-notification">
                                        <legend class="email-notification-label">E-mail Notifications</legend>
                                        <div class="form-item form-checkbox">
                                            <div class="cr-holder checkbox">
                                                <label class="checkbox-inline">
                                                    <input id="form-email-new-message" class="checkbox-inline" type="checkbox" value="1" name="notifications[new_message]" <?php echo $urow['notifications']['new_message'] == 1 ? 'checked="checked"' : ''; ?>>
                                                    <span class="checkbox-label sub-label"><?php echo _t("Email me when someone sends me a private message"); ?></span>
                                                </label>
                                            </div>  
                                        </div>
                                        <div class="form-item form-checkbox">
                                            <div class="cr-holder checkbox">
                                                <label class="checkbox-inline">
                                                    <input id="form-email-new-comment" class="checkbox-inline" type="checkbox" value="1" name="notifications[new_comment]" <?php echo $urow['notifications']['new_comment'] == 1 ? 'checked="checked"' : ''; ?>>
                                                    <span class="checkbox-label sub-label"><?php echo _t("Email me when someone posts a comment to my content"); ?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-item form-checkbox">
                                            <div class="cr-holder checkbox">
                                                <label class="checkbox-inline">
                                                    <input id="form-email-new-post" class="checkbox-inline" type="checkbox" value="1" name="notifications[new_post]" <?php echo $urow['notifications']['new_post'] == 1 ? 'checked="checked"' : ''; ?>>
                                                    <span class="checkbox-label sub-label"><?php echo _t("Email me when someone writes on my wall"); ?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-item form-checkbox">
                                            <div class="cr-holder checkbox">
                                                <label class="checkbox-inline">
                                                    <input id="form-email-friend-request" class="checkbox-inline" type="checkbox" value="1" name="notifications[friend_request]" <?php echo $urow['notifications']['friend_request'] == 1 ? 'checked="checked"' : ''; ?>>
                                                    <span class="checkbox-label sub-label"><?php echo _t("Email me when someone sends me a friend request"); ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>




                                    <div class="row">
                                        <!-- form-item -->
                                        <div class="form-item-col col form-item--actions">
                                            <div class="form-item-inner-col inner-col">
                                                <button class="btn btn-default"  type="submit" name="button" id="button"><span class="btn-label"><?php echo _t("Save") ?></span></button>
                                            </div>
                                        </div>
                                        <!-- form-item END -->
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- form END -->
                    </div>
                </div>
            </section>

        </form>










    </div>
</div>
<!-- textpage END -->